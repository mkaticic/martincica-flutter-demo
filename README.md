# martincica_flutter_demo

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials, samples, guidance on
mobile development, and a full API reference.

## platform-tools

```
[mirna@mkaticic platform-tools]$ pwd
/data/home/install/android-sdk/Sdk/platform-tools
[mirna@mkaticic platform-tools]$ adb shell
HWVOG:/ $ pm list packages|grep martincica
package:hr.martincica.android_demo
package:hr.martincica.martincica_flutter_demo
package:hr.martincica.flutter_state_management
package:hr.martincica.martincica_demo
```

## code generation - moor / drift

flutter packages pub run build_runner build

## Generate icons based on flutter_launcher_icons dependency
 - https://pub.dev/packages/flutter_launcher_icons

```
flutter pub run flutter_launcher_icons:main
```

## Build apk
```
PUBSPEC_FILE=pubspec.yaml
version=1.0.0+17
sed -i -E "$(yq '.version | line' "$PUBSPEC_FILE")s/(.*version:).*/\1 ${version}/" "$PUBSPEC_FILE" || true # ignore errors
git tag -a ${version} -m "${version} release"
flutter build apk
mv build/app/outputs/flutter-apk/app-release.apk build/app/outputs/flutter-apk/app-release-${version}.apk
```

## logs

/home/mirna/install/flutter/latest/bin/flutter logs

## flutter upgrade downgrade

current flutter version: 3.22.0-40.0.pre

```
flutter_home=/data/home/install/flutter/2.5.3/bin
# check version
$flutter_home/flutter --version

# upgrade flutter
flutter_home/flutter upgrage

# revert to tag 3.22.0-40.0.pre using git
cd $flutter_home
git checkout 3.22.0-40.0.pre
```