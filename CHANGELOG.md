# CHANGELOG: martincica_flutter_demo 

## v1.3
 - GEOT-73: Add web search button where apropriate
   - on species cannonical name
   - on latin word lemma

## v1.2 
 - app icon added

## v1.1
 - observation detail improvements
 - gallery detail improvements

## v1.0
 - first release
 - contains all features implemeted since the beggining 30/10/2021