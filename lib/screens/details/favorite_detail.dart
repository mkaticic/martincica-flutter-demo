// ignore_for_file: prefer_const_constructors, prefer_const_constructors_in_immutables, use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/screens/mixin/api/gbif_api_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/favorite_tags_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/preferences_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/url_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:martincica_flutter_demo/widget/species_widget.dart';

import '../../data/drift_db.dart';

class FavoriteDetailScreen extends StatefulWidget {
  final FavoriteSpecy favElement;

  FavoriteDetailScreen(this.favElement, {Key? key}) : super(key: key);

  @override
  _FavoriteDetailScreenState createState() => _FavoriteDetailScreenState();
}

class _FavoriteDetailScreenState extends State<FavoriteDetailScreen>
    with
        PreferencesMixin,
        FavoriteTagsMixin,
        UtilsMixin,
        UrlMixin,
        GbifApiMixin {
  @override
  void initState() {
    getGbifSpecies(widget.favElement.gbifKey ?? -1).then((value) {
      gbifSpecies = value;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (favoriteTags == null) {
      reloadTags(widget.favElement.id);
    }

    if (favoriteTagSuggestions == null) {
      reloadFavoriteTagSuggestions();
    }

    if (favoriteTags == null || favoriteTagSuggestions == null) {
      return Container();
    }

    return Scaffold(
        appBar: AppBar(title: Text(widget.favElement.canonicalName)),
        body: Padding(
            padding: const EdgeInsets.all(24.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  buildTags(widget.favElement.id),
                  Divider(),
                  SpeciesWidget(gbifSpecies, key: ValueKey(gbifSpecies.key))
                ],
              ),
            )));
  }

}
