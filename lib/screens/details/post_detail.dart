// ignore_for_file: prefer_const_constructors, prefer_const_constructors_in_immutables, use_key_in_widget_constructors

import 'package:drift/drift.dart' hide Column;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/blog_post_dao.dart';
import 'package:martincica_flutter_demo/screens/posts_screen.dart';
import 'package:provider/provider.dart';

import '../../data/drift_db.dart';

class PostDetailScreen extends StatefulWidget {
  final BlogPost post;
  final isNew;

  PostDetailScreen(this.post, this.isNew);

  @override
  _PostDetailScreenState createState() => _PostDetailScreenState();
}

class _PostDetailScreenState extends State<PostDetailScreen> {
  TextEditingController txtName = TextEditingController();
  TextEditingController txtContent = TextEditingController();
  TextEditingController txtDate = TextEditingController();

  DateFormat formatter = DateFormat('dd/MM/yyyy');

  @override
  void initState() {
    txtName.text = widget.post.name;
    txtContent.text = widget.post.content ?? '';
    String postDate =
        (widget.post.date != null) ? formatter.format(widget.post.date!) : '';
    txtDate.text = postDate;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    BlogPostDao db = Provider.of<MoorDb>(context).blogPostDao;

    return Scaffold(
      appBar: AppBar(
          title:
              widget.isNew ? Text('Insert blog post') : Text('Edit blog post')),
      body: SingleChildScrollView(
        child: Column(
          children: [
            BlogText('Name', txtName, 1),
            BlogText('Content', txtContent, 5),
            BlogText('Date', txtDate, 1),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.save),
          onPressed: () {
            if (widget.isNew) {
              BlogPostsCompanion newPost = BlogPostsCompanion(
                name: Value(txtName.text),
                content: Value(txtContent.text),
                date: Value((txtDate.text != '')
                    ? formatter.parse(txtDate.text)
                    : null),
              );
              db.insertPost(newPost);
            } else {
              BlogPost post = BlogPost(
                id: widget.post.id,
                name: txtName.text,
                content: txtContent.text,
                date:
                    (txtDate.text != '') ? formatter.parse(txtDate.text) : null,
              );
              db.updatePost(post);
            }

            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => PostsScreen()));
          }),
    );
  }
}

class BlogText extends StatelessWidget {
  final String description;
  final TextEditingController controller;
  final int numLines;

  BlogText(this.description, this.controller, this.numLines);

  @override
  Widget build(BuildContext context) {
    TextInputType textInputType;

    if (numLines > 1) {
      textInputType = TextInputType.multiline;
    } else if (this.description == 'Date') {
      textInputType = TextInputType.datetime;
    } else {
      textInputType = TextInputType.text;
    }

    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: TextField(
        controller: controller,
        keyboardType: textInputType,
        maxLines: numLines,
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            hintText: description),
      ),
    );
  }
}
