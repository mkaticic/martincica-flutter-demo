import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:martincica_flutter_demo/screens/mixin/osm_map_mixin.dart';

import '../../shared/menu_drawer.dart';

class OpenStreetMapDetail extends StatefulWidget {
  final LatLng point;

  const OpenStreetMapDetail(this.point, {Key? key}) : super(key: key);

  @override
  State<OpenStreetMapDetail> createState() => _OpenStreetMapDetailState();
}

class _OpenStreetMapDetailState extends State<OpenStreetMapDetail>
    with OsmMapMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Location')),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            // const Padding(
            //   padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
            //   child: Text('This is a map that is showing (51.5, -0.9).'),
            // ),
            Flexible(
              child: buildFlutterMap(widget.point),
            ),
          ],
        ),
      ),
    );
  }
}
