import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/screens/mixin/osm_map_mixin.dart';

import '../../shared/menu_drawer.dart';
import '../../utils/exif_utils.dart';

class OsmDetailFromExifData extends StatefulWidget {
  final List<ExifData> exifData;

  const OsmDetailFromExifData(this.exifData, {Key? key}) : super(key: key);

  @override
  State<OsmDetailFromExifData> createState() => _OsmDetailFromExifDataState();
}

class _OsmDetailFromExifDataState extends State<OsmDetailFromExifData>
    with OsmMapMixin {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Locations: ${widget.exifData.length}')),
      drawer: MenuDrawer(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            // const Padding(
            //   padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
            //   child: Text('This is a map that is showing (51.5, -0.9).'),
            // ),
            Flexible(
              child: buildFlutterMapFromExifData(widget.exifData),
            ),
          ],
        ),
      ),
    );
  }
}
