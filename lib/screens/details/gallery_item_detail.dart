// ignore_for_file: prefer_const_constructors

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/screens/mixin/osm_map_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/preferences_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/tags_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:photo_view/photo_view.dart';

import '../../utils/exif_utils.dart';
import '../mixin/files_mixin.dart';
import '../mixin/share_whatsup_mixin.dart';

class GaleryItemDetail extends StatefulWidget {
  final TagType tagType;
  final String tagRefId;
  final String path;
  final VoidCallback onDeleteAction;

  GaleryItemDetail(this.path, {Key? key, TagType? tagType, String? tagRefId, VoidCallback? onDeleteAction}) :
        this.tagType = tagType ?? TagType.none,
        this.tagRefId = tagRefId?? '',
        this.onDeleteAction = onDeleteAction?? (() => {});

  @override
  _GaleryItemDetailState createState() => _GaleryItemDetailState();
}

class _GaleryItemDetailState extends State<GaleryItemDetail>
    with TagsMixin, OsmMapMixin, PreferencesMixin, ShareWhatsupMixin {
  late ExifData data;
  bool dataInitialized = false;

  @override
  Widget build(BuildContext context) {
    if (!dataInitialized) {
      ExifUtils.getExifData(File(widget.path)).then((value) {
        data = value;
        dataInitialized = true;
        setState(() {});
      });
      return Container();
    }
    return Scaffold(
        appBar: AppBar(title: Text('Gallery item details')),
        bottomSheet: GalleryItemDetailActionBar(widget.path, data, widget.tagType, widget.tagRefId, widget.onDeleteAction),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(24),
          child: Column(children: [
            addText('Date:', data.dateTime),
            addPhotoView(data.file),
            addText('Path:', data.path, debug: true),
            addText('', data.userComment),
            addMorphologyTags(data.path),
          ]),
        ));
  }

  addText(String label, String dateTime, {bool debug = false}) {
    return Visibility(
            visible: debug? isDebugModeOn(): true,
            child: Container(
              alignment: Alignment.topLeft,
              margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
              child: Text('$label $dateTime'),
            ));
  }

  addPhotoView(File file) {
    return Container(
              height: 300,
              margin: EdgeInsets.only(top: 8.0, bottom: 16.0),
              child: PhotoView(
                tightMode: true,
                backgroundDecoration: BoxDecoration(
                  color: Colors.white,
                ),
                imageProvider: FileImage(file),
              )
            );
  }

  addMorphologyTags(String path) {
    return Container(
              alignment: Alignment.bottomLeft,
              margin: EdgeInsets.only(top: 16.0, bottom: 16.0),
              child: buildTags(TagType.morfology, data.path),
            );
  }


}

class GalleryItemDetailActionBar extends StatefulWidget {
  final TagType tagType;
  final String tagRefId;
  final String path;
  final ExifData data;
  final VoidCallback onDeleteAction;

  const GalleryItemDetailActionBar(this.path, this.data, this.tagType, this.tagRefId, this.onDeleteAction, {Key? key}) : super(key: key);

  @override
  State<GalleryItemDetailActionBar> createState() => _GalleryItemDetailActionBarState();
}

class _GalleryItemDetailActionBarState extends State<GalleryItemDetailActionBar>
    with TagsMixin, OsmMapMixin, FilesMixin, UtilsMixin, PreferencesMixin, ShareWhatsupMixin {

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        switch (index) {
          case 0:
            break;
          case 1:
            openOSMapDetailPage(widget.data!.point);
            break;
          case 2:
            shareFile(widget.data!.path);
            break;
          case 3:
            showConfirmDeleteDialog(() {
              // on confirm
              deleteTag(widget.tagType, widget.tagRefId, widget.path)
                  .then((tagDeleted) {
                    checkNotReferenced(widget.path)
                        .then((pathNotReferenced){
                          if(pathNotReferenced){
                            deleteFile(widget.path);
                          }
                        });
                    widget.onDeleteAction.call();
                  });
              Navigator.pop(context);
              setState(() {});
            });
            break;
        }
      },
      items: [
        BottomNavigationBarItem(icon: Icon(Icons.arrow_right), label: 'Actions'),
        BottomNavigationBarItem(icon: Icon(Icons.map), label: 'Show on map'),
        BottomNavigationBarItem(icon: Icon(Icons.share), label: 'Share image'),
        deleteIcon(widget.tagType)
      ],
    );
  }

  deleteIcon(TagType tagType) {
    if(tagType == TagType.none){
      return BottomNavigationBarItem(icon:  Container(), label: '');
    }
    return BottomNavigationBarItem(icon:  Icon(Icons.delete), label: 'Delete');
  }

}
