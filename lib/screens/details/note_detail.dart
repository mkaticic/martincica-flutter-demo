// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/model/db/sqlite/note.dart';
import 'package:martincica_flutter_demo/data/sqlite_db.dart';
import 'package:martincica_flutter_demo/screens/notelist_screen.dart';

class NoteScreen extends StatefulWidget {
  final Note note;
  final bool isNew;

  NoteScreen(this.note, this.isNew);

  @override
  _NoteScreenState createState() => _NoteScreenState();
}

class _NoteScreenState extends State<NoteScreen> {
  SqliteDb db = SqliteDb();

  final TextEditingController txtName = TextEditingController();
  final TextEditingController txtNotes = TextEditingController();
  final TextEditingController txtDate = TextEditingController();

  @override
  void initState() {
    if (!widget.isNew) {
      txtName.text = widget.note.name;
      txtNotes.text = widget.note.notes;
      txtDate.text = widget.note.date;
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          AppBar(title: widget.isNew ? Text('Insert Note') : Text('Edit Note')),
      body: SingleChildScrollView(
        child: Column(
          children: [
            NoteText('Name', txtName),
            NoteText('Description', txtNotes),
            NoteText('Date', txtDate),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.save),
          onPressed: () {
            //save Note
            widget.note.name = txtName.text;
            widget.note.notes = txtNotes.text;
            widget.note.date = txtDate.text;
            widget.note.position = 0;
            if (widget.isNew) {
              db.insertNote(widget.note).then((_) => backToTheMainNoteScreen());
            } else {
              db.updateNote(widget.note).then((_) => backToTheMainNoteScreen());
            }
          }),
    );
  }

  void backToTheMainNoteScreen() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (_) => NotelistScreen()));
  }
}

class NoteText extends StatelessWidget {
  final String description;
  final TextEditingController controller;

  NoteText(this.description, this.controller);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: TextField(
        controller: controller,
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10))),
            hintText: description),
      ),
    );
  }
}
