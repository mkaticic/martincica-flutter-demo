// ignore_for_file: prefer_const_constructors

import 'dart:io';

import 'package:camera/camera.dart';
import 'package:drift/drift.dart' hide Column;
import 'package:flutter/material.dart';
import 'package:flutter_document_picker/flutter_document_picker.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/data/file_helper.dart';
import 'package:martincica_flutter_demo/screens/mixin/gallery_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/tags_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:martincica_flutter_demo/shared/camera/camera_screen.dart';
import 'package:martincica_flutter_demo/utils/camera_utils.dart';
import 'package:martincica_flutter_demo/utils/exif_utils.dart';
import 'package:provider/provider.dart';

import '../../data/model/db/drift/dao/observations_dao.dart';
import 'observation_detail.dart';

mixin ObservationDetailsPhotosTabMixin<S extends StatefulWidget>
    on State<S>, TagsMixin<S>, GalleryMixin<S>, UtilsMixin<S> {
  late CameraDescription camera;

  void initPhotosTab(ObservationDetailDialog widget) {
    CameraUtils.getCamera().then((value) => camera = value);
    reloadPhotos(widget.observation.id.toString());
  }

  Widget buildPhotosDetails(camera, widget) {
    var observationId = widget.observation.id;
    return SingleChildScrollView(
      child: Column(children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          expandedElevatedButton(
              child: Icon(Icons.camera),
              onPressed: () => openTakePicScreen(widget)),
          expandedElevatedButton(
              child: Icon(Icons.image_search),
              onPressed: () => chooseImagesFromDisk(widget)),
        ]),
        buildGallery(context, scalehight: 0.6)
      ]),
    );
  }

  chooseImagesFromDisk(ObservationDetailDialog widget) {
    //With parameters:
    FlutterDocumentPickerParams params =
        FlutterDocumentPickerParams(allowedMimeTypes: ['image/*']);

    //Pick multiple files:
    FlutterDocumentPicker.openDocuments(params: params).then((files) {
      var fls = files ?? [];
      for (var element in fls) {
        if (element == null) continue;
        saveLocationToObservation(widget, element);
        copyFileAndTag(widget.observation.id, File(element));
      }
    });
  }

  openTakePicScreen(ObservationDetailDialog widget) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => TakePictureScreen(
                camera: camera,
                onSave: (File picture) {
                  saveLocationToObservation(widget, picture.path);
                  copyFileAndTag(widget.observation.id, picture);
                })));
  }

  copyFileAndTag(int observationId, File file) {
    FileHelper helper = FileHelper();
    helper.moveFileToObservationFolder(file).then((picture) {
      insertTag(
              context,
              newTag(TagType.observation_photo, observationId.toString(),
                  picture.path))
          .then((_) {
        reloadPhotos(observationId.toString());
      });
    });
  }

  void saveLocationToObservation(
      ObservationDetailDialog widget, String element) {
    ObservationsDao db =
        Provider.of<MoorDb>(context, listen: false).observationsDao;
    db.getById(widget.observation.id).then((obs) {
      ExifUtils.checkGPSData(File(element)).then((latlong) {
        if (obs.latitude == null && latlong != null) {
          ObservationsCompanion oc = ObservationsCompanion(
              id: Value(widget.observation.id),
              latitude: Value(latlong!.latitude),
              longitude: Value(latlong!.longitude),
              date: Value(obs.date),
              description: Value(obs.description),
              scientificName: Value(obs.scientificName),
              location: Value(obs.location),
              isMine: Value(obs.isMine),
              gbifKey: Value(obs.gbifKey));
          db.updateRow(oc).then((onValue) {
            setState(() {});
          });
        }
      });
    });
  }
}
