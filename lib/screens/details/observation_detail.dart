// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/screens/mixin/gallery_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/preferences_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/search_species_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/tags_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:martincica_flutter_demo/screens/observations_screen.dart';
import 'package:martincica_flutter_demo/widget/species_widget.dart';

import '../mixin/osm_map_mixin.dart';
import '../mixin/api/weather_api_mixin.dart';
import 'observation_detail__main_tab_mixin.dart';
import 'observation_detail__photos_tab_mixin.dart';

class ObservationDetailDialog extends StatefulWidget {
  Observation observation;
  bool isNew;

  ObservationDetailDialog(this.observation, this.isNew);

  @override
  _ObservationDetailDialogState createState() =>
      _ObservationDetailDialogState();
}

class _ObservationDetailDialogState extends State<ObservationDetailDialog>
    with
        PreferencesMixin,
        SearchGbifSpeciesMixin,
        TagsMixin,
        UtilsMixin,
        OsmMapMixin,
        GalleryMixin,
        WeatherApiMixin,

        // tabs
        ObservationDetailsMainTabMixin,
        ObservationDetailsPhotosTabMixin {
  int selectedIndex = 0;
  final double pannelPadding = 24.0;
  final double contentPaddingH = 6.0;
  final double contentPaddingV = 6.0;

  final double minTabHeight = 40;
  late List<bool> isSelected = [true, false, false, false];

  @override
  void initState() {
    initMainTab(widget);
    initPhotosTab(widget);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String title = widget.isNew ? "Insert new observation" : "Edit observation";
    return Scaffold(
        appBar: AppBar(
            title: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text(title), addSaveElevatedButton(context)])),
        body: SingleChildScrollView(
          padding: EdgeInsets.all(pannelPadding),
          child: Column(children: [
            buildToggleButtonsTop()
            //,buildToggleButtonsButtom()
            ,
            buildTabContent(selectedIndex)
          ]),
        ));
  }

  Widget buildTabContent(int selectedIndex) {
    Widget content = Container();
    int observationId = widget.observation.id;
    if (selectedIndex == 2) {
      content = SpeciesWidget(gbifSpecies, key: ValueKey(gbifSpecies.key));
    } else if (selectedIndex == 1) {
      content = buildPhotosDetails(camera, widget);
    } else {
      // default
      content = buildMainTabContent(context, widget);
    }
    return SingleChildScrollView(
        child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: contentPaddingV, horizontal: contentPaddingH),
            child: content));
  }

  Widget buildToggleButtonsTop() {
    return ToggleButtons(
      children: [
        paddedHText('Observation'),
        paddedHText('Photos'),
        paddedHText('Taxon'),
      ],
      constraints: BoxConstraints(
          minHeight: minTabHeight,
          minWidth: (MediaQuery.of(context).size.width -
                  (2 * pannelPadding + 4 * contentPaddingH)) /
              3),
      borderRadius: BorderRadius.only(topRight: Radius.circular(70)),
      isSelected: isSelected.sublist(0, 3),
      onPressed: (int index) {
        selectedIndex = index;
        for (int i = 0; i < isSelected.length; i++) {
          if (i == index) {
            isSelected[i] = true;
          } else {
            isSelected[i] = false;
          }
        }
        setState(() {});
      },
    );
  }

  Widget buildToggleButtonsButtom() {
    return ToggleButtons(
      children: [paddedHText('Photos'), paddedHText('Weather')],
      constraints: BoxConstraints(
          minHeight: minTabHeight,
          minWidth: (MediaQuery.of(context).size.width -
                  (2 * pannelPadding + 4 * contentPaddingH)) /
              2),
      borderRadius: BorderRadius.circular(90),
      isSelected: isSelected.sublist(2, 4),
      onPressed: (int index) {
        selectedIndex = index + 2;
        for (int i = 0; i < isSelected.length; i++) {
          if (i == selectedIndex) {
            isSelected[i] = true;
          } else {
            isSelected[i] = false;
          }
        }
        setState(() {});
      },
    );
  }

  addSaveElevatedButton(BuildContext context) {
    return ElevatedButton(
        child: Row(
          children: [Icon(Icons.save)],
        ),
        style: ButtonStyle(
            shadowColor: MaterialStateProperty.all<Color>(Colors.white)),
        onPressed: () {
          saveObservation(widget);
          Navigator.pop(context);
          Navigator.push(
              context, MaterialPageRoute(builder: (_) => ObservationsScreen()));
        });
  }
}
