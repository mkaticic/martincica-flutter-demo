// ignore_for_file: prefer_const_constructors

import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/file_helper.dart';
import 'package:path/path.dart';
import 'package:share/share.dart';
import '../files_screen.dart';

class FileScreen extends StatefulWidget {
  final File? file;

  FileScreen(this.file);

  @override
  _FileScreenState createState() => _FileScreenState();
}

class _FileScreenState extends State<FileScreen> {
  TextEditingController titleController = TextEditingController();
  TextEditingController contentController = TextEditingController();
  FileHelper helper = FileHelper();

  @override
  void initState() {
    if (widget.file == null) {
      DateTime now = DateTime.now();
      String today = '${now.year}-${now.month}-${now.day}';
      titleController.text = 'observation-' + today;
      contentController.text = '';
    } else {
      titleController.text = basename(widget.file!.path);
      helper
          .readFromAFile(widget.file!)
          .then((value) => contentController.text = value);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: TextField(controller: titleController), actions: [
        IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              saveFile().then((value) => Share.shareFiles([widget.file!.path],
                  text: basename(widget.file!.path)));
            })
      ]),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: Column(children: [
          Expanded(
            child: TextField(
              maxLines: null,
              expands: true,
              controller: contentController,
            ),
          )
        ]),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.save),
        onPressed: () {
          saveFile();
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => FilesScreen()));
        },
      ),
    );
  }

  Future saveFile() async {
    helper.writeToFile(titleController.text, contentController.text);
  }
}
