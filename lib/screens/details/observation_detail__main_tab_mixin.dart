// ignore_for_file: prefer_const_constructors

import 'dart:ffi';

import 'package:drift/drift.dart' hide Column;
import 'package:flutter/material.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:intl/intl.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif/gbif_species.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/observations_dao.dart';
import 'package:martincica_flutter_demo/screens/mixin/search_species_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/tags_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:martincica_flutter_demo/widget/gbif_taxon_chooser.dart';
import 'package:provider/provider.dart';

import '../mixin/api/weather_api_mixin.dart';
import 'observation_detail.dart';

mixin ObservationDetailsMainTabMixin<S extends StatefulWidget>
    on
        State<S>,
        UtilsMixin<S>,
        SearchGbifSpeciesMixin<S>,
        TagsMixin<S>,
        WeatherApiMixin<S> {
  int? observationId;
  int? gbifKey;

  final DateFormat formatter = DateFormat('yyyy-MM-dd');

  final TextEditingController txtDescription = TextEditingController();
  final TextEditingController txtNotes = TextEditingController();

  //late FocusNode? txtDescriptionFocusNode;

  final TextEditingController txtScientificName = TextEditingController();
  final TextEditingController txtLocation = TextEditingController();
  final TextEditingController txtDate = TextEditingController();
  late bool isMine;

  bool hidePass = false;
  bool observationMainTabIsInitialized = false;

  void initMainTab(ObservationDetailDialog widget) {
    if(observationMainTabIsInitialized){
      return;
    }
    if (!widget.isNew) {
      txtDate.text = formatter.format(widget.observation.date);
      txtDescription.text = widget.observation.description;
      txtScientificName.text = widget.observation.scientificName ?? '';
      txtLocation.text = widget.observation.location;
      txtNotes.text = widget.observation.notes ?? '';

      isMine = widget.observation.isMine;
      observationId = widget.observation.id;
      gbifKey = widget.observation.gbifKey;

      getSpecies(gbifKey);
    } else {
      txtDate.text = formatter.format(DateTime.now());
      isMine = true;
    }
    observationMainTabIsInitialized = true;
  }

  Widget buildMainTabContent(
      BuildContext context, ObservationDetailDialog widget) {
    // txtDescriptionFocusNode = FocusNode();
    // txtDescriptionFocusNode?.requestFocus();
    return Column(
      children: [
        // my details
        addTextfield('Description', txtDescription),
        addTextfield('Scientific name', txtScientificName,
            sufixIcon: addLinkTaxonButton(context, widget)),
        addDatePicker(context),
        addLocationTxtFiled(),
        addTextArea('Notes', txtNotes, maxLines: 3),
        addWeatherButtonToggle(),
        buildWheather(observationId),
        addIsMineCheckBox(),
        buildTags(TagType.observation, observationId.toString()),
      ],
    );
  }

  Widget linkScinetificName(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(bottom: 16, right: 32),
        child: ElevatedButton(
            child: Icon(Icons.link),
            onPressed: () {
              txtScientificName.text = gbifSpecies.scientificName;
              gbifKey = gbifSpecies.key;
            }));
  }

  addDatePicker(BuildContext context) {
    return TextField(
      controller: txtDate,
      decoration: InputDecoration(hintText: 'Date'),
      onTap: () => showDatePicker(
        context: context,
        initialDate: DateTime.parse(txtDate.text),
        firstDate: DateTime(2000),
        lastDate: DateTime(2125),
        helpText: "Select observation date",
        cancelText: "Cancel",
        confirmText: "Select",
        fieldHintText: "DATE/MONTH/YEAR",
        fieldLabelText: "Observation date",
      ).then((value) => txtDate.text =
          value != null ? formatter.format(value) : txtDate.text),
    );
  }

  addLocationTxtFiled() {
    return TypeAheadField(
          textFieldConfiguration: TextFieldConfiguration(
              controller: txtLocation,
              onSubmitted: (value) => settings.addSuggestion(settings.ddHistoryLocationsKey, value),
              autofocus: false,
              obscureText: hidePass,
              decoration: InputDecoration(
                hintText: 'Location',
                suffixIcon: IconButton(
                    onPressed: () {
                      setState(() {
                        hidePass = !hidePass;
                      });
                    },
                    icon: hidePass
                        ? Icon(Icons.visibility)
                        : Icon(Icons.visibility_off)))
          ),
          suggestionsCallback: (pattern) async {
            return await settings.getSuggestions(
                settings.ddHistoryLocationsKey, pattern);
          },
          itemBuilder: (context, suggestion) {
            return ListTile(
              title: Text(suggestion!.toString()),
              onTap: () => txtLocation.text = suggestion!.toString(),
              onLongPress: () => showConfirmDeleteDialog(
                  () => settings.deleteSuggestion(settings.ddHistoryLocationsKey, suggestion!.toString())
              )
            );
          },
          onSuggestionSelected: (suggestion) {
            txtLocation.text = suggestion!.toString();
          }
        );
  }

  addIsMineCheckBox() {
    return Row(
      children: [
        Text('Is mine?'),
        Checkbox(
            value: isMine,
            activeColor: Colors.green,
            onChanged: (bool? newValue) {
              setState(() {
                isMine = newValue ?? false;
              });
            })
      ],
    );
  }

  addLinkTaxonButton(BuildContext context, ObservationDetailDialog widget) {
    return IconButton(
        icon: Icon(Icons.account_tree),
        onPressed: () {
          showDialog(
              context: context,
              builder: (_) {
                return GbifTaxonChooser(onSelected: (GbifSpecies species) {
                  gbifSpecies = species;
                  gbifKey = species.key;
                  txtScientificName.text = species.scientificName;
                  saveObservation(widget);
                });
              });
        });
  }

  void saveObservation(ObservationDetailDialog widget) {
    ObservationsDao db =
        Provider.of<MoorDb>(context, listen: false).observationsDao;
    if (widget.isNew) {
      ObservationsCompanion observation = ObservationsCompanion(
          date: Value(DateTime.parse(txtDate.text)),
          description: Value(txtDescription.text),
          scientificName: Value(txtScientificName.text),
          location: Value(txtLocation.text),
          notes: Value(txtNotes.text),
          isMine: Value(isMine),
          gbifKey: Value(gbifKey));
      db.insertRow(observation).then((id) {
        db.getById(id).then((value) {
          widget.observation = value;
          widget.isNew = false;
          observationId = id;
          setState(() {});
        });
      });
    } else {
      ObservationsCompanion observation = ObservationsCompanion(
          id: Value(widget.observation.id),
          date: Value(DateTime.parse(txtDate.text)),
          description: Value(txtDescription.text),
          scientificName: Value(txtScientificName.text),
          location: Value(txtLocation.text),
          notes: Value(txtNotes.text),
          isMine: Value(isMine),
          gbifKey: Value(gbifKey));
      db.updateRow(observation);
    }
  }

  addWeatherButtonToggle() {
    return Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
      Text("Show / hide weather data: "),
      expandedElevatedButton(
          child: Icon(Icons.sunny),
          onPressed: () {
            showWeather = !showWeather;
            setState(() {});
          })
    ]);
  }
}
