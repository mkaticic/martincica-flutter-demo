// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/locations_dao.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';
import 'package:provider/provider.dart';

import 'mixin/filter_screen_mixin.dart';

class LocationsScreen extends StatefulWidget {
  const LocationsScreen({Key? key}) : super(key: key);

  @override
  _LocationsScreenState createState() => _LocationsScreenState();
}

class _LocationsScreenState extends State<LocationsScreen>
    with UtilsMixin, FilterScreenMixin {
  final DateFormat formatter = DateFormat('yyyy-MM-dd');

  List<Location> locations = [];

  @override
  void initState() {
    runFilterByText(txtFilter.value.text);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: buildFilterableTitle(
                screenTitle: 'Locations',
                filterFunction: (text) => runFilterByText(text)),
        actions: [
          IconButton(
            onPressed: () {
              Provider.of<MoorDb>(context, listen: false).importLocationsFromObservations();
            },
            icon: const Icon(Icons.import_export))
        ],
        ),
        drawer: MenuDrawer(),
        bottomNavigationBar: GTBottomNavBar(),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              buildLocationsList()
            ],
          ),
        ),
        // floatingActionButton: FloatingActionButton(
        //     child: Icon(Icons.add),
        //     onPressed: () {
        //       showDialog(
        //           context: context,
        //           builder: (BuildContext context) {
        //             return ObservationDetailDialog(
        //                 Observation(
        //                     id: 0,
        //                     date: DateTime.now(),
        //                     location: '',
        //                     description: '',
        //                     isMine: true),
        //                 true);
        //           });
        //     })
        );
  }

  String getItemDescription(Location item) {
    String desc = item.name;
    // if (desc.isEmpty) {
    //   desc = item.scientificName ?? '';
    // }
    // desc += (item.isMine ? '' : ' [not mine]');
    return desc;
  }




  buildLocationsList() {
    LocationsDao db = Provider.of<MoorDb>(context).locationsDao;
    return Expanded(
        child: ListView.builder(
            itemCount: locations.length,
            itemBuilder: (_, index) {
              Location item = locations[index];
              return Dismissible(
                  key: UniqueKey(),
                  confirmDismiss: (_) => showConfirmDeleteDialog(() {
                        db.deleteLocation(item.id).then((value) {
                          runFilterByText(txtFilter.value.text);
                          setState(() {});
                        });
                      }),
                  child: ListTile(
                      title: Text(getItemDescription(item)),
                      //subtitle: Text(formatter.format(item.date)),
                      //trailing: Icon(Icons.edit),
                      // onTap: () {
                      //   showDialog(
                      //       context: context,
                      //       builder: (_) {
                      //         return ObservationDetailDialog(
                      //             locations[index], false);
                      //       });
                      // }
                      ));
            }));
  }

  Future runFilterByText(String? text) async {
    LocationsDao db =
        Provider.of<MoorDb>(context, listen: false).locationsDao;
    locations = await db.getLocationsByText(text);
    setState(() {});
  }
  
}
