// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/tags_dao.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';
import 'package:provider/provider.dart';

class TagsScreen extends StatefulWidget {
  const TagsScreen({Key? key}) : super(key: key);

  @override
  _TagsScreenState createState() => _TagsScreenState();
}

class _TagsScreenState extends State<TagsScreen> {
  List<Tag> tags = [];

  @override
  void initState() {
    reloadTags();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    TagsDao tagsDao = Provider.of<MoorDb>(context, listen: false).tagsDao;
    return Scaffold(
        appBar: AppBar(title: Text('Tags')),
        drawer: MenuDrawer(),
        bottomNavigationBar: GTBottomNavBar(),
        body: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(children: [
              Expanded(child: buildTagsFutureBuilder(tags, tagsDao))
            ])));
  }

  ListView buildTagsFutureBuilder(List<Tag> tags, TagsDao tagsDao) {
    return ListView.builder(
      itemCount: tags.length,
      itemBuilder: (context, index) {
        Tag item = tags[index];
        return Dismissible(
          key: UniqueKey(),
          onDismissed: (_) =>
              tagsDao.deleteTagById(item.id).then((_) => reloadTags()),
          child: ListTile(
            title: Text(item.tag),
            subtitle: Text(item.type),
            // onTap: () {
            //   Navigator.push(
            //       context,
            //       MaterialPageRoute(
            //           builder: (context) => FavoriteDetailScreen(item)));
            // },
          ),
        );
      },
    );
  }

  void reloadTags() {
    TagsDao tagsDao = Provider.of<MoorDb>(context, listen: false).tagsDao;
    tagsDao.getTags().then((value) {
      tags = value;
      setState(() {});
    });
  }
}
