// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';
import 'package:martincica_flutter_demo/widget/species_widget.dart';
import 'package:martincica_flutter_demo/screens/mixin/preferences_mixin.dart';
import 'mixin/search_species_mixin.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen>
    with PreferencesMixin, SearchGbifSpeciesMixin {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title:
              widgetRow(
                  Text("Search kingdom: "),
                  buildTaxonChooserDropdown(
                      dropDownColor: Theme.of(context).primaryColor,
                      textColor: Colors.white70
                  ))),
      drawer: MenuDrawer(),
      bottomNavigationBar: GTBottomNavBar(),
      body: Padding(
        padding: EdgeInsets.all(16),
        child: ListView(
          children: [
            widgetRow3(searchBox(), buildDropdown(list), favoriteButton(context)),
            SpeciesWidget(gbifSpecies, key: ValueKey(gbifSpecies.key))
          ],
        ),
      ),
    );
  }

}
