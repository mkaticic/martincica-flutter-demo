// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/data/model/api/dbpedia_api.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/observations_dao.dart';
import 'package:martincica_flutter_demo/screens/details/observation_detail.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';
import 'package:provider/provider.dart';

import 'mixin/filter_screen_mixin.dart';

class ObservationsScreen extends StatefulWidget {
  const ObservationsScreen({Key? key}) : super(key: key);

  @override
  _ObservationsScreenState createState() => _ObservationsScreenState();
}

class _ObservationsScreenState extends State<ObservationsScreen>
    with UtilsMixin, FilterScreenMixin {
  final DateFormat formatter = DateFormat('yyyy-MM-dd');

  List<Observation> observations = [];

  @override
  void initState() {
    runFilterByText(txtFilter.value.text);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title:
                buildFilterableTitle(
                  screenTitle: 'Observations',
                  filterFunction: (text) => runFilterByText(text)),
              actions: [monthYearPicker()],
        ),
        drawer: MenuDrawer(),
        bottomNavigationBar: GTBottomNavBar(),
        body: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              buildObservationsList()
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return ObservationDetailDialog(
                        Observation(
                            id: 0,
                            date: DateTime.now(),
                            location: '',
                            description: '',
                            isMine: true),
                        true);
                  });
            }));
  }

  String getItemDescription(Observation item) {
    String separator = item.scientificName != null &&
            (item.scientificName ?? '').isNotEmpty &&
            item.description.isNotEmpty
        ? ': '
        : '';
    String commonNamePart = (item.description ?? '').trim();
    if (commonNamePart.isNotEmpty) {
      commonNamePart = commonNamePart.capitalize();
    }
    String sciNamePart = item.scientificName ?? '';
    if (sciNamePart.isNotEmpty) {
      sciNamePart = sciNamePart.capitalize();
    }
    String desc = '$sciNamePart$separator$commonNamePart';
    if (desc.isNotEmpty) {
      desc = desc.capitalize();
    }
    desc += (item.isMine ? '' : ' [not mine]');
    return desc;
  }

  buildObservationsList() {
    ObservationsDao db = Provider.of<MoorDb>(context).observationsDao;
    return Expanded(
        child: ListView.builder(
            itemCount: observations.length,
            itemBuilder: (_, index) {
              Observation item = observations[index];
              return Dismissible(
                  key: UniqueKey(),
                  confirmDismiss: (_) => showConfirmDeleteDialog(() {
                        db.deleteRow(item).then((value) {
                          runFilterByText(txtFilter.value.text);
                          setState(() {});
                        });
                      }),
                  // onDismissed: (_) {
                  //   db.deleteRow(item).then((value) => setState(() {}));
                  // },
                  child: ListTile(
                      title: Text(getItemDescription(item)),
                      subtitle: Text(formatter.format(item.date)),
                      trailing: Icon(Icons.edit),
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (_) {
                              return ObservationDetailDialog(
                                  observations[index], false);
                            });
                      }));
            }));
  }

  Future runFilterByText(String? text) async {
    ObservationsDao db =
        Provider.of<MoorDb>(context, listen: false).observationsDao;
    observations = await db.getObservationsByText(text);
    setState(() {});
  }

  @override
  void filterByDate(DateTime? value) {
    ObservationsDao db = Provider.of<MoorDb>(context, listen: false).observationsDao;
    if(value != null){
      db.getObservationsByDate(value)
          .then((value) {
            observations = value;
            setState(() {
            });
      } );
    } else {
      db.getAll()
          .then((value) {
        observations = value;
        setState(() {});
      });
    }
  }

}
