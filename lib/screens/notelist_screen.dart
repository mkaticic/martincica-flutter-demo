// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/model/db/sqlite/note.dart';
import 'package:martincica_flutter_demo/data/sqlite_db.dart';
import 'package:martincica_flutter_demo/screens/details/note_detail.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';

class NotelistScreen extends StatefulWidget {
  const NotelistScreen({Key? key}) : super(key: key);

  @override
  _NotelistScreenState createState() => _NotelistScreenState();
}

class _NotelistScreenState extends State<NotelistScreen> {
  SqliteDb db = SqliteDb();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Notes')),
        drawer: MenuDrawer(),
        bottomNavigationBar: GTBottomNavBar(),
        body: FutureBuilder(
            future: getNotes(),
            builder: (context, snapshot) {
              List<Note> notes =
                  snapshot.data == null ? [] : snapshot.data as List<Note>;
              if (notes == null) {
                return Container();
              }
              // else
              return ReorderableListView(
                  onReorder: (oldIndex, newIndex) async {
                    final Note note = notes[oldIndex];
                    if (newIndex > oldIndex) {
                      newIndex -= 1; // fix for ReorderableListView bug
                    }
                    await db.reorder(note, newIndex, oldIndex).then((_) {
                      setState(() {});
                    });
                  },
                  children: [
                    for (final note in notes)
                      Dismissible(
                          key: UniqueKey(),
                          onDismissed: (_) => db
                              .deleteNote(note)
                              .then((value) => setState(() {})),
                          child: Card(
                            key: ValueKey(note.position.toString()),
                            child: ListTile(
                                title: Text(note.name),
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) =>
                                              NoteScreen(note, false)));
                                }),
                          ))
                  ]);
            }),
        floatingActionButton: FloatingActionButton(
            child: Icon(Icons.add),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return NoteScreen(Note('', '', '', 1), true);
                  });
            }));
  }

  Future<List<Note>> getNotes() async {
    List<Note> notes = await db.getNotes();
    return notes;
  }
}
