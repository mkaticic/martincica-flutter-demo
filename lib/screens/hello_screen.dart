// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';

class HelloScreen extends StatefulWidget {
  const HelloScreen({Key? key}) : super(key: key);

  @override
  _HelloScreenState createState() => _HelloScreenState();
}

class _HelloScreenState extends State<HelloScreen> {
  String name = 'Mirna';
  final TextEditingController txtName = TextEditingController();

  @override
  Widget build(BuildContext context) {
    txtName.text = name;

    return Scaffold(
        drawer: MenuDrawer(),
        bottomNavigationBar: GTBottomNavBar(),
        body: Center(
          child: Container(
            padding: const EdgeInsets.all(100.0),
            child: Column(children: [
              TextField(controller: txtName, textAlign: TextAlign.center),
              ElevatedButton(
                  child: Text('Say hello'),
                  onPressed: () {
                    setState(() {
                      name = txtName.text;
                    });
                  }),
              Text('Hello, ' + name)
            ]),
          ),
        ));
  }
}
