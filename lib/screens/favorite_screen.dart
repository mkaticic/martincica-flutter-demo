// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/favorite_species_dao.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/screens/mixin/favorite_tags_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/filter_screen_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';
import 'package:provider/provider.dart';

import 'details/favorite_detail.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen>
    with FavoriteTagsMixin, UtilsMixin, FilterScreenMixin {
  List<FavoriteSpecy> favorites = [];

  @override
  void initState() {
    FavoriteSpeciesDao speciesDao =
        Provider.of<MoorDb>(context, listen: false).favoriteSpeciesDao;
    speciesDao.getFavorites().then((value) {
      favorites = value;
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FavoriteSpeciesDao speciesDao =
        Provider.of<MoorDb>(context).favoriteSpeciesDao;
    return Scaffold(
        appBar: AppBar(title: buildFilterableTitle(
                screenTitle: 'Favorites',
                filterFunction: (text) => runFilterByText(text))),
        drawer: MenuDrawer(),
        bottomNavigationBar: GTBottomNavBar(),
        body: Padding(
            padding: const EdgeInsets.all(10),
            child: Column(children: [
              Expanded(child: buildFavoriteFutureBuilder(favorites, speciesDao))
            ])));
  }

  ListView buildFavoriteFutureBuilder(
      List<FavoriteSpecy> favorites, FavoriteSpeciesDao speciesDao) {
    return ListView.builder(
      itemCount: favorites.length,
      itemBuilder: (context, index) {
        FavoriteSpecy item = favorites[index];
        DateFormat formatter = DateFormat('dd/MM/yyyy');
        String postDate =
            (item.created != null) ? formatter.format(item.created!) : '';
        return Dismissible(
          key: UniqueKey(),
          onDismissed: (_) => speciesDao
              .deleteFavorite(item)
              .then((_) => runFilterByText(txtFilter.value.text)),
          child: ListTile(
            title: Text(item.canonicalName),
            subtitle: Text(postDate),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FavoriteDetailScreen(item)));
            },
          ),
        );
      },
    );
  }

  Future runFilterByText(String? text) async {
    FavoriteSpeciesDao db =
        Provider.of<MoorDb>(context, listen: false).favoriteSpeciesDao;
    favorites = await db.getFavoritesByTag(text);
    setState(() {});
  }

}
