// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/blog_post_dao.dart';
import 'package:martincica_flutter_demo/screens/details/post_detail.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';
import 'package:provider/provider.dart';
import '../data/drift_db.dart';

class PostsScreen extends StatefulWidget {
  @override
  _PostsScreenState createState() => _PostsScreenState();
}

class _PostsScreenState extends State<PostsScreen> {
  List<BlogPost> posts = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    BlogPostDao db = Provider.of<MoorDb>(context).blogPostDao;

    return Scaffold(
      appBar: AppBar(title: Text('Blog Posts')),
      drawer: MenuDrawer(),
      bottomNavigationBar: GTBottomNavBar(),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            BlogPost post = BlogPost(id: 0, name: '', content: '', date: null);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => PostDetailScreen(post, true)));
          }),
      body: FutureBuilder(
        future: db.getPosts(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            posts = snapshot.data as List<BlogPost>;
          } else {
            posts = [];
          }

          return ListView.builder(
            itemCount: posts.length,
            itemBuilder: (context, index) {
              DateFormat formatter = DateFormat('dd/MM/yyyy');
              String postDate = (posts[index].date != null)
                  ? formatter.format(posts[index].date!)
                  : '';
              return Dismissible(
                key: UniqueKey(),
                onDismissed: (_) => db
                    .deletePost(posts[index])
                    .then((value) => setState(() {})),
                child: ListTile(
                  title: Text(posts[index].name),
                  subtitle: Text(postDate),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                PostDetailScreen(posts[index], false)));
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
