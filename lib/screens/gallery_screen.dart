// ignore_for_file: prefer_const_constructors, use_key_in_widget_constructors


import 'dart:io';

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/sp_helper.dart';
import 'package:martincica_flutter_demo/screens/details/osm_detail_from_exif_data.dart';
import 'package:martincica_flutter_demo/screens/mixin/osm_map_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/tags_mixin.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';
import 'package:martincica_flutter_demo/utils/exif_utils.dart';

import 'mixin/gallery_mixin.dart';

class GalleryScreen extends StatefulWidget {
  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen>
    with TagsMixin, OsmMapMixin, GalleryMixin {
  SharedPreferencesHelper settings = SharedPreferencesHelper();

  @override
  void initState() {
    settings.init().then((_) {
      galleryPath = settings.getStr(settings.photoGalleryDir);
      createGallery(galleryPath);
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(title: Text('Photos'), actions: [showMapAction(galleryPath)],),
        drawer: MenuDrawer(),
        bottomNavigationBar: GTBottomNavBar(),
        body: SingleChildScrollView(
            child: Column(children: [photoGalleryDirPicker(), buildGallery(context)])));
  }

  photoGalleryDirPicker() {
    Widget row = Padding(
        padding: EdgeInsets.all(10),
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          chooseGalleryButton("Main gallery", settings.photoGalleryDir),
          chooseGalleryButton("Observations", settings.observationsDir),
          addSortButton()
        ]));
    return row;
  }

  chooseGalleryButton(String buttonText, String galleryKey) {
    return Expanded(
        flex: 5,
        child: Padding(
          padding: EdgeInsets.all(8),
          child: ElevatedButton(
              onPressed: () {
                galleryPath = settings.getStr(galleryKey);
                createGallery(galleryPath);
                setState(() {});
              },
              child: Text(buttonText)),
        ));
  }

  showMapAction(String? galleryPath) {
    if(galleryPath == null){
      return Container();
    }
    return IconButton(
            onPressed: () {
              List<ExifData> exifData = [];
              fileHelper
                  .getImages(galleryPath, sortDesc)
                  .then((List<File> files) => {
                    files.forEach((image) {
                      ExifUtils.getExifData(image).then((value) => {
                        exifData.add(value)
                      });
                    })
                  });

                Navigator.push(context,
                    MaterialPageRoute(builder: (context) =>
                        OsmDetailFromExifData(exifData)));
            },
            icon: const Icon(Icons.map));
  }
}
