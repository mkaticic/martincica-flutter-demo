// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:package_info_plus/package_info_plus.dart';

import '../shared/bottom_nav_bar.dart';
import '../shared/menu_drawer.dart';
import 'mixin/utils_mixin.dart';

class InfoScreen extends StatefulWidget {
  const InfoScreen({Key? key}) : super(key: key);

  @override
  _InfoScreenState createState() => _InfoScreenState();
}

class _InfoScreenState extends State<InfoScreen> with UtilsMixin{

  var initialized = false;

  String appName = '';
  String packageName = '';
  String version = '';
  String buildNumber = '';

  @override
  void initState() {
    getInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if(!initialized){
      getInfo();
    }
    return Scaffold(
          appBar: AppBar(title: Text('Info')),
          drawer: MenuDrawer(),
          bottomNavigationBar: GTBottomNavBar(),
          body: SingleChildScrollView(
            padding: EdgeInsets.only(top: 30, left: 10),
            child: Column(
              children: [
              boldedTextColored("App info"),
              Divider(),
              row('App: ', Text(appName)),
              row('version: ', Text(version)),
              row('build number: ', Text(buildNumber)),
              Divider(),
              boldedTextColored("Integrated APIs"),
              Divider(),
              row('GBIF: ', urlText('https://api.gbif.org')),
              row('DBpedia: ', urlText('https://dbpedia.org')),
              row('Latin WordNet: ',
                  urlText('https://latinwordnet.exeter.ac.uk')),
              row('Open-meteo: ', urlText('https://open-meteo.com')),
              Divider(),
              boldedTextColored("Libraries"),
              Divider(),
              row(
                  'flutter tags:',
                  urlText(
                      'https://pub.dev/packages/flutter_tags/versions/1.0.0-nullsafety.1')),
            ],
            ),
          )
        );
  }


  void getInfo() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        appName = packageInfo.appName;
        packageName = packageInfo.packageName;
        version = packageInfo.version;
        buildNumber = packageInfo.buildNumber;
        initialized = true;
      });
    });
  }

}
