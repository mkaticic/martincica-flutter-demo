// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

mixin UrlMixin<S extends StatefulWidget> on State<S> {
  buildUrl(String? url, {String prefix = ''}) {
    if (url == null || url.isEmpty) {
      return Container();
    }
    final Uri _url = Uri.parse(url ?? '');
    return MaterialButton(
        onPressed: () => _launchUrl(_url),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Text(Uri.parse(url ?? '-').host,
                style: TextStyle(color: Colors.blue))));
  }

  String webSearchString(String prefix, String? text) {
    return prefix + (text ?? '');
  }

  Future<void> _launchUrl(_url) async {
    if (!await launchUrl(_url)) {
      throw 'Could not launch $_url';
    }
  }
}
