// ignore_for_file: prefer_const_constructors

import 'package:drift/drift.dart';
import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/favorite_tags_dao.dart';
import 'package:martincica_flutter_demo/ext/flutter_tags-null-safety/lib/flutter_tags.dart';
import 'package:provider/provider.dart';

mixin FavoriteTagsMixin<S extends StatefulWidget> on State<S> {
  List<FavoriteTag>? favoriteTags;
  List<String>? favoriteTagSuggestions;
  final double tagFontSize = 24;
  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();

  buildTags(int gbifSpeciesId) {
    return Tags(
      key: _tagStateKey,

      textField: TagsTextField(
        textStyle: TextStyle(fontSize: tagFontSize),
        constraintSuggestion: false, suggestions: favoriteTagSuggestions,
        //width: double.infinity, padding: EdgeInsets.symmetric(horizontal: 10),
        onSubmitted: (String tag) {
          // required
          insert(
              context,
              FavoriteTagsCompanion(
                  favoriteSpeciesId: Value(gbifSpeciesId),
                  tag: Value(tag),
                  active: Value(true)))
              .then((_) => reloadTags(gbifSpeciesId));
        },
      ),
      itemCount: favoriteTags!.length, // required
      itemBuilder: (int index) {
        final item = favoriteTags![index];

        return ItemTags(
          // Each ItemTags must contain a Key. Keys allow Flutter to
          // uniquely identify widgets.
          key: Key(index.toString()),
          index: index,
          // required
          title: item.tag,
          active: true,
          customData: 'ds',
          textStyle: TextStyle(
            fontSize: tagFontSize,
          ),
          combine: ItemTagsCombine.withTextBefore,
          // image: ItemTagsImage(
          //     image: AssetImage("img.jpg") // OR NetworkImage("https://...image.png")
          // ), // OR null,
          // icon: ItemTagsIcon(
          //   icon: Icons.add,
          // ), // OR null,
          removeButton: ItemTagsRemoveButton(
            onRemoved: () {
              // Remove the item from the data source.
              if (favoriteTags != null) {
                deleteTag(gbifSpeciesId, favoriteTags![index].tag)
                    .then((value) {
                  favoriteTags = null;
                  setState(() {});
                });
                return true;
              }
              return false;
            },
          ),
          // OR null,
          onPressed: (item) => print(item),
          onLongPressed: (item) => print(item),
        );
      },
    );
  }

  Future<List<FavoriteTag>> getTags(int speciesId) async {
    FavoriteTagsDao db =
        Provider.of<MoorDb>(context, listen: false).favoriteTagsDao;
    return await db.getTagsByFavoriteSpeciesId(speciesId);
  }

  Future insert(BuildContext context, FavoriteTagsCompanion favoriteTagsCompanion) async {
    FavoriteTagsDao db =
        Provider.of<MoorDb>(context, listen: false).favoriteTagsDao;
    await db.insert(favoriteTagsCompanion);
  }

  Future<List<String>> getSuggestions() async {
    FavoriteTagsDao db =
        Provider.of<MoorDb>(context, listen: false).favoriteTagsDao;
    List<String> sugesstions = await db.getSuggestions();
    return sugesstions;
  }

  Future<bool> deleteTag(int gbifSpeciesId, String tag) async {
    FavoriteTagsDao db =
        Provider.of<MoorDb>(context, listen: false).favoriteTagsDao;
    await db.deleteTag(gbifSpeciesId, tag);
    return true;
  }

  reloadTags(int gbifSpeciesId) {
    getTags(gbifSpeciesId).then((value) {
      favoriteTags = value;
      setState(() {});
    });
  }

  void reloadFavoriteTagSuggestions() {
    getSuggestions().then((suggestions) {
      favoriteTagSuggestions = suggestions;
      setState(() {});
    });
  }

}
