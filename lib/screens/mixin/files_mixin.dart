
import 'dart:io';

import 'package:flutter/material.dart';

import '../../data/file_helper.dart';

mixin FilesMixin<S extends StatefulWidget> on State<S> {
  FileHelper helper = FileHelper();
  List<File> files = [];

  getFiles(){
    return helper.geFiles();
  }

  deleteFileAtIndex(index){
    helper.deleteFile(files[index]);
  }

  Future deleteFile(path){
    return helper.deleteFile(File(path));
  }
}