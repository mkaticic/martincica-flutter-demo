import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:month_picker_dialog/month_picker_dialog.dart';

mixin FilterScreenMixin<S extends StatefulWidget> on State<S>, UtilsMixin<S> {
  final TextEditingController txtFilter = TextEditingController();
  final FocusNode txtFilterFocusNode = FocusNode();

  Widget buildFilter(ValueChanged<String>? onChanged) {
    return Row(children: [
      Expanded(child:
            addTextfield(' ...enter filter text here', txtFilter,
              focusNode: txtFilterFocusNode,
              onChanged: onChanged,
              textColor: Colors.white,
              textFontSize: Theme.of(context).textTheme.bodyLarge?.fontSize,
              hintColor: Colors.white70,
              hintStyle: FontStyle.italic)),
          IconButton(
            onPressed: () {
              closeTxtSearchFilter();
              onChanged!.call("");
            },
            icon: const Icon(Icons.clear))
      ]);
  }

  bool isTxtSearchFilterOn = false;

  buildFilterableTitle({
    String screenTitle = "Title",
    ValueChanged<String>? filterFunction
  }) {
    if(isTxtSearchFilterOn){
      return buildFilter(filterFunction);
    }
    return buildStandardTitle(screenTitle);
  }

  buildStandardTitle(String title) {
    return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(title),
            actionIconButtonTxtSearch()
          ]);
  }

  actionIconButtonTxtSearch() {
    if(isTxtSearchFilterOn){
      return Container();
    }
    return IconButton(
                onPressed: () {
                  isTxtSearchFilterOn = true;
                  setState(() {
                    txtFilterFocusNode.requestFocus();
                  });
                },
                icon: const Icon(Icons.filter_alt));
  }

  void closeTxtSearchFilter() {
    isTxtSearchFilterOn = false;
    txtFilter.clear();
    setState(() {
    });
  }

  monthYearPicker() {
    return IconButton(
            onPressed: () {
              pickDate().then((value)
              {
                closeTxtSearchFilter();
                filterByDate(value);
              });
            },
            icon: const Icon(Icons.date_range));
  }

  Future<DateTime?> pickDate() async {
    return await showMonthPicker(
      context: context,
      initialDate: DateTime.now(),
      headerColor: Theme.of(context).primaryColorDark,
      unselectedMonthTextColor: Theme.of(context).primaryColorDark,
      selectedMonthBackgroundColor: Theme.of(context).primaryColorDark,
    );
  }

  void filterByDate(DateTime? value) {}


}