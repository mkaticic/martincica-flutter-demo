// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

mixin UtilsMixin<S extends StatefulWidget> on State<S> {
  addTextfield(
        String hintText,
        TextEditingController controler,
      {Widget? sufixIcon,
      FocusNode? focusNode,
      ValueChanged<String>? onChanged,
      ValueChanged<String>? onSubmitted,
      VoidCallback? onEditingComplete,
      TapRegionCallback? onTapOutside,
      Color? textColor,
      double? textFontSize,
      FontStyle? hintStyle,
      Color? hintColor}) {
    InputDecoration decoration = InputDecoration(
        hintText: hintText,
        suffixIcon: sufixIcon,
        hintStyle: TextStyle(color: hintColor, fontStyle: hintStyle));
    return TextField(
        focusNode: focusNode,
        controller: controler,
        style: TextStyle(color: textColor, fontSize: textFontSize),
        decoration: decoration,
        onChanged: onChanged,
        onSubmitted: onSubmitted,
        onTapOutside: onTapOutside,
        onEditingComplete: onEditingComplete);
  }

  addTextArea(String hintText, TextEditingController controler,
      {Widget? sufixIcon,
      FocusNode? focusNode,
      ValueChanged<String>? onChanged,
      ValueChanged<String>? onSubmitted,
      VoidCallback? onEditingComplete,
      TapRegionCallback? onTapOutside,
      Color? textColor,
      double? textFontSize,
      FontStyle? hintStyle,
      Color? hintColor,
      int? maxLines}) {
    InputDecoration decoration = InputDecoration(
        hintText: hintText,
        suffixIcon: sufixIcon,
        hintStyle: TextStyle(color: hintColor, fontStyle: hintStyle));
    // if(collapsed??false){
    //   decoration = InputDecoration.collapsed(
    //       hintText: hintText
    //     ).copyWith(isDense: true);
    // }
    return Card(
        color: Theme.of(context).primaryColorLight,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: TextField(
            focusNode: focusNode,
            controller: controler,
            style: TextStyle(color: textColor, fontSize: textFontSize),
            decoration: decoration,
            onChanged: onChanged,
            onSubmitted: onSubmitted,
            onTapOutside: onTapOutside,
            onEditingComplete: onEditingComplete,
            maxLines: maxLines,
            //or null
            keyboardType: TextInputType.multiline,
          ),
        ));
  }

  boldedText(String text) {
    return Text(text, style: TextStyle(fontWeight: FontWeight.bold));
  }

  boldedTextColored(String text) {
    return Text(text,
        style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor));
  }

  paddedHText(String text) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Text(text),
    );
  }

  paddedVText(String text) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Text(text),
    );
  }

  closeTextButton() {
    return TextButton(
        onPressed: () {
          Navigator.pop(context);
        },
        child: Text('Close'));
  }

  showConfirmDeleteDialog(Function onConfirm) async {
    return await showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Confirm DELETE action"),
          content: const Text("Are you sure you wish to delete this item?"),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop(true);
                  onConfirm();
                },
                child: const Text("DELETE")),
            ElevatedButton(
              onPressed: () {
                setState(() {});
                Navigator.of(context).pop(false);
              },
              child: const Text("CANCEL"),
            ),
          ],
        );
      },
    );
  }

  expandedElevatedButton(
      {required Widget child, required VoidCallback onPressed}) {
    return Expanded(
        flex: 5,
        child: Padding(
          padding: EdgeInsets.all(6),
          child: ElevatedButton(onPressed: onPressed, child: child),
        ));
  }

  urlText(String url) {
    return InkWell(child: Text(url), onTap: () => launchUrl(Uri.parse(url)));
  }

  Widget row(String text, Widget widget) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Text(text, style: TextStyle(fontWeight: FontWeight.bold)),
          ),
          Expanded(flex: 6, child: widget),
        ],
      ),
    );
  }
}
