// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';
import 'package:martincica_flutter_demo/screens/mixin/preferences_mixin.dart';
import 'package:whatsapp_share/whatsapp_share.dart';

import '../../utils/exif_utils.dart';
import '../details/open_street_map_detail.dart';

mixin ShareWhatsupMixin<S extends StatefulWidget> on State<S>, PreferencesMixin<S>  {
  
  buildShare(String path) {
    return IconButton(onPressed: () => shareFile(path).then((value){
      setState(() {
      });
    }), icon: Icon(Icons.share));
  }

   Future<void> shareFile(String path) async {
    String? phone = settings.getStr(settings.whatsupPhoneNumber);
    if (phone == null) {
      return;
    }
    await WhatsappShare.shareFile(
      text: 'Whatsapp share text',
      phone: phone,
      filePath: [path],
    );
  }
}
