// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/sp_helper.dart';

mixin PreferencesMixin<S extends StatefulWidget> on State<S> {

  SharedPreferencesHelper settings = SharedPreferencesHelper();

  Color getPreferedColor(){
    return Color(settings.getColor());
  }

  bool isDebugModeOn(){
    return settings.getBool(settings.debugModeOnKey)?? false;
  }

}
