// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

mixin GoogleSearchMixin<S extends StatefulWidget> on State<S> {

  var google_search = 'https://www.google.com/search?q=';

  buildWebSearchIconButton(String? query, {String prefix=''}) {
    if(query == null || query.isEmpty){
      return Container();
    }
    final Uri _url = Uri.parse(google_search + prefix + (query??''));
    return IconButton(
            onPressed: () => _launchUrl(_url),
            icon: Icon(Icons.search));
  }

  String webSearchString(String prefix, String? text) {
    return prefix + (text??'');
  }

  Future<void> _launchUrl(_url) async {
    if (!await launchUrl(_url)) {
      throw 'Could not launch $_url';
    }
  }

}
