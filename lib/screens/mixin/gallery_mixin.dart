// ignore_for_file: prefer_const_constructors

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:martincica_flutter_demo/data/file_helper.dart';
import 'package:martincica_flutter_demo/screens/details/gallery_item_detail.dart';
import 'package:martincica_flutter_demo/screens/mixin/osm_map_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/tags_mixin.dart';
import 'package:martincica_flutter_demo/utils/exif_utils.dart';


mixin GalleryMixin<S extends StatefulWidget>
    on State<S>, TagsMixin<S>, OsmMapMixin<S> {
  List<Widget> images = [];
  FileHelper fileHelper = FileHelper();

  String? galleryPath;
  bool sortDesc = true;

  addSortButton() {
    return Expanded(
        flex: 2,
        child: Padding(
            padding: EdgeInsets.all(8),
            child: IconButton(
              onPressed: () {
                setState(() {
                  sortDesc = !sortDesc;
                  createGallery(galleryPath);
                });
              },
              icon: Icon(Icons.sort),
            )));
  }

  createGallery(String? path) {
    if (path == null) return [];
    fileHelper
        .getImages(path, sortDesc)
        .then((List<File> files) => createGalleryFromFiles(files));
  }

  createGalleryFromFiles(List<File> files) {
    images = [];
    for (var file in files) {
      images.add(buildImageItem(file, null, null));
    }
  }

  buildGallery(BuildContext context, { double? scalehight}) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height * (scalehight??1);
    return SizedBox(
        width: sizeX,
        height: sizeY,
        child: GridView(
          children: images,
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.all(5),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            mainAxisSpacing: 5,
            crossAxisSpacing: 5,
          )
        ));
  }

  Widget buildImageItem(File file, TagType? tagType, String? refId) {
    return GestureDetector(
      onTap: () => showGaleryItemDetail(file.path, tagType, refId),
      onLongPress: () => openExternalMapAppFromGPSData(file),
      child: Container(
          decoration: BoxDecoration(
              image:
                  DecorationImage(fit: BoxFit.cover, image: FileImage(file))),
          child: buildQuickActionBar(file, FractionalOffset.bottomRight)),
    );
  }

  openOpenStreetMapFromGPSData(File file) {
    ExifUtils.checkGPSData(file).then((point) {
      if (point != null) {
        openOSMapDetailPage(point);
      } else {
        showDialog(
            context: context,
            builder: (_) => AlertDialog(
                  title: Text("Map can't be opened"),
                  content: Text("no GPS data found"),
                ));
      }
    });
  }

  openExternalMapAppFromGPSData(File file) {
    ExifUtils.checkGPSData(file).then((point) {
      if (point != null) {
        MapsLauncher.launchCoordinates(point.latitude, point.longitude);
      } else {
        showDialog(
            context: context,
            builder: (_) => AlertDialog(
                  title: Text("Map can't be opened"),
                  content: Text("no GPS data found"),
                ));
      }
    });
  }

  showGaleryItemDetail(String path, TagType? tagType, String? refId) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return GaleryItemDetail(
              path,
              tagType: tagType,
              tagRefId: refId,
              onDeleteAction: () => reloadPhotos(refId??'')
          );
        });
  }

  buildQuickActionBar(File file, FractionalOffset alignment) {
    return Align(
        alignment: alignment,
        child: Row(mainAxisAlignment: MainAxisAlignment.end, children: [
          quickButton(() => openOpenStreetMapFromGPSData(file), Icons.map),
          quickButton(
              () => openExternalMapAppFromGPSData(file), Icons.arrow_right_alt)
        ]));
  }

  Align quickButton(VoidCallback? action, IconData icon) {
    return Align(
        alignment: Alignment.bottomRight,
        child: Container(
            padding: EdgeInsets.all(1),
            margin: EdgeInsets.symmetric(horizontal: 1),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(5)),
                color: Colors.white38),
            child: IconButton(icon: Icon(icon), onPressed: action)));
  }

  void reloadPhotos(String refId) {
    getTags(TagType.observation_photo, refId)
        .then((paths) {
          images = [];
          for (var element in paths) {
            images.add(buildImageItem(
                File(element.tag),
                TagType.observation_photo,
                refId));
          }
          setState(() {});
        });
  }
}
