// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_cancellable_tile_provider/flutter_map_cancellable_tile_provider.dart';
import 'package:latlong2/latlong.dart';

import '../../utils/exif_utils.dart';
import '../details/open_street_map_detail.dart';

mixin OsmMapMixin<S extends StatefulWidget> on State<S> {
  final String osmTemplate =
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

  TileLayer get openStreetMapTileLayer => TileLayer(
        urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
        userAgentPackageName: 'GEOTropa-mobile-app',
        // Use the recommended flutter_map_cancellable_tile_provider package to
        // support the cancellation of loading tiles.
        tileProvider: CancellableNetworkTileProvider(),
      );

  getCircleMarkers(LatLng? point) {
    List<CircleMarker> markers = [];
    if (point == null) return markers;
    return <CircleMarker>[
      CircleMarker(
          point: point,
          color: Colors.blue.withOpacity(0.7),
          borderStrokeWidth: 2,
          useRadiusInMeter: true,
          radius: 50 // 50 meters
          )
    ];
  }

  List<CircleMarker> getCircleMarkersFromExifData(List<ExifData> exifData) {
    List<CircleMarker> makers = [];
    exifData.forEach((element) {
      makers.addAll(getCircleMarkers(element!.point));
    });
    return makers;
  }

  getOverlayImages(LatLng? point) {
    if (point == null) return [];
    return <OverlayImage>[
      OverlayImage(
          bounds: LatLngBounds.fromPoints(<LatLng>[point, transformNE(point)]),
          imageProvider: AssetImage('assets/images/pin.jpg')),
    ];
  }

  buildFlutterMap(LatLng point) {
    return FlutterMap(
        options: MapOptions(
          initialCenter: point,
          initialZoom: 15.0,
        ),
        children: [
          openStreetMapTileLayer,
          CircleLayer(
            circles: [
              CircleMarker(
                point: point,
                color: Colors.blue.withOpacity(0.7),
                borderColor: Colors.black,
                borderStrokeWidth: 6,
                useRadiusInMeter: true,
                radius: 30, // 2000 meters
              )
            ],
          ),
        ]);
  }

  buildFlutterMapFromExifData(List<ExifData> exifData) {
    if (exifData.first.point == null) {
      return Text("no exif location data found");
    }
    return buildFlutterMap(exifData.first.point ?? LatLng(0, 0));
  }

  transformNE(LatLng point) {
    LatLng newPoint = LatLng(point.latitude, point.longitude + 0.01);
    return newPoint;
  }

  void openOSMapDetailPage(LatLng? point) {
    if(point == null) return;
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => OpenStreetMapDetail(point)));
  }
}
