// ignore_for_file: prefer_const_constructors, sized_box_for_whitespace

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_typeahead/flutter_typeahead.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif/gbif_species.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif_api.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/favorite_species_dao.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/mapper/species_mapper.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/screens/mixin/preferences_mixin.dart';
import 'package:provider/provider.dart';

import '../favorite_screen.dart';

mixin SearchGbifSpeciesMixin<S extends StatefulWidget> on State<S>, PreferencesMixin<S> {

  GbifTaxon taxonKey = GbifTaxon.fungi;
  GbifSpecies gbifSpecies = GbifSpecies();
  List<GbifSpecies> list = [];
  TextEditingController txtSearch = TextEditingController();

  widgetRow(Widget w1, Widget w2) {
    return Row(
      children: [
        Expanded(flex: 4, child: w1),
        Expanded(flex: 3, child: w2),
      ],
    );
  }

  widgetRow3(Widget w1, Widget w2, Widget w3) {
    if(gbifSpecies.key == null){
      return Row(
        children: [
          Expanded(flex: 4, child: w1),
          Expanded(flex: 2, child: w2),
          Expanded(flex: 0, child: w3),
        ],
      );
    }
    return Row(
      children: [
        Expanded(flex: 3, child: w1),
        Expanded(flex: 4, child: w2),
        Expanded(flex: 1, child: w3),
      ],
    );
  }

  Future getSpecies(int? gbfKey) async {
    if (gbfKey == null) {
      return;
    }
    GbifApi helper = GbifApi();
    gbifSpecies = await helper.getSpeciesByKey(gbfKey);
  }

  Future<void> getData() async {
    GbifApi helper = GbifApi();
    list = await helper.searchSpecies(txtSearch.text, taxonKey: taxonKey);
    if (list.isEmpty) {
      gbifSpecies = GbifSpecies();
    } else {
      gbifSpecies = helper.findAcceptedOrFirst(list);
      settings.addSuggestion(settings.ddHistorySearchKey, txtSearch.text);
    }
    setState(() {});
  }

  buildDropdown(List<GbifSpecies> list) {
    if (list.isEmpty) {
      return Container();
    }
    list.sort((i1, i2) => i1.canonicalName.compareTo(i2.canonicalName));
    return Container(
        height: 48,
        child: DropdownButton<GbifSpecies>(
            isExpanded: true,
            value: gbifSpecies,
            onChanged: (GbifSpecies? newValue) {
              setState(() {
                gbifSpecies = newValue!;
              });
            },
            items: list.map<DropdownMenuItem<GbifSpecies>>((GbifSpecies value) {
              return DropdownMenuItem<GbifSpecies>(
                value: value,
                child: Text(value.canonicalName),
              );
            }).toList()));
  }

  buildTaxonChooserDropdown({Color? dropDownColor, Color? textColor}) {
    return Container(
        height: 48,
        child: DropdownButton<GbifTaxon>(
            isExpanded: true,
            value: taxonKey,
            dropdownColor: dropDownColor,
            style: TextStyle(
                color: textColor,
                fontSize: Theme.of(context).textTheme.titleLarge?.fontSize),
            onChanged: (GbifTaxon? newValue) {
              setState(() {
                taxonKey = newValue!;
              });
            },
            items: GbifTaxon.values
                .map<DropdownMenuItem<GbifTaxon>>((GbifTaxon value) {
              return DropdownMenuItem<GbifTaxon>(
                  value: value, child: Text(value.name));
            }).toList()));
  }

  Widget searchBox() {
    return Padding(
        padding: EdgeInsets.only(bottom: 16, right: 16),
        child: TypeAheadField(
          textFieldConfiguration: TextFieldConfiguration(
            controller: txtSearch,
            onTap: () => txtSearch.clear(),
            autofocus: false,
            decoration: InputDecoration(
                hintText: 'search',
                suffixIcon: IconButton(
                  icon: Icon(Icons.search),
                  onPressed: getData,
                )),
          ),
          suggestionsCallback: (pattern) async {
            return await settings.getSuggestions(
                settings.ddHistorySearchKey, pattern);
          },
          itemBuilder: (context, suggestion) {
            return ListTile(title: Text(suggestion!.toString()));
          },
          noItemsFoundBuilder: (context) => Text(''),
          onSuggestionSelected: (suggestion) {
            txtSearch.text = suggestion!.toString();
            getData();
          },
        ));
  }

  Widget favoriteButton(BuildContext context) {
    if(gbifSpecies.key == null){
      return Container();
    }
    FavoriteSpeciesDao speciesDao =
        Provider.of<MoorDb>(context).favoriteSpeciesDao;
    return IconButton(
            color: getPreferedColor(),
            icon: Icon(CupertinoIcons.heart_solid),
            onPressed: () => saveToFavorites(speciesDao, gbifSpecies));
  }

  saveToFavorites(FavoriteSpeciesDao speciesDao, GbifSpecies gbifSpecies) {
    speciesDao
        .insertIfNew(SpeciesMapper.toFavoriteSpeciesCompanion(gbifSpecies));
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text(gbifSpecies.canonicalName + " added to Favorites"),
              actions: [
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text('Close')),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (_) => FavoriteScreen()));
                    },
                    child: Text('Show favorites'))
              ]);
        });
  }
}
