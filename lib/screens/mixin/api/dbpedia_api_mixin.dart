import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/model/api/dbpedia_api.dart';
import 'package:martincica_flutter_demo/data/model/api/dbpedia/myco_morph_box.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';

mixin DbpediaApiMixin<S extends StatefulWidget> on State<S>, UtilsMixin<S> {
  MycoMorphBox? mycoMorphBox;
  bool dbpediaDataInitialized = false;

  void initDbPedia(String gbifCanonicalName) {
    // dbpedia
    if (!dbpediaDataInitialized) {
      getDbpediaData(gbifCanonicalName).then((_) => () {
            dbpediaDataInitialized = true;
            setState(() {});
          });
    }
  }

  Future getDbpediaData(String gbifName) async {
    DbpediaApi api = DbpediaApi();
    mycoMorphBox = await api.getMycoMorphBox(gbifName);
  }

  mycoMorphBoxData() {
    if (mycoMorphBox == null) {
      return Row();
    }
    return ExpansionTile(
      title: const Text("Description:"),
      tilePadding: EdgeInsets.zero,
      initiallyExpanded: false,
      children: [
        Text(mycoMorphBox!.description.toString(),
            style: TextStyle(color: Theme.of(context).primaryColor)),
        morfoBoxRow("Spore print:", mycoMorphBox!.sporeprintcolor.toString()),
        morfoBoxRow("Gills:", mycoMorphBox!.whichgills.toString()),
        morfoBoxRow("Hymenium:", mycoMorphBox!.hymeniumtype.toString()),
        morfoBoxRow("Cap:", mycoMorphBox!.capshapem.toString()),
        morfoBoxRow("Stipe:", mycoMorphBox!.stipecharacter.toString()),
        morfoBoxRow(
            "Ecological type:", mycoMorphBox!.ecologicaltype.toString()),
        morfoBoxRow("Edible:", mycoMorphBox!.howedible.toString()),
      ],
    );
  }

  Widget morfoBoxRow(String label, String value) {
    if(value == '[]'){
      return Row();
    }
    Widget row = Padding(
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Row(
        children: [
          Expanded(
              flex: 3,
              child: Text(label,
                  style: TextStyle(color: Theme.of(context).hintColor))),
          Expanded(
              flex: 4,
              child: Text(value,
                  style: TextStyle(color: Theme.of(context).primaryColor)))
          // child: RichText(
          //   text: TextSpan(children: [
          //     TextSpan(
          //         text: value,
          //         // style: getValueTextStyle(gbifKey != null),
          //         // recognizer: getTapGestureRecognizer(gbifKey)
          //     )
          //   ]),
          // ))
        ],
      ),
    );
    return row;
  }
}
