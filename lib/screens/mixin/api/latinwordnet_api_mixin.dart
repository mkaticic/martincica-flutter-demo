// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/model/api/latinwordnet_api.dart';
import 'package:martincica_flutter_demo/screens/mixin/google_search_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';

mixin LatinwordnetApiMixin<S extends StatefulWidget>
    on State<S>, UtilsMixin<S>, GoogleSearchMixin<S> {

  LatinWordNetApi api = LatinWordNetApi();
  var webSearchPrefix = 'latin+word+';

  showExplainTaxonDialog(String taxon) {
    explain(taxon).then((explainedLemmas) {
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Row(
                  children: [
                    Text(taxon),
                    buildWebSearchIconButton(taxon, prefix: webSearchPrefix)
                  ],
                ),
                content: SingleChildScrollView(
                  child: Column(
                    children: buildLemas(explainedLemmas)
                  ),
                ),
                actions: [closeTextButton()],
              ));
    });
  }

  Future<List<LemaElem>> explain(String term) async {
    List<String> words = term.split(" ");
    List<LemaElem> explain = [];
    LemaElem? translated;
    for (var element in words) {
      translated = await api.translate(element);
      if (translated != null) {
        explain.add(translated);
      }
    }
    return explain;
  }

  buildLemas(List<LemaElem> explainedLemmas) {
    List<Widget> widgets = [];
    for (LemaElem lemaElem in explainedLemmas) {
      addLemmaTitle(lemaElem, widgets);
      if (lemaElem.synsets == null) {
        continue;
      }
      String lemma = lemaElem.lemma ?? '';
      addSynsets(
          '[$lemma] - literal translation', lemaElem.synsets!.literal, widgets);
      addSynsets(
          '[$lemma] - metaphoric translation', lemaElem.synsets!.metaphoric,
          widgets);
      addSynsets(
          '[$lemma] - metonymic translation', lemaElem.synsets!.metonymic,
          widgets);
    }
    return widgets;
  }

  void addSynsets(String title, List<SynsetElement>? synsets,
      List<Widget> widgets) {
    if (synsets!.isEmpty) {
      return;
    }
    widgets.add(boldedText(title));
    for (var synset in synsets) {
      Widget lemma = paddedVText(synset.gloss ?? '-');
      widgets.add(lemma);
    }
  }

  void addLemmaTitle(LemaElem lemaElem, List<Widget> widgets) {
    if (lemaElem.lemma != null) {
      widgets.add(
          Padding(
              padding: const EdgeInsets.only(top: 16, bottom: 8),
              child: Row(
                  children: [
                    buildLemmaTitleText(lemaElem.lemma),
                    buildWebSearchIconButton(lemaElem.lemma, prefix: webSearchPrefix)
                ])
          )
      );
    }
  }

  buildLemmaTitleText(String? lemma) {
    return   
      Text(
        'lat.' + (lemma ?? '-'),
        style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: Theme.of(context).primaryTextTheme.headlineMedium!.fontSize,
            color: Theme
                .of(context)
                .primaryColorDark));
  }

}
