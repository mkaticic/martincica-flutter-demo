// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:galleryimage/galleryimage.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif/gbif_species.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif_api.dart';
import 'package:martincica_flutter_demo/screens/mixin/preferences_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';

import '../../../data/model/api/gbif/gbif_related.dart';
import '../url_mixin.dart';

mixin GbifApiMixin<S extends StatefulWidget>
    on State<S>, PreferencesMixin<S>, UtilsMixin<S>, UrlMixin<S> {
  GbifApi gbifApi = GbifApi();
  GbifSpecies gbifSpecies = GbifSpecies();

  bool speciesMediaInitialized = false;
  bool occuranceMediaPngInitialized = false;
  bool selectedMediaInitialized = false;

  String selectedMediaPath = '';
  List<String> speciesMedia = [];
  List<String> occuranceMedia = [];

  // related
  List<GbifRelated> reladedBox = [];
  bool reladedBoxInitialized = false;

  // partners = associatedTaxa
  Set<String> partnersBox = <String>{};
  bool partnerBoxInitialized = false;

  void resetGbif(GbifSpecies species) {
    gbifSpecies = species;
    selectedMediaPath = '';
    speciesMedia = [];
    occuranceMedia = [];
    selectedMediaInitialized = false;
    speciesMediaInitialized = false;
    occuranceMediaPngInitialized = false;
    reladedBoxInitialized = false;
  }

  void initGbif(GbifSpecies species) {
    initSpeciesMedia(gbifSpecies.key);
    initOccuranceMediaPng(gbifSpecies.key);
    initSelectedMedia();
    initRelatedBox(gbifSpecies.key);
    initPartners(gbifSpecies.key);
  }

  void initRelatedBox(int? id) {
    // dbpedia
    if (!reladedBoxInitialized) {
      gbifApi.getRelated(id).then((related) {
        reladedBox = related;
        reladedBoxInitialized = true;
        setState(() {});
      });
    }
  }

  void initPartners(int? key) {
    // dbpedia
    if (!partnerBoxInitialized) {
      gbifApi.getMycorrhizalPartners(key).then((partners) {
        partnersBox = partners;
        partnerBoxInitialized = true;
        setState(() {});
      });
    }
  }

  Future<GbifSpecies> getGbifSpecies(int gbifKey) async {
    GbifSpecies species = await gbifApi.getSpeciesByKey(gbifKey);
    return species;
  }

  void initSpeciesMedia(int? speciesKey) {
    if (!speciesMediaInitialized) {
      gbifApi.searchSpeciesMediaPng(speciesKey ?? -1).then((media) {
        speciesMedia = media;
            speciesMediaInitialized = true;
            setState(() {});
          });
    }
  }

  void initOccuranceMediaPng(int? speciesKey) {
    // dbpedia
    if(!occuranceMediaPngInitialized) {
      gbifApi.searchOccuranceMediaPng(speciesKey ?? -1).then((value) {
        occuranceMedia = value;
        occuranceMediaPngInitialized = true;
        setState(() {});
      });
    }
  }

  void initSelectedMedia() {
    if(speciesMediaInitialized && speciesMedia.isNotEmpty){
      selectedMediaPath = speciesMedia[0];
      selectedMediaInitialized = true;
      return;
    }
    if(speciesMediaInitialized && occuranceMediaPngInitialized && occuranceMedia.isNotEmpty){
      selectedMediaPath = occuranceMedia[0];
      selectedMediaInitialized = true;
      return;
    }
    selectedMediaPath = '';
    if(speciesMediaInitialized && occuranceMediaPngInitialized){
      selectedMediaInitialized = true;
    }
  }

  Widget selectedGbifImage() {
    if (!selectedMediaInitialized || selectedMediaPath.isEmpty) {
      return Container();
    }
    return Padding(
        padding: EdgeInsets.only(bottom: 16),
        child: Image.network(selectedMediaPath));
  }

  Widget photoGalleryButton() {
    if (!occuranceMediaPngInitialized || occuranceMedia.isEmpty) {
      return Container();
    }

    return Container(
        alignment: Alignment.centerRight,
        //padding: EdgeInsets.only(right: 32),
        child: IconButton(
            icon: Icon(Icons.photo_library),
            color: getPreferedColor(),
            onPressed: () {
              showDialog(
                  context: context,
                  builder: (BuildContext context) {
                    return Column(
                      children: [
                        GalleryImage(
                            titleGallery: gbifSpecies.canonicalName,
                            imageUrls: occuranceMedia),
                        ElevatedButton(
                            onPressed: () => Navigator.pop(context),
                            child: Text('Cancel'))
                      ],
                    );
                  });
            }));
  }

  relatedUrlsBoxData() {
    if (reladedBox.isEmpty) {
      return Row();
    }
    List<Widget> rows = [];
    for (GbifRelated gr in reladedBox) {
      if (gr.references != null && gr.references!.isNotEmpty) {
        rows.add(relatedRow(gr!.lastCrawled, gr.references));
      }
    }
    if (rows.length == 1) {
      return Row();
    }
    return ExpansionTile(
        title: Text("Related urls"),
        tilePadding: EdgeInsets.zero,
        children: rows);
  }

  Widget relatedRow(DateTime? lastCrawled, String? url) {
    if (url == '[]') {
      return Row();
    }
    Widget row = Padding(
      padding: EdgeInsets.zero,
      child: Row(
        children: [Expanded(flex: 5, child: buildUrl(url))],
      ),
    );
    return row;
  }

  Widget partnerRow(String partner) {
    if (partner == '[]') {
      return Row();
    }
    Widget row = Padding(
      padding: EdgeInsets.zero,
      child: Row(
        children: [Expanded(flex: 5, child: Text(partner))],
      ),
    );
    return row;
  }

  associatedTaxaBoxData() {
    if (partnersBox.isEmpty) {
      return Row();
    }
    List<Widget> rows = [];
    for (String partner in partnersBox) {
      rows.add(partnerRow(partner));
    }
    if (rows.length == 1) {
      return Row();
    }
    return ExpansionTile(
        title: Text("Associated taxa"),
        tilePadding: EdgeInsets.zero,
        children: rows);
  }
}
