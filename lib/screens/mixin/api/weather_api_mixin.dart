// ignore_for_file: prefer_const_constructors

import 'package:drift/drift.dart' hide Column;
import 'package:flutter/material.dart';
import 'package:latlong2/latlong.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/screens/mixin/osm_map_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:provider/provider.dart';

import '../../../data/model/api/open_meteo/open_meteo.dart';
import '../../../data/model/api/open_meteo_api.dart';
import '../../../data/model/db/drift/dao/observations_dao.dart';

mixin WeatherApiMixin<S extends StatefulWidget>
    on State<S>, UtilsMixin<S>, OsmMapMixin<S> {
  OpenMeteoData? meteoData;
  bool initialized = false;
  bool showWeather = false;

  Widget buildWheather(observationId) {
    if (!showWeather || observationId == null) {
      return SingleChildScrollView(child: Text(''));
    }

    if (!initialized) {
      ObservationsDao db =
          Provider.of<MoorDb>(context, listen: false).observationsDao;
      db.getById(observationId).then((obs) {
        if (obs.latitude == null || obs.longitude == null) {
          return SingleChildScrollView(child: Text('No location data yet'));
        }
        getOpenMeteoData(obs).then((wheaterData) => {
              setState(() {
                initialized = true;
              })
            });
      });
      return SingleChildScrollView(child: Text(''));
    } else {
      return SingleChildScrollView(child: meteoDataBox(observationId));
    }
  }

  Future getOpenMeteoData(Observation observation) async {
    OpenMeteoApi api = OpenMeteoApi();
    meteoData =
        await api.getOpenMeteoData(latlong(observation), observation.date);
  }

  meteoDataBox(observationId) {
    if (meteoData == null) {
      return Row();
    }
    return Column(children: toBoxData(observationId));
  }

  Widget morfoBoxRow(String day, String temperature, String desc) {
    if (day == null) {
      return Row();
    }
    Widget row = Padding(
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Row(
        children: [
          Expanded(
              flex: 3,
              child: Text(day,
                  style: TextStyle(color: Theme.of(context).hintColor))),
          Expanded(
              flex: 3,
              child: Text(temperature,
                  style: TextStyle(color: Theme.of(context).primaryColor))),
          Expanded(
              flex: 4,
              child: Text(desc,
                  style: TextStyle(color: Theme.of(context).primaryColor)))
        ],
      ),
    );
    return row;
  }

  toBoxData(observationId) {
    List<Widget> children = [];
    LatLng latLng = LatLng(meteoData!.latitude, meteoData!.longitude);
    children.add(Row(
      children: [
        Text('Show measurement location:  '),
        ElevatedButton(
            child: Icon(Icons.map),
            onPressed: () => openOSMapDetailPage(latLng))
      ],
    ));
    children.add(Row(
      children: [
        Text('Delete mesurment location:  '),
        ElevatedButton(
            child: Icon(Icons.delete),
            onPressed: () {
              ObservationsDao db =
                  Provider.of<MoorDb>(context, listen: false).observationsDao;
              db.customUpdate(
                  "update observations set latitude = null, longitude = null where id = :id",
                  variables: [
                    Variable.withInt(observationId)
                  ]).then((val) => setState(() {
                    showWeather = false;
                    initialized = false;
                  }));
            })
      ],
    ));
    children.add(Row(
      children: [
        Text("Lat: " +
            latLng.latitude.toString() +
            ", Long: " +
            latLng.longitude.toString())
      ],
    ));
    for (List day in meteoData!.data.reversed) {
      children.add(morfoBoxRow(day[0], day[1], day[2]));
    }
    return children;
  }

  LatLng? latlong(Observation observation) {
    if (observation.latitude == null || observation.longitude == null) {
      return null;
    }
    return LatLng(observation.latitude ?? 0, observation.longitude ?? 0);
  }
}
