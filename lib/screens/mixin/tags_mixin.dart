// ignore_for_file: prefer_const_constructors

import 'package:drift/drift.dart';
import 'package:flutter/material.dart';

import 'package:martincica_flutter_demo/data/model/db/drift/dao/tags_dao.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:provider/provider.dart';

import 'package:martincica_flutter_demo/ext/flutter_tags-null-safety/lib/flutter_tags.dart';

mixin TagsMixin<S extends StatefulWidget> on State<S> {
  List<Tag> tags = [];
  bool tagsInitialized = false;
  bool tagsSuggestionsInitialized = false;

  List<String> tagSuggestions = [];
  final double tagFontSize = 24;
  final GlobalKey<TagsState> _tagStateKey = GlobalKey<TagsState>();

  final TextEditingController txtNewTag = TextEditingController();

  buildTags(TagType type, String? refId) {
    if (refId == null) {
      return Container();
    }
    if (!tagsInitialized) {
      reloadTags(refId, type);
    }

    if (!tagsSuggestionsInitialized) {
      reloadTagsSuggestions(type);
    }

    return Tags(
      key: _tagStateKey,
      textField: TagsTextField(
          autofocus: false,
          textStyle: TextStyle(fontSize: tagFontSize),
          constraintSuggestion: false,
          suggestions: tagSuggestions,
          //width: double.infinity, padding: EdgeInsets.symmetric(horizontal: 10),
          onSubmitted: (String tag) => insertAndReload(tag, type, refId)),
      itemCount: tags.length, // required
      itemBuilder: (int index) {
        final item = tags[index];
        return ItemTags(
          key: Key(index.toString()),
          index: index,
          title: item.tag,
          active: true,
          customData: 'ds',
          textStyle: TextStyle(
            fontSize: tagFontSize,
          ),
          combine: ItemTagsCombine.withTextBefore,
          removeButton: buildItemTagsRemoveButton(index, type, refId),
          onPressed: (item) => print(item),
          onLongPressed: (item) => print(item),
          // image: ItemTagsImage(
          //     image: AssetImage("img.jpg") // OR NetworkImage("https://...image.png")
          // ), // OR null,
          // icon: ItemTagsIcon(
          //   icon: Icons.add,
          // ), // OR null,
        );
      },
    );
  }

  Future<List<Tag>> getTags(TagType type, String refId) async {
    TagsDao db = Provider.of<MoorDb>(context, listen: false).tagsDao;
    return await db.getTagsByRefId(type.toString(), refId);
  }

  resetTags() {
    tags = [];
  }

  Future<List<Tag>> getTagTypes(String tag) async {
    TagsDao db = Provider.of<MoorDb>(context, listen: false).tagsDao;
    return await db.getTagTypes(tag);
  }

  Future insertTag(BuildContext context, TagsCompanion tagsCompanion) async {
    TagsDao db = Provider.of<MoorDb>(context, listen: false).tagsDao;
    await db.insert(tagsCompanion);
  }

  Future<List<String>> getTagsByType(TagType type) async {
    TagsDao db = Provider.of<MoorDb>(context, listen: false).tagsDao;
    List<String> sugesstions = await db.getTagsByType(type.toString());
    return sugesstions;
  }

  Future<bool> checkNotReferenced(String tag) async {
    TagsDao db = Provider.of<MoorDb>(context, listen: false).tagsDao;
    List<Tag> tags = await db.getTagsByTag(tag);
    return tags.isEmpty;
  }


  Future<bool> deleteTag(TagType type, String refId, String tag) async {
    TagsDao db = Provider.of<MoorDb>(context, listen: false).tagsDao;
    await db.deleteTag(refId, type.toString(), tag);
    return true;
  }

  reloadTags(String refId, TagType type) {
    getTags(type, refId).then((value) {
      tags = value;
      tagsInitialized = true;
      setState(() {});
    });
  }

  void reloadTagsSuggestions(TagType type) {
    getTagsByType(type).then((suggestions) {
      tagSuggestions = suggestions;
      tagsSuggestionsInitialized = true;
      setState(() {});
    });
  }

  Future runFilterByTag(TagType type, String? newValue) async {}

  TagsCompanion newTag(TagType type, refId, String tag) {
    return TagsCompanion(
        refId: Value(refId),
        type: Value(type.toString()),
        tag: Value(tag),
        active: Value(true));
  }

  insertAndReload(String tag, TagType type, String refId) {
    // required
    insertTag(
            context,
            TagsCompanion(
                refId: Value(refId),
                type: Value(type.toString()),
                tag: Value(tag),
                active: Value(true)))
        .then((_) {
      reloadTags(refId, type);
      reloadTagsSuggestions(type);
    });
  }

  buildItemTagsRemoveButton(int index, TagType type, String refId) {
    return ItemTagsRemoveButton(
      onRemoved: () {
        // Remove the item from the data source.
        deleteTag(type, refId, tags[index].tag).then((value) {
          reloadTags(refId, type);
          reloadTagsSuggestions(type);
          setState(() {});
        });
        return true;
      },
    );
  }

}

enum TagType {
  none,
  observation,
  observation_photo,
  morfology

  // fungiMorfCap,
  // fungiMorfStem,
  // fungiMorfCap

}
