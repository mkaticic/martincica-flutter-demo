// ignore_for_file: prefer_const_constructors

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/screens/mixin/files_mixin.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';
import 'package:path/path.dart';

import '../data/file_helper.dart';
import 'details/file_detail.dart';

class FilesScreen extends StatefulWidget {
  @override
  _FilesScreenState createState() => _FilesScreenState();
}

class _FilesScreenState extends State<FilesScreen> with FilesMixin {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Files')),
      drawer: MenuDrawer(),
      bottomNavigationBar: GTBottomNavBar(),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => FileScreen(null)));
          }),
      body: FutureBuilder(
        future: getFiles(),
        builder: (context, snapshot) {
          files = (snapshot.data == null) ? [] : snapshot.data as List<File>;
          return ListView.builder(
            itemCount: files.length,
            itemBuilder: (context, index) {
              return Dismissible(
                key: UniqueKey(),
                onDismissed: (direction) {
                  deleteFileAtIndex(index);
                },
                child: ListTile(
                  title: Text(basename(files[index].path)),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FileScreen(files[index])));
                  },
                ),
              );
            },
          );
        },
      ),
    );
  }
}
