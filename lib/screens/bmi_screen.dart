// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';

class BMIScreen extends StatefulWidget {
  const BMIScreen({Key? key}) : super(key: key);

  @override
  State<BMIScreen> createState() => _BMIScreenState();
}

class _BMIScreenState extends State<BMIScreen> {
  double fontSize = 18;
  String bmi = '';

  bool isMetric = true;
  late List<bool> isSelected;

  double? height;
  double? weight;
  String result = "result will be here";

  final TextEditingController txtWeight = TextEditingController();
  final TextEditingController txtHeight = TextEditingController();

  String? hMessage;
  String? wMessage;

  @override
  void initState() {
    isSelected = [true, false];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String hUnit = isMetric ? 'meters' : 'inches';
    String wUnit = isMetric ? 'kilos' : 'pounds';
    hMessage = 'Please select you height in ' + hUnit;
    wMessage = 'Please select you weight in ' + wUnit;
    return Scaffold(
        appBar: AppBar(title: Text('BMI calculator')),
        drawer: MenuDrawer(),
        bottomNavigationBar: GTBottomNavBar(),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(40),
          child: Column(children: [
            ToggleButtons(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text('Metric', style: TextStyle(fontSize: fontSize)),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text('Imperial', style: TextStyle(fontSize: fontSize)),
                )
              ],
              isSelected: isSelected,
              onPressed: toggleMeasure,
            ),
            TextField(
                controller: txtHeight,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                decoration: InputDecoration(hintText: hMessage)),
            TextField(
                controller: txtWeight,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                decoration: InputDecoration(hintText: wMessage)),
            Padding(
                padding: const EdgeInsets.symmetric(vertical: 30),
                child: ElevatedButton(
                    child: Text('Calculate BMI',
                        style: TextStyle(fontSize: fontSize)),
                    onPressed: calcBMI)),
            Text(result, style: TextStyle(fontSize: fontSize))
          ]),
        ));
  }

  void toggleMeasure(value) {
    if (value == 0) {
      setState(() {
        isMetric = true;
        isSelected = [true, false];
      });
    } else {
      setState(() {
        isMetric = false;
        isSelected = [false, true];
      });
    }
  }

  void calcBMI() {
    double bmi = 0;
    double h = double.tryParse(txtHeight.text) ?? 0;
    double w = double.tryParse(txtWeight.text) ?? 0;
    if (isMetric) {
      bmi = w / (h * h);
    } else {
      bmi = w * 703 / (h * h);
    }
    setState(() {
      result = "Your BMI: " + bmi.toStringAsFixed(2);
    });
  }
}
