// ignore_for_file: prefer_const_constructors, prefer_const_constructors_in_immutables, use_key_in_widget_constructors, avoid_function_literals_in_foreach_calls

import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:martincica_flutter_demo/data/model/db/sp/font_size.dart';
import 'package:martincica_flutter_demo/data/sp_helper.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  SharedPreferencesHelper settings = SharedPreferencesHelper();
  int settingColor = Colors.grey.value;
  late double fontSize;
  late bool debugOn = false;

  final TextEditingController txtPhotoGallery = TextEditingController();
  final TextEditingController txtObservations = TextEditingController();

  final TextEditingController txtShareWhatsupNumber = TextEditingController();

  List<FontSize> fontSizeList = [
    FontSize('small', 12),
    FontSize('medium', 16),
    FontSize('large', 20),
    FontSize('extra-large', 24)
  ];

  @override
  void initState() {
    settings.init().then((_) {
      setState(() {});
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    txtPhotoGallery.text = settings.getStr(settings.photoGalleryDir) ?? '';
    txtObservations.text = settings.getStr(settings.observationsDir) ?? '';
    txtShareWhatsupNumber.text = settings.getStr(settings.whatsupPhoneNumber) ?? '';
    fontSize = settings.getFontSize();
    debugOn = settings.getDebugModeOn();
    settingColor = settings.getColor();
    return Scaffold(
      appBar:
          AppBar(backgroundColor: Color(settingColor), title: Text('Settings')),
      drawer: MenuDrawer(),
      bottomNavigationBar: GTBottomNavBar(),
      body:
      SingleChildScrollView(
        padding: EdgeInsets.only(top: 30, left: 10),
          child: Column(
            children: [

              Text("Choose font size:", style: texStyle()),
              DropdownButton(
                  value: fontSize.toString(),
                  items: getFontSizeList(),
                  onChanged: changeSize),

              themeColor(),

              padding(),
              Text('Folders', style: texStyle()),
              TextField(
                controller: txtPhotoGallery,
                decoration: InputDecoration(
                    hintText: 'Select PhotoGallery directory',
                    prefix: Text('Photo gallery: ', style: texStyle()),
                    contentPadding: EdgeInsets.all(16),
                    suffixIcon: IconButton(
                        icon: Icon(Icons.folder),
                        onPressed: () => chooseAndSet(settings.photoGalleryDir))),
              ),
              TextField(
                controller: txtObservations,
                decoration: InputDecoration(
                    hintText: 'Select observation photo directory',
                    prefix: Text('Observation photos: ', style: texStyle()),
                    contentPadding: EdgeInsets.all(16),
                    suffixIcon: IconButton(
                        icon: Icon(Icons.folder),
                        onPressed: () => chooseAndSet(settings.observationsDir))),
              ),

              padding(),
              Text('Share settings', style: texStyle()),
              TextField(
                controller: txtShareWhatsupNumber,
                decoration: InputDecoration(
                    hintText: 'Whatsup phone number',
                    prefix: Text('Phone: ', style: texStyle()),
                    contentPadding: EdgeInsets.all(16),
                    suffixIcon:  Icon(Icons.phone)),
                onChanged: (value) => setValue(settings.whatsupPhoneNumber, value)
              ),

              padding(),
              Text('Debug on?', style: texStyle()),
              Checkbox(
              value: debugOn,
              onChanged: (bool? newValue) {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: Text(
                            'Debug mode ON/OF?'
                            'Do you want to continue?'),
                        actions: [
                          TextButton(
                              onPressed: () {
                                bool debug = newValue ?? false;
                                settings.setDebugModeOn(debug);
                                setState(() {
                                  debugOn = debug;
                                });
                                Navigator.pop(context);
                              },
                              child: Text('Continue')),
                          ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('No'))
                        ],
                      );
                    });
              })
            ],
          )),
    );
  }

  setColor(int color) {
    setState(() {
      settingColor = color;
      settings.setColor(color);
    });
  }

  colorElem(MaterialColor color) {
    return GestureDetector(
        child: Padding(
            padding: EdgeInsets.all(10),
            child: ColorSquare(color.value)),
        onTap: () => setColor(color.value));
  }

  List<DropdownMenuItem<String>> getFontSizeList() {
    List<DropdownMenuItem<String>> list = [];
    fontSizeList.forEach((element) {
      list.add(DropdownMenuItem(
          value: element.size.toString(), child: Text(element.name)));
    });
    return list;
  }

  void changeSize(Object? value) {
    double font = 14;
    if (value != null) {
      font = double.parse(value.toString());
    }
    settings.setFontSize(font);
    setState(() {
      fontSize = font;
    });
  }

  texStyle() {
    return TextStyle(
        fontSize: fontSize,
        color: Color(settingColor),
        fontWeight: FontWeight.bold);
  }

  chooseAndSet(String key) {
    FilePicker.platform.getDirectoryPath().then((path) {
      if (path != null) {
        settings.setStr(key, path);
        setState(() {});
      }
    });
  }

  setValue(String key, String value) {
    settings.setStr(key, value);
    setState(() {});
  }

  themeColor() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              padding(),
              Text("Select theme color:", style: texStyle()),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      colorElem(Colors.green),
                      colorElem(Colors.deepOrange),
                      colorElem(Colors.amber)
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      colorElem(Colors.deepPurple),
                      colorElem(Colors.cyan),
                      colorElem(Colors.blue)
                    ],
                  ),
                  ElevatedButton(
                      style: ButtonStyle(
                          backgroundColor:
                              MaterialStateProperty.all(Color(settingColor))),
                      onPressed: () {
                        Phoenix.rebirth(context);
                      },
                      child: Text('Apply settings'))
            ],
          )],
    );
  }

  padding() {
    return Padding(padding: EdgeInsets.only(top: 20));
  }
}

class ColorSquare extends StatelessWidget {
  final int colorCode;

  ColorSquare(this.colorCode);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 72,
      height: 72,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(16)),
          color: Color(colorCode)),
    );
  }
}
