// ignore_for_file: prefer_const_constructors


import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/model/db/sp/session.dart';
import 'package:martincica_flutter_demo/data/sp_helper.dart';
import 'package:martincica_flutter_demo/shared/bottom_nav_bar.dart';
import 'package:martincica_flutter_demo/shared/menu_drawer.dart';
import 'package:uuid_type/uuid_type.dart';

class SessionsScreen extends StatefulWidget {
  const SessionsScreen({Key? key}) : super(key: key);

  @override
  _SessionsScreenState createState() => _SessionsScreenState();
}

class _SessionsScreenState extends State<SessionsScreen> {
  SharedPreferencesHelper spHelper = SharedPreferencesHelper();

  List<Session> sessions = [];

  TextEditingController txtName = TextEditingController();
  TextEditingController txtDesc = TextEditingController();

  @override
  void initState() {
    spHelper.init().then((value) => updateScreen());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Sessions')),
      drawer: MenuDrawer(),
      bottomNavigationBar: GTBottomNavBar(),
      body: ListView(children: getContent()),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          showSessionDialog(context);
        },
      ),
    );
  }

  Future<dynamic> showSessionDialog(BuildContext context) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Insert session'),
            content: SingleChildScrollView(
              child: Column(
                children: [
                  TextField(
                      controller: txtName,
                      decoration: InputDecoration(hintText: 'name')),
                  TextField(
                      controller: txtDesc,
                      decoration: InputDecoration(hintText: 'description')),
                ],
              ),
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                    cleanTxt();
                  },
                  child: Text('Cancel')),
              ElevatedButton(
                  onPressed: saveSession,
                  child: Text('Save', style: TextStyle(fontSize: 18)))
            ],
          );
        });
  }

  void saveSession() {
    String id = 'session_' + RandomUuidGenerator().generate().toString();
    DateTime now = DateTime.now();
    String today = '${now.year}-${now.month}-${now.day}';
    Session session = Session(id, today, txtName.text, txtDesc.text, 20);
    spHelper.writeSession(session).then((_) => updateScreen());
    cleanTxt();
    Navigator.pop(context);
  }

  void updateScreen() {
    sessions = spHelper.getSessions();
    setState(() {});
  }

  void cleanTxt() {
    txtName.text = '';
    txtDesc.text = '';
  }

  List<Widget> getContent() {
    List<Widget> tiles = [];
    sessions.forEach((session) {
      tiles.add(Dismissible(
        key: UniqueKey(),
        onDismissed: (_) {
          spHelper.deleteSession(session.id).then((_) => updateScreen());
        },
        child: ListTile(
            title: Text(session.name),
            subtitle:
                Text('${session.date} - duration: ${session.duration} min')),
      ));
    });
    return tiles;
  }
}
