import 'dart:io';

import 'package:exif/exif.dart';
import 'package:latlong2/latlong.dart';

class ExifUtils {
  static const EXIF_IMAGE_DATETIME = 'Image DateTime';
  static const EXIF_USER_COMMENT = 'EXIF UserComment';
  static const EXIF_GPS_LONGITUDE = 'GPS GPSLongitude';
  static const EXIF_GPS_LONGITUDE_REF = 'GPS GPSLongitudeRef';
  static const EXIF_GPS_LATITUDE = 'GPS GPSLatitude';
  static const EXIF_GPS_LATITUDE_REF = 'GPS GPSLatitudeRef';

  static Future<ExifData> getExifData(File file) async {
    Map<String, IfdTag> imgTags =
        await readExifFromBytes(file.readAsBytesSync());
    ExifData data = ExifData();
    data.file = file;
    data.path = file.path;
    if (imgTags.containsKey(EXIF_GPS_LONGITUDE)) {
      data.point = exifGPSToGeoFirePoint(imgTags);
    }
    if (imgTags.containsKey(EXIF_IMAGE_DATETIME)) {
      data.dateTime = imgTags[EXIF_IMAGE_DATETIME]!.printable;
    }
    if (imgTags.containsKey(EXIF_USER_COMMENT)) {
      final RegExp exp = RegExp(r'mgzn-content>(.*?)<');
      String temp = imgTags[EXIF_USER_COMMENT]!.printable;
      List<RegExpMatch> matches = exp.allMatches(temp).toList();
      for (var match in matches) {
        data.userComment = temp.substring(match.start, match.end);
        data.userComment = data.userComment.replaceAll('mgzn-content>', '');
        data.userComment = data.userComment.replaceAll('<', '');
      }
    }
    return data;
  }

  static Future<LatLng?> checkGPSData(File file) async {
    Map<String, IfdTag> imgTags =
        await readExifFromBytes(file.readAsBytesSync());
    if (imgTags.containsKey(EXIF_GPS_LONGITUDE) &&
        imgTags[EXIF_GPS_LONGITUDE]!.printable != "[0/0, 0/0, 0/0]") {
      return exifGPSToGeoFirePoint(imgTags);
    }
    return null;
  }

  static LatLng exifGPSToGeoFirePoint(Map<String, IfdTag> tags) {
    // exif format : [d1/d2, m1/m2, s1/s2]
    // Degrees + Minutes/60 + Seconds/3600

    final latitudeValue = tags[EXIF_GPS_LATITUDE]!
        .values
        .toList()
        .map<double>(
            (item) => (item.numerator.toDouble() / item.denominator.toDouble()))
        .toList();
    final latitudeSignal = tags[EXIF_GPS_LATITUDE_REF]!.printable;

    final longitudeValue = tags[EXIF_GPS_LONGITUDE]!
        .values
        .toList()
        .map<double>(
            (item) => (item.numerator.toDouble() / item.denominator.toDouble()))
        .toList();
    final longitudeSignal = tags[EXIF_GPS_LONGITUDE_REF]!.printable;

    double latitude =
        latitudeValue[0] + (latitudeValue[1] / 60) + (latitudeValue[2] / 3600);

    double longitude = longitudeValue[0] +
        (longitudeValue[1] / 60) +
        (longitudeValue[2] / 3600);

    if (latitudeSignal == 'S') latitude = -latitude;
    if (longitudeSignal == 'W') longitude = -longitude;
    // print('latitude: ' + latitude.toString());
    // print('longitude: ' + longitude.toString());

    return LatLng(latitude, longitude);
  }
}

class ExifData {
  late File file;
  late String path;
  String dateTime = "";
  String userComment = "";
  LatLng? point;
}
