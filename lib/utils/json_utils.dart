class JsonUtils {
  static String getOrDashStr(Map<String, dynamic> json, String key) {
    return json[key] ?? '-';
  }

  static int getOrDashInt(Map<String, dynamic> json, String key) {
    return json[key] ?? -1;
  }

  static bool getOrDashBoolean(Map<String, dynamic> json, String key) {
    return json[key] ?? false;
  }
}
