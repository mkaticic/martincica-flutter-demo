// A widget that displays the picture taken by the user.
// ignore_for_file: prefer_const_constructors

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/file_helper.dart';

class DisplayPictureScreen extends StatelessWidget {
  final String imagePath;
  final Function(File) onSave;

  const DisplayPictureScreen(
      {Key? key, required this.imagePath, required this.onSave})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    File image = File(imagePath);
    return Scaffold(
      appBar: AppBar(title: const Text('Display the Picture')),
      // The image is stored as a file on the device. Use the `Image.file`
      // constructor with the given path to display the image.
      body: Column(
        children: [
          Image.file(image),
          ElevatedButton(
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (_) => AlertDialog(content: Text('TODO')));
              },
              child: Text('Tag image')),
          ElevatedButton(
              onPressed: () {
                //
                onSave(image);
                // this is not ideal
                Navigator.pop(context);
                Navigator.pop(context);
              },
              child: Text('Save'))
        ],
      ),
    );
  }
}
