// ignore_for_file: prefer_const_constructors, avoid_function_literals_in_foreach_calls

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/sp_helper.dart';
import 'package:martincica_flutter_demo/screens/gallery_screen.dart';
import 'package:martincica_flutter_demo/screens/locations_screen.dart';
import 'package:martincica_flutter_demo/screens/observations_screen.dart';
import 'package:martincica_flutter_demo/screens/search_screen.dart';
import 'package:martincica_flutter_demo/screens/settings_screen.dart';
import 'package:martincica_flutter_demo/screens/tags_screen.dart';

import '../screens/favorite_screen.dart';
import '../screens/info_screen.dart';
import '../screens/intro_screen.dart';

class MenuDrawer extends StatelessWidget {
  SharedPreferencesHelper settings = SharedPreferencesHelper();

  MenuDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (!settings.isInitialized) {
      return Container();
    }
    return Drawer(child: ListView(children: buildMenuItems(context)));
  }

  List<Widget> buildMenuItems(BuildContext context) {
    final List<MenuItem> menuTitles = [
      MenuItem(Icons.home, 'Home', IntroScreen()),
      MenuItem(Icons.search, 'Search', SearchScreen()),
      MenuItem(Icons.visibility, 'Observations', ObservationsScreen()),
      MenuItem(Icons.photo, 'Photos', GalleryScreen()),
      MenuItem(Icons.place, 'Locations', LocationsScreen()),
      MenuItem(Icons.favorite, 'Favorites', FavoriteScreen()),
      MenuItem(Icons.tag, 'Tags', TagsScreen()),
      MenuItem(Icons.settings, 'Settings', SettingsScreen()),
      MenuItem(Icons.info, 'Info', InfoScreen()),

      // will be deleted
      // MenuItem(Icons.circle, '', Divider()),
      // MenuItem(Icons.note, 'Blog', PostsScreen()),
      // MenuItem(Icons.note, 'Notes', NotelistScreen()),
      // MenuItem(Icons.note, 'Sessions', SessionsScreen()),
      // MenuItem(Icons.storage, 'Files', FilesScreen()),
      // MenuItem(Icons.calculate, 'BMI', BMIScreen()),
    ];

    List<Widget> menuItems = [];

    menuItems.add(DrawerHeader(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 40),
        decoration: BoxDecoration(color: Color(settings.getColor())),
        child: Text('GEOTropa',
            style: TextStyle(
                color: Colors.white,
                fontSize: 40,
                fontWeight: FontWeight.bold))));

    for (MenuItem element in menuTitles) {
      if (element.title.isEmpty) {
        menuItems.add(element.screen);
        continue;
      }
      // else
      menuItems.add(buildListTile(context, element));
    }
    return menuItems;
  }

  Widget buildListTile(BuildContext context, MenuItem element) {
    return ListTile(
        leading: Icon(element.icon, color: Color(settings.getColor())),
        title: Text(element.title,
            style: TextStyle(
                fontSize: settings.getFontSize(), fontWeight: FontWeight.bold)),
        onTap: () {
          Navigator.of(context).pop(); // close the menu
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (context) => element.screen));
        });
  }
}

class MenuItem {
  IconData icon;
  String title;
  Widget screen;

  MenuItem(this.icon, this.title, this.screen);
}