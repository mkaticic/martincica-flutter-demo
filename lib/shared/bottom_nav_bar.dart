// ignore_for_file: prefer_const_literals_to_create_immutables, prefer_const_constructors

import 'package:flutter/material.dart';

class GTBottomNavBar extends StatelessWidget {
  const GTBottomNavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (int index) {
        switch (index) {
          case 1:
            Navigator.pushNamed(context, '/observations');
            break;
          case 2:
            Navigator.pushNamed(context, '/search');
            break;
          case 3:
            Navigator.pushNamed(context, '/settings');
            break;
          default:
            Navigator.pushNamed(context, '/');
            break;
        }
      },
      items: [
        item(Icons.home, "Home", context,
            col: Theme.of(context).primaryColorDark),
        item(Icons.visibility, "Observations", context),
        item(Icons.search, "Search", context),
        item(Icons.settings, "Settings", context)
      ],
    );
  }

  item(IconData icon, String lab, BuildContext context, {Color? col}) {
    return BottomNavigationBarItem(icon: Icon(icon, color: col), label: lab);
  }
}
