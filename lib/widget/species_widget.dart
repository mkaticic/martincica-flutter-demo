// ignore_for_file: prefer_const_constructors

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif/gbif_species.dart';
import 'package:martincica_flutter_demo/screens/mixin/api/latinwordnet_api_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/url_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/utils_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/preferences_mixin.dart';
import '../screens/mixin/api/dbpedia_api_mixin.dart';
import '../screens/mixin/api/gbif_api_mixin.dart';
import '../screens/mixin/google_search_mixin.dart';

class SpeciesWidget extends StatefulWidget {

  GbifSpecies species = GbifSpecies();

  SpeciesWidget(this.species, {Key? key}): super(key: key);

  @override
  State<SpeciesWidget> createState() => _SpeciesWidgetState();

}

class _SpeciesWidgetState extends State<SpeciesWidget>
    with
        UtilsMixin,
        UrlMixin,
        PreferencesMixin,
        GbifApiMixin,
        GoogleSearchMixin,
        LatinwordnetApiMixin,
        DbpediaApiMixin {

  @override
  Widget build(BuildContext context) {
    resetWidgetDataForSpecies();
    return SingleChildScrollView(
      child: Column(
        children: [
          widgetRow(
              Text(gbifSpecies.canonicalName,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).primaryColor)),
              buildWebSearchIconButton(gbifSpecies.canonicalName),
              photoGalleryButton()),
          selectedGbifImage(),
          speciesRow('Author:', gbifSpecies.authorship),
          speciesRow('Status:', gbifSpecies.taxonomicStatus),
          speciesRow('Current:', gbifSpecies.species,
              gbifKey: gbifSpecies.speciesKey),
          mycoMorphBoxData(),
          taxonDetails(),
          relatedUrlsBoxData(),
          associatedTaxaBoxData()
        ],
      ),
    );
  }

  void resetWidgetDataForSpecies() {
    if(gbifSpecies.key != widget.species.key){
      resetGbif(widget.species);
      dbpediaDataInitialized = false;
    }
    initDbPedia(gbifSpecies.canonicalName);
    initGbif(gbifSpecies);
  }

  void resetWidgetDataForKey(int gbifKey) {
    getGbifSpecies(gbifKey)
        .then((species) => setState(() {
          widget.species = species;
        }));
  }

  Widget widgetRow(Widget w1, Widget w2, Widget w3) {
    return Row(
      children: [Expanded(flex: 6, child: w1), Expanded(flex: 1, child: w2), Expanded(flex: 1, child: w3)],
    );
  }

  Widget speciesRow(String label, String value, {int? gbifKey}) {
    if (value.isEmpty || value.compareTo("-") == 0) {
      return Row();
    }
    Widget row = Padding(
        padding: EdgeInsets.only(top: 0),
        child: Row(
        children: [
          Expanded(
              flex: 2,
              child: Text(label,
                  style: TextStyle(color: Theme.of(context).hintColor))),
          Expanded(
              flex: 4,
              // child: Text(value,
              //     style: TextStyle(color: Theme.of(context).primaryColor)))
              child: RichText(
                text: TextSpan(children: [
                  TextSpan(
                      text: value,
                      style: getValueTextStyle(gbifKey != null),
                      recognizer: onTapChangeSpecies(gbifKey))
                ]),
              )),
          Expanded(
              flex: 1,
              child: buildExplainButton(value)),
        ],
      )
    );
    return row;
  }

  TextStyle getValueTextStyle(bool isHyperlink) {
    late TextStyle valueTextStyle;
    if (!isHyperlink) {
      valueTextStyle = TextStyle(color: Theme.of(context).primaryColor);
    } else {
      valueTextStyle = TextStyle(
          fontWeight: FontWeight.bold,
          color: Theme.of(context).primaryColorDark);
    }
    return valueTextStyle;
  }

  TapGestureRecognizer? onTapChangeSpecies(int? gbifKey) {
    if (gbifKey == null) {
      return null;
    }
    return TapGestureRecognizer()
      ..onTap = () => resetWidgetDataForKey(gbifKey);
  }

  buildExplainButton(String value) {
    if (value.isEmpty) {
      return Container();
    }
    return IconButton(
      icon: Icon(Icons.info_outline),
      onPressed: () => showExplainTaxonDialog(value),
    );
  }

  taxonDetails() {
    return ExpansionTile(
        title: Text("Taxon details"),
        tilePadding: EdgeInsets.zero,
        children: [
          speciesRow('Genus:', gbifSpecies.genus,
              gbifKey: gbifSpecies.genusKey),
          speciesRow('Family:', gbifSpecies.family,
              gbifKey: gbifSpecies.familyKey),
          speciesRow('Order:', gbifSpecies.order,
              gbifKey: gbifSpecies.orderKey),
          speciesRow('Class:', gbifSpecies.clazz,
              gbifKey: gbifSpecies.classKey),
          speciesRow('Phylum:', gbifSpecies.phylum,
              gbifKey: gbifSpecies.phylumKey),
          speciesRow('Kingdom:', gbifSpecies.kingdom,
              gbifKey: gbifSpecies.kingdomKey)
        ]);
  }
}
