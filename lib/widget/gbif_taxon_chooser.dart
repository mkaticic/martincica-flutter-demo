// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif/gbif_species.dart';
import 'package:martincica_flutter_demo/screens/mixin/preferences_mixin.dart';
import 'package:martincica_flutter_demo/screens/mixin/search_species_mixin.dart';
import 'package:martincica_flutter_demo/widget/species_widget.dart';

class GbifTaxonChooser extends StatefulWidget {
  final void Function(GbifSpecies species)? onSelected;

  const GbifTaxonChooser({Key? key, this.onSelected}) : super(key: key);

  @override
  _GbifTaxonChooserState createState() => _GbifTaxonChooserState();
}

class _GbifTaxonChooserState extends State<GbifTaxonChooser>
    with PreferencesMixin, SearchGbifSpeciesMixin {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        title: Text("Taxon chooser..."),
        content: SingleChildScrollView(
            child: Column(children: [
          buildTaxonChooserDropdown(textColor: Theme.of(context).primaryColor),
          searchBox(),
          buildDropdown(list),
          SpeciesWidget(gbifSpecies, key: ValueKey(gbifSpecies.key))
        ])),
        actions: [
          TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('Close')),
          ElevatedButton(
              onPressed: () {
                widget.onSelected!(gbifSpecies);
                Navigator.pop(context);
              },
              child: Text('Choose'))
        ]);
  }
}
