// ignore_for_file: prefer_const_constructors, prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:provider/provider.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';
import 'package:martincica_flutter_demo/screens/favorite_screen.dart';
import 'package:martincica_flutter_demo/screens/hello_screen.dart';
import 'package:martincica_flutter_demo/screens/intro_screen.dart';
import 'package:martincica_flutter_demo/screens/notelist_screen.dart';
import 'package:martincica_flutter_demo/screens/observations_screen.dart';
import 'package:martincica_flutter_demo/screens/search_screen.dart';
import 'package:martincica_flutter_demo/screens/sessions_screen.dart';
import 'package:martincica_flutter_demo/screens/settings_screen.dart';

import 'data/drift_db.dart';
import 'data/sp_helper.dart';

void main() {
  runApp(Phoenix(child: GeotropaApp()));
}

class GeotropaApp extends StatefulWidget {
  const GeotropaApp({Key? key}) : super(key: key);

  @override
  _GeotropaAppState createState() => _GeotropaAppState();
}

class _GeotropaAppState extends State<GeotropaApp> {
  SharedPreferencesHelper settings = SharedPreferencesHelper();

  late int settingColor;
  late double fontSize;

  @override
  void initState() {
    settings.init().then((_) => setState(() {}));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (!settings.isInitialized) {
      return Container();
    }

    settingColor = settings.getColor();
    fontSize = settings.getFontSize();

    return Provider(
        create: (context) => MoorDb(),
        child: MaterialApp(
            theme: ThemeData(
                primarySwatch: getPrimarySwatch(settingColor),
                appBarTheme: AppBarTheme(
                    backgroundColor: Color(settingColor),
                    foregroundColor: Colors.white,
                    titleTextStyle: TextStyle(
                        fontWeight: FontWeight.bold, fontSize: fontSize + 5)),
                textTheme: TextTheme(
                    bodySmall: TextStyle(fontSize: fontSize - 2),
                    bodyMedium: TextStyle(fontSize: fontSize),
                    bodyLarge: TextStyle(fontSize: fontSize + 2))),
            //localizationsDelegates: [MonthYearPickerLocalizations.delegate],
            routes: {
              '/': (context) => IntroScreen(),
              '/search': (context) => SearchScreen(),
              '/observations': (context) => ObservationsScreen(),
              '/notes': (context) => NotelistScreen(),
              '/settings': (context) => SettingsScreen(),
              // TODO remove?
              '/hello': (context) => HelloScreen(),
              '/favorites': (context) => FavoriteScreen(),
              '/sessions': (context) => SessionsScreen(),
            }
            //,home: IntroScreen() // can't have home and routes defined
            ));
  }

  MaterialColor getPrimarySwatch(int settingColor) {
    if (settingColor == Colors.red.value) {
      return Colors.red;
    }
    if (settingColor == Colors.amber.value) {
      return Colors.amber;
    }
    if (settingColor == Colors.blue.value) {
      return Colors.blue;
    }
    if (settingColor == Colors.deepOrange.value) {
      return Colors.deepOrange;
    }
    if (settingColor == Colors.deepPurple.value) {
      return Colors.deepPurple;
    }
    if (settingColor == Colors.cyan.value) {
      return Colors.cyan;
    }
    return Colors.green;
  }
}
