import 'package:martincica_flutter_demo/data/model/db/sembast/observation.dart';
import 'package:martincica_flutter_demo/data/sebmast_codec.dart';
import 'package:martincica_flutter_demo/data/sp_helper.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sembast/sembast.dart';
import 'package:sembast/sembast_io.dart';

class SembastDb {
  DatabaseFactory dbFactory = databaseFactoryIo;
  bool isDbInitialized = false;
  late final Database _db;
  final store = stringMapStoreFactory.store('observations');
  var codec = getEncryptSembastCodec(password: 'big-secret');
  static final SembastDb _singleton = SembastDb._internal();

  SharedPreferencesHelper settings = SharedPreferencesHelper();
  late bool enableEncryption;

  SembastDb._internal();

  factory SembastDb() {
    return _singleton;
  }

  Future<Database> init() async {
    await settings.init();
    enableEncryption = settings.getEncryptDatabase();
    if (!isDbInitialized) {
      _db = await _openDb();
      isDbInitialized = true;
    }
    return _db;
  }

  Future _openDb() async {
    final docsDir = await getApplicationDocumentsDirectory();
    Database? db;
    if (enableEncryption) {
      final dbPath = join(docsDir.path, 'database', 'sembast_encrypted.db');
      db = await dbFactory.openDatabase(dbPath, codec: codec);
    } else {
      final dbPath = join(docsDir.path, 'database', 'sembast.db');
      db = await dbFactory.openDatabase(dbPath);
    }
    return db;
  }

  Future<String> addObservation(Observation observation) async {
    String id = await store.add(_db, observation.toMap());
    return id;
  }

  Future<List<Observation>> getObservations() async {
    final finder = Finder(sortOrders: [SortOrder('date', false, true)]);
    final snapshot = await store.find(_db, finder: finder);
    List<Observation> observations = snapshot.map((item) {
      final pwd = Observation.fromMap(item.value);
      pwd.id = item.key;
      return pwd;
    }).toList();
    return observations;
  }

  Future updateObservation(Observation pwd) async {
    final finder = Finder(filter: Filter.byKey(pwd.id));
    await store.update(_db, pwd.toMap(), finder: finder);
  }

  Future deleteObservation(Observation pwd) async {
    final finder = Finder(filter: Filter.byKey(pwd.id));
    await store.delete(_db, finder: finder);
  }

  Future deleteAll(Observation pwd) async {
    store.delete(_db);
  }
}
