// ignore_for_file: prefer_const_constructors

import 'package:drift/drift.dart';
import 'package:drift_sqflite/drift_sqflite.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/favorite_tags_dao.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/locations_dao.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/dao/tags_dao.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/favorite_tags.dart';

import 'model/db/drift/dao/blog_post_dao.dart';
import 'model/db/drift/dao/favorite_species_dao.dart';
import 'model/db/drift/dao/observations_dao.dart';
import 'model/db/drift/tables/blog_post.dart';
import 'model/db/drift/tables/favorite_species.dart';
import 'model/db/drift/tables/locations.dart';
import 'model/db/drift/tables/observations.dart';
import 'model/db/drift/tables/tags.dart';

part 'drift_db.g.dart';

@DriftDatabase(
    tables: [
      BlogPosts,
      FavoriteSpecies,
      FavoriteTags,
      Observations,
      Tags,
      Locations
    ],
    daos: [
      BlogPostDao,
      FavoriteSpeciesDao,
      FavoriteTagsDao,
      ObservationsDao,
      TagsDao,
      LocationsDao
    ]
    // ,include:  {'drift_db_native.drift'}
    // ,queries: {
    //   'searchObservations':
    //     'SELECT * FROM VObservations WHERE VObservations MATCH \'fts5\''
  // },
)
class MoorDb extends _$MoorDb {
  MoorDb()
      : super(SqfliteQueryExecutor.inDatabaseFolder(path: 'database/moor.db'));

  @override
  int get schemaVersion => 16;

  @override
  MigrationStrategy get migration =>
      MigrationStrategy(onUpgrade: (migrator, from, to) async {
        // //from: is the version we are currently running before upgrade
        // if (from == 1) {
        //   // We are telling the database to add the category column so it can be upgraded to version 2
        //   await migrator.addColumn(notesDB, notesDB.category);
        //   await migrator.createTable(todoDB);
        // }
        if (from <= 2) {
          // We are telling the database to add a new table called todoDB so it can be upgraded to version 3
          await migrator.createTable(observations);
        }

        // if (from == 3) {
        //   // We are telling the database to add a new table called todoDB so it can be upgraded to version 3
        //   await migrator.addColumn(observations, observations.scientificName);
        //   await migrator.addColumn(observations, observations.gbifKey);
        // }

        if (from <= 4) {
          await migrator.createTable(favoriteSpecies);
          await migrator.createTable(favoriteTags);
        }

        if (from <= 5) {
          await migrator.createTable(tags);
        }

        if (from == 6) {
          // drift version changed
        }

        if (from <= 7) {
          await migrator.createTable(locations);
        }

        if (from == 12) {
          // constraints are changed
          await migrator.alterTable(TableMigration(locations));
        }

        if (from == 13) {
          // constraints are changed
          await migrator.addColumn(observations, observations.longitude);
          await migrator.addColumn(observations, observations.latitude);
        }

        if (from == 14) {
          // constraints are changed
          await migrator.addColumn(observations, observations.notes);
        }
      });

  void importLocationsFromObservations() {
    observationsDao
        .getLocationSuggestions()
        .then((locations) {
          locationsDao.deleteAllLocation();
          locations.forEach((element) {
            LocationsCompanion lc = LocationsCompanion(
              name: Value(element),
              active: Value(true)
            );
            locationsDao.insert(lc);
          });
        });
  }
}