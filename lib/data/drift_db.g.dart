// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'drift_db.dart';

// ignore_for_file: type=lint
class $BlogPostsTable extends BlogPosts
    with TableInfo<$BlogPostsTable, BlogPost> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $BlogPostsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 80),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  static const VerificationMeta _contentMeta =
      const VerificationMeta('content');
  @override
  late final GeneratedColumn<String> content = GeneratedColumn<String>(
      'content', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _dateMeta = const VerificationMeta('date');
  @override
  late final GeneratedColumn<DateTime> date = GeneratedColumn<DateTime>(
      'date', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [id, name, content, date];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'blog_posts';
  @override
  VerificationContext validateIntegrity(Insertable<BlogPost> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('content')) {
      context.handle(_contentMeta,
          content.isAcceptableOrUnknown(data['content']!, _contentMeta));
    }
    if (data.containsKey('date')) {
      context.handle(
          _dateMeta, date.isAcceptableOrUnknown(data['date']!, _dateMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  BlogPost map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return BlogPost(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      content: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}content']),
      date: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}date']),
    );
  }

  @override
  $BlogPostsTable createAlias(String alias) {
    return $BlogPostsTable(attachedDatabase, alias);
  }
}

class BlogPost extends DataClass implements Insertable<BlogPost> {
  final int id;
  final String name;
  final String? content;
  final DateTime? date;
  const BlogPost(
      {required this.id, required this.name, this.content, this.date});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    if (!nullToAbsent || content != null) {
      map['content'] = Variable<String>(content);
    }
    if (!nullToAbsent || date != null) {
      map['date'] = Variable<DateTime>(date);
    }
    return map;
  }

  BlogPostsCompanion toCompanion(bool nullToAbsent) {
    return BlogPostsCompanion(
      id: Value(id),
      name: Value(name),
      content: content == null && nullToAbsent
          ? const Value.absent()
          : Value(content),
      date: date == null && nullToAbsent ? const Value.absent() : Value(date),
    );
  }

  factory BlogPost.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return BlogPost(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      content: serializer.fromJson<String?>(json['content']),
      date: serializer.fromJson<DateTime?>(json['date']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'content': serializer.toJson<String?>(content),
      'date': serializer.toJson<DateTime?>(date),
    };
  }

  BlogPost copyWith(
          {int? id,
          String? name,
          Value<String?> content = const Value.absent(),
          Value<DateTime?> date = const Value.absent()}) =>
      BlogPost(
        id: id ?? this.id,
        name: name ?? this.name,
        content: content.present ? content.value : this.content,
        date: date.present ? date.value : this.date,
      );
  @override
  String toString() {
    return (StringBuffer('BlogPost(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('content: $content, ')
          ..write('date: $date')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name, content, date);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is BlogPost &&
          other.id == this.id &&
          other.name == this.name &&
          other.content == this.content &&
          other.date == this.date);
}

class BlogPostsCompanion extends UpdateCompanion<BlogPost> {
  final Value<int> id;
  final Value<String> name;
  final Value<String?> content;
  final Value<DateTime?> date;
  const BlogPostsCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.content = const Value.absent(),
    this.date = const Value.absent(),
  });
  BlogPostsCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    this.content = const Value.absent(),
    this.date = const Value.absent(),
  }) : name = Value(name);
  static Insertable<BlogPost> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<String>? content,
    Expression<DateTime>? date,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (content != null) 'content': content,
      if (date != null) 'date': date,
    });
  }

  BlogPostsCompanion copyWith(
      {Value<int>? id,
      Value<String>? name,
      Value<String?>? content,
      Value<DateTime?>? date}) {
    return BlogPostsCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      content: content ?? this.content,
      date: date ?? this.date,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (content.present) {
      map['content'] = Variable<String>(content.value);
    }
    if (date.present) {
      map['date'] = Variable<DateTime>(date.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('BlogPostsCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('content: $content, ')
          ..write('date: $date')
          ..write(')'))
        .toString();
  }
}

class $FavoriteSpeciesTable extends FavoriteSpecies
    with TableInfo<$FavoriteSpeciesTable, FavoriteSpecy> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FavoriteSpeciesTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _canonicalNameMeta =
      const VerificationMeta('canonicalName');
  @override
  late final GeneratedColumn<String> canonicalName = GeneratedColumn<String>(
      'canonical_name', aliasedName, false,
      additionalChecks:
          GeneratedColumn.checkTextLength(minTextLength: 1, maxTextLength: 300),
      type: DriftSqlType.string,
      requiredDuringInsert: true);
  static const VerificationMeta _createdMeta =
      const VerificationMeta('created');
  @override
  late final GeneratedColumn<DateTime> created = GeneratedColumn<DateTime>(
      'created', aliasedName, true,
      type: DriftSqlType.dateTime, requiredDuringInsert: false);
  static const VerificationMeta _gbifKeyMeta =
      const VerificationMeta('gbifKey');
  @override
  late final GeneratedColumn<int> gbifKey = GeneratedColumn<int>(
      'gbif_key', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  @override
  List<GeneratedColumn> get $columns => [id, canonicalName, created, gbifKey];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'favorite_species';
  @override
  VerificationContext validateIntegrity(Insertable<FavoriteSpecy> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('canonical_name')) {
      context.handle(
          _canonicalNameMeta,
          canonicalName.isAcceptableOrUnknown(
              data['canonical_name']!, _canonicalNameMeta));
    } else if (isInserting) {
      context.missing(_canonicalNameMeta);
    }
    if (data.containsKey('created')) {
      context.handle(_createdMeta,
          created.isAcceptableOrUnknown(data['created']!, _createdMeta));
    }
    if (data.containsKey('gbif_key')) {
      context.handle(_gbifKeyMeta,
          gbifKey.isAcceptableOrUnknown(data['gbif_key']!, _gbifKeyMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FavoriteSpecy map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FavoriteSpecy(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      canonicalName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}canonical_name'])!,
      created: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}created']),
      gbifKey: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}gbif_key']),
    );
  }

  @override
  $FavoriteSpeciesTable createAlias(String alias) {
    return $FavoriteSpeciesTable(attachedDatabase, alias);
  }
}

class FavoriteSpecy extends DataClass implements Insertable<FavoriteSpecy> {
  final int id;
  final String canonicalName;
  final DateTime? created;
  final int? gbifKey;
  const FavoriteSpecy(
      {required this.id,
      required this.canonicalName,
      this.created,
      this.gbifKey});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['canonical_name'] = Variable<String>(canonicalName);
    if (!nullToAbsent || created != null) {
      map['created'] = Variable<DateTime>(created);
    }
    if (!nullToAbsent || gbifKey != null) {
      map['gbif_key'] = Variable<int>(gbifKey);
    }
    return map;
  }

  FavoriteSpeciesCompanion toCompanion(bool nullToAbsent) {
    return FavoriteSpeciesCompanion(
      id: Value(id),
      canonicalName: Value(canonicalName),
      created: created == null && nullToAbsent
          ? const Value.absent()
          : Value(created),
      gbifKey: gbifKey == null && nullToAbsent
          ? const Value.absent()
          : Value(gbifKey),
    );
  }

  factory FavoriteSpecy.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FavoriteSpecy(
      id: serializer.fromJson<int>(json['id']),
      canonicalName: serializer.fromJson<String>(json['canonicalName']),
      created: serializer.fromJson<DateTime?>(json['created']),
      gbifKey: serializer.fromJson<int?>(json['gbifKey']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'canonicalName': serializer.toJson<String>(canonicalName),
      'created': serializer.toJson<DateTime?>(created),
      'gbifKey': serializer.toJson<int?>(gbifKey),
    };
  }

  FavoriteSpecy copyWith(
          {int? id,
          String? canonicalName,
          Value<DateTime?> created = const Value.absent(),
          Value<int?> gbifKey = const Value.absent()}) =>
      FavoriteSpecy(
        id: id ?? this.id,
        canonicalName: canonicalName ?? this.canonicalName,
        created: created.present ? created.value : this.created,
        gbifKey: gbifKey.present ? gbifKey.value : this.gbifKey,
      );
  @override
  String toString() {
    return (StringBuffer('FavoriteSpecy(')
          ..write('id: $id, ')
          ..write('canonicalName: $canonicalName, ')
          ..write('created: $created, ')
          ..write('gbifKey: $gbifKey')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, canonicalName, created, gbifKey);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FavoriteSpecy &&
          other.id == this.id &&
          other.canonicalName == this.canonicalName &&
          other.created == this.created &&
          other.gbifKey == this.gbifKey);
}

class FavoriteSpeciesCompanion extends UpdateCompanion<FavoriteSpecy> {
  final Value<int> id;
  final Value<String> canonicalName;
  final Value<DateTime?> created;
  final Value<int?> gbifKey;
  const FavoriteSpeciesCompanion({
    this.id = const Value.absent(),
    this.canonicalName = const Value.absent(),
    this.created = const Value.absent(),
    this.gbifKey = const Value.absent(),
  });
  FavoriteSpeciesCompanion.insert({
    this.id = const Value.absent(),
    required String canonicalName,
    this.created = const Value.absent(),
    this.gbifKey = const Value.absent(),
  }) : canonicalName = Value(canonicalName);
  static Insertable<FavoriteSpecy> custom({
    Expression<int>? id,
    Expression<String>? canonicalName,
    Expression<DateTime>? created,
    Expression<int>? gbifKey,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (canonicalName != null) 'canonical_name': canonicalName,
      if (created != null) 'created': created,
      if (gbifKey != null) 'gbif_key': gbifKey,
    });
  }

  FavoriteSpeciesCompanion copyWith(
      {Value<int>? id,
      Value<String>? canonicalName,
      Value<DateTime?>? created,
      Value<int?>? gbifKey}) {
    return FavoriteSpeciesCompanion(
      id: id ?? this.id,
      canonicalName: canonicalName ?? this.canonicalName,
      created: created ?? this.created,
      gbifKey: gbifKey ?? this.gbifKey,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (canonicalName.present) {
      map['canonical_name'] = Variable<String>(canonicalName.value);
    }
    if (created.present) {
      map['created'] = Variable<DateTime>(created.value);
    }
    if (gbifKey.present) {
      map['gbif_key'] = Variable<int>(gbifKey.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FavoriteSpeciesCompanion(')
          ..write('id: $id, ')
          ..write('canonicalName: $canonicalName, ')
          ..write('created: $created, ')
          ..write('gbifKey: $gbifKey')
          ..write(')'))
        .toString();
  }
}

class $FavoriteTagsTable extends FavoriteTags
    with TableInfo<$FavoriteTagsTable, FavoriteTag> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $FavoriteTagsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _favoriteSpeciesIdMeta =
      const VerificationMeta('favoriteSpeciesId');
  @override
  late final GeneratedColumn<int> favoriteSpeciesId = GeneratedColumn<int>(
      'favorite_species_id', aliasedName, true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints: GeneratedColumn.constraintIsAlways(
          'REFERENCES favorite_species (id)'));
  static const VerificationMeta _tagMeta = const VerificationMeta('tag');
  @override
  late final GeneratedColumn<String> tag = GeneratedColumn<String>(
      'tag', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _activeMeta = const VerificationMeta('active');
  @override
  late final GeneratedColumn<bool> active = GeneratedColumn<bool>(
      'active', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("active" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [id, favoriteSpeciesId, tag, active];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'favorite_tags';
  @override
  VerificationContext validateIntegrity(Insertable<FavoriteTag> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('favorite_species_id')) {
      context.handle(
          _favoriteSpeciesIdMeta,
          favoriteSpeciesId.isAcceptableOrUnknown(
              data['favorite_species_id']!, _favoriteSpeciesIdMeta));
    }
    if (data.containsKey('tag')) {
      context.handle(
          _tagMeta, tag.isAcceptableOrUnknown(data['tag']!, _tagMeta));
    } else if (isInserting) {
      context.missing(_tagMeta);
    }
    if (data.containsKey('active')) {
      context.handle(_activeMeta,
          active.isAcceptableOrUnknown(data['active']!, _activeMeta));
    } else if (isInserting) {
      context.missing(_activeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  FavoriteTag map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return FavoriteTag(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      favoriteSpeciesId: attachedDatabase.typeMapping.read(
          DriftSqlType.int, data['${effectivePrefix}favorite_species_id']),
      tag: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}tag'])!,
      active: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}active'])!,
    );
  }

  @override
  $FavoriteTagsTable createAlias(String alias) {
    return $FavoriteTagsTable(attachedDatabase, alias);
  }
}

class FavoriteTag extends DataClass implements Insertable<FavoriteTag> {
  final int id;
  final int? favoriteSpeciesId;
  final String tag;
  final bool active;
  const FavoriteTag(
      {required this.id,
      this.favoriteSpeciesId,
      required this.tag,
      required this.active});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    if (!nullToAbsent || favoriteSpeciesId != null) {
      map['favorite_species_id'] = Variable<int>(favoriteSpeciesId);
    }
    map['tag'] = Variable<String>(tag);
    map['active'] = Variable<bool>(active);
    return map;
  }

  FavoriteTagsCompanion toCompanion(bool nullToAbsent) {
    return FavoriteTagsCompanion(
      id: Value(id),
      favoriteSpeciesId: favoriteSpeciesId == null && nullToAbsent
          ? const Value.absent()
          : Value(favoriteSpeciesId),
      tag: Value(tag),
      active: Value(active),
    );
  }

  factory FavoriteTag.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return FavoriteTag(
      id: serializer.fromJson<int>(json['id']),
      favoriteSpeciesId: serializer.fromJson<int?>(json['favoriteSpeciesId']),
      tag: serializer.fromJson<String>(json['tag']),
      active: serializer.fromJson<bool>(json['active']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'favoriteSpeciesId': serializer.toJson<int?>(favoriteSpeciesId),
      'tag': serializer.toJson<String>(tag),
      'active': serializer.toJson<bool>(active),
    };
  }

  FavoriteTag copyWith(
          {int? id,
          Value<int?> favoriteSpeciesId = const Value.absent(),
          String? tag,
          bool? active}) =>
      FavoriteTag(
        id: id ?? this.id,
        favoriteSpeciesId: favoriteSpeciesId.present
            ? favoriteSpeciesId.value
            : this.favoriteSpeciesId,
        tag: tag ?? this.tag,
        active: active ?? this.active,
      );
  @override
  String toString() {
    return (StringBuffer('FavoriteTag(')
          ..write('id: $id, ')
          ..write('favoriteSpeciesId: $favoriteSpeciesId, ')
          ..write('tag: $tag, ')
          ..write('active: $active')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, favoriteSpeciesId, tag, active);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is FavoriteTag &&
          other.id == this.id &&
          other.favoriteSpeciesId == this.favoriteSpeciesId &&
          other.tag == this.tag &&
          other.active == this.active);
}

class FavoriteTagsCompanion extends UpdateCompanion<FavoriteTag> {
  final Value<int> id;
  final Value<int?> favoriteSpeciesId;
  final Value<String> tag;
  final Value<bool> active;
  const FavoriteTagsCompanion({
    this.id = const Value.absent(),
    this.favoriteSpeciesId = const Value.absent(),
    this.tag = const Value.absent(),
    this.active = const Value.absent(),
  });
  FavoriteTagsCompanion.insert({
    this.id = const Value.absent(),
    this.favoriteSpeciesId = const Value.absent(),
    required String tag,
    required bool active,
  })  : tag = Value(tag),
        active = Value(active);
  static Insertable<FavoriteTag> custom({
    Expression<int>? id,
    Expression<int>? favoriteSpeciesId,
    Expression<String>? tag,
    Expression<bool>? active,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (favoriteSpeciesId != null) 'favorite_species_id': favoriteSpeciesId,
      if (tag != null) 'tag': tag,
      if (active != null) 'active': active,
    });
  }

  FavoriteTagsCompanion copyWith(
      {Value<int>? id,
      Value<int?>? favoriteSpeciesId,
      Value<String>? tag,
      Value<bool>? active}) {
    return FavoriteTagsCompanion(
      id: id ?? this.id,
      favoriteSpeciesId: favoriteSpeciesId ?? this.favoriteSpeciesId,
      tag: tag ?? this.tag,
      active: active ?? this.active,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (favoriteSpeciesId.present) {
      map['favorite_species_id'] = Variable<int>(favoriteSpeciesId.value);
    }
    if (tag.present) {
      map['tag'] = Variable<String>(tag.value);
    }
    if (active.present) {
      map['active'] = Variable<bool>(active.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FavoriteTagsCompanion(')
          ..write('id: $id, ')
          ..write('favoriteSpeciesId: $favoriteSpeciesId, ')
          ..write('tag: $tag, ')
          ..write('active: $active')
          ..write(')'))
        .toString();
  }
}

class $ObservationsTable extends Observations
    with TableInfo<$ObservationsTable, Observation> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $ObservationsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String> description = GeneratedColumn<String>(
      'description', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _scientificNameMeta =
      const VerificationMeta('scientificName');
  @override
  late final GeneratedColumn<String> scientificName = GeneratedColumn<String>(
      'scientific_name', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _notesMeta = const VerificationMeta('notes');
  @override
  late final GeneratedColumn<String> notes = GeneratedColumn<String>(
      'notes', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _gbifKeyMeta =
      const VerificationMeta('gbifKey');
  @override
  late final GeneratedColumn<int> gbifKey = GeneratedColumn<int>(
      'gbif_key', aliasedName, true,
      type: DriftSqlType.int, requiredDuringInsert: false);
  static const VerificationMeta _locationMeta =
      const VerificationMeta('location');
  @override
  late final GeneratedColumn<String> location = GeneratedColumn<String>(
      'location', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _longitudeMeta =
      const VerificationMeta('longitude');
  @override
  late final GeneratedColumn<double> longitude = GeneratedColumn<double>(
      'longitude', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  static const VerificationMeta _latitudeMeta =
      const VerificationMeta('latitude');
  @override
  late final GeneratedColumn<double> latitude = GeneratedColumn<double>(
      'latitude', aliasedName, true,
      type: DriftSqlType.double, requiredDuringInsert: false);
  static const VerificationMeta _dateMeta = const VerificationMeta('date');
  @override
  late final GeneratedColumn<DateTime> date = GeneratedColumn<DateTime>(
      'date', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _isMineMeta = const VerificationMeta('isMine');
  @override
  late final GeneratedColumn<bool> isMine = GeneratedColumn<bool>(
      'is_mine', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("is_mine" IN (0, 1))'));

  @override
  List<GeneratedColumn> get $columns => [
        id,
        description,
        scientificName,
        notes,
        gbifKey,
        location,
        longitude,
        latitude,
        date,
        isMine
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'observations';
  @override
  VerificationContext validateIntegrity(Insertable<Observation> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    } else if (isInserting) {
      context.missing(_descriptionMeta);
    }
    if (data.containsKey('scientific_name')) {
      context.handle(
          _scientificNameMeta,
          scientificName.isAcceptableOrUnknown(
              data['scientific_name']!, _scientificNameMeta));
    }
    if (data.containsKey('notes')) {
      context.handle(
          _notesMeta, notes.isAcceptableOrUnknown(data['notes']!, _notesMeta));
    }
    if (data.containsKey('gbif_key')) {
      context.handle(_gbifKeyMeta,
          gbifKey.isAcceptableOrUnknown(data['gbif_key']!, _gbifKeyMeta));
    }
    if (data.containsKey('location')) {
      context.handle(_locationMeta,
          location.isAcceptableOrUnknown(data['location']!, _locationMeta));
    } else if (isInserting) {
      context.missing(_locationMeta);
    }
    if (data.containsKey('longitude')) {
      context.handle(_longitudeMeta,
          longitude.isAcceptableOrUnknown(data['longitude']!, _longitudeMeta));
    }
    if (data.containsKey('latitude')) {
      context.handle(_latitudeMeta,
          latitude.isAcceptableOrUnknown(data['latitude']!, _latitudeMeta));
    }
    if (data.containsKey('date')) {
      context.handle(
          _dateMeta, date.isAcceptableOrUnknown(data['date']!, _dateMeta));
    } else if (isInserting) {
      context.missing(_dateMeta);
    }
    if (data.containsKey('is_mine')) {
      context.handle(_isMineMeta,
          isMine.isAcceptableOrUnknown(data['is_mine']!, _isMineMeta));
    } else if (isInserting) {
      context.missing(_isMineMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Observation map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Observation(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      description: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}description'])!,
      scientificName: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}scientific_name']),
      notes: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}notes']),
      gbifKey: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}gbif_key']),
      location: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}location'])!,
      longitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}longitude']),
      latitude: attachedDatabase.typeMapping
          .read(DriftSqlType.double, data['${effectivePrefix}latitude']),
      date: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}date'])!,
      isMine: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}is_mine'])!,
    );
  }

  @override
  $ObservationsTable createAlias(String alias) {
    return $ObservationsTable(attachedDatabase, alias);
  }
}

class Observation extends DataClass implements Insertable<Observation> {
  final int id;
  final String description;
  final String? scientificName;
  final String? notes;
  final int? gbifKey;
  final String location;
  final double? longitude;
  final double? latitude;
  final DateTime date;
  final bool isMine;

  const Observation(
      {required this.id,
      required this.description,
      this.scientificName,
      this.notes,
      this.gbifKey,
      required this.location,
      this.longitude,
      this.latitude,
      required this.date,
      required this.isMine});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['description'] = Variable<String>(description);
    if (!nullToAbsent || scientificName != null) {
      map['scientific_name'] = Variable<String>(scientificName);
    }
    if (!nullToAbsent || notes != null) {
      map['notes'] = Variable<String>(notes);
    }
    if (!nullToAbsent || gbifKey != null) {
      map['gbif_key'] = Variable<int>(gbifKey);
    }
    map['location'] = Variable<String>(location);
    if (!nullToAbsent || longitude != null) {
      map['longitude'] = Variable<double>(longitude);
    }
    if (!nullToAbsent || latitude != null) {
      map['latitude'] = Variable<double>(latitude);
    }
    map['date'] = Variable<DateTime>(date);
    map['is_mine'] = Variable<bool>(isMine);
    return map;
  }

  ObservationsCompanion toCompanion(bool nullToAbsent) {
    return ObservationsCompanion(
      id: Value(id),
      description: Value(description),
      scientificName: scientificName == null && nullToAbsent
          ? const Value.absent()
          : Value(scientificName),
      notes:
          notes == null && nullToAbsent ? const Value.absent() : Value(notes),
      gbifKey: gbifKey == null && nullToAbsent
          ? const Value.absent()
          : Value(gbifKey),
      location: Value(location),
      longitude: longitude == null && nullToAbsent
          ? const Value.absent()
          : Value(longitude),
      latitude: latitude == null && nullToAbsent
          ? const Value.absent()
          : Value(latitude),
      date: Value(date),
      isMine: Value(isMine),
    );
  }

  factory Observation.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Observation(
      id: serializer.fromJson<int>(json['id']),
      description: serializer.fromJson<String>(json['description']),
      scientificName: serializer.fromJson<String?>(json['scientificName']),
      notes: serializer.fromJson<String?>(json['notes']),
      gbifKey: serializer.fromJson<int?>(json['gbifKey']),
      location: serializer.fromJson<String>(json['location']),
      longitude: serializer.fromJson<double?>(json['longitude']),
      latitude: serializer.fromJson<double?>(json['latitude']),
      date: serializer.fromJson<DateTime>(json['date']),
      isMine: serializer.fromJson<bool>(json['isMine']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'description': serializer.toJson<String>(description),
      'scientificName': serializer.toJson<String?>(scientificName),
      'notes': serializer.toJson<String?>(notes),
      'gbifKey': serializer.toJson<int?>(gbifKey),
      'location': serializer.toJson<String>(location),
      'longitude': serializer.toJson<double?>(longitude),
      'latitude': serializer.toJson<double?>(latitude),
      'date': serializer.toJson<DateTime>(date),
      'isMine': serializer.toJson<bool>(isMine),
    };
  }

  Observation copyWith(
          {int? id,
          String? description,
          Value<String?> scientificName = const Value.absent(),
          Value<String?> notes = const Value.absent(),
          Value<int?> gbifKey = const Value.absent(),
          String? location,
          Value<double?> longitude = const Value.absent(),
          Value<double?> latitude = const Value.absent(),
          DateTime? date,
          bool? isMine}) =>
      Observation(
        id: id ?? this.id,
        description: description ?? this.description,
        scientificName:
            scientificName.present ? scientificName.value : this.scientificName,
        notes: notes.present ? notes.value : this.notes,
        gbifKey: gbifKey.present ? gbifKey.value : this.gbifKey,
        location: location ?? this.location,
        longitude: longitude.present ? longitude.value : this.longitude,
        latitude: latitude.present ? latitude.value : this.latitude,
        date: date ?? this.date,
        isMine: isMine ?? this.isMine,
      );
  @override
  String toString() {
    return (StringBuffer('Observation(')
          ..write('id: $id, ')
          ..write('description: $description, ')
          ..write('scientificName: $scientificName, ')
          ..write('notes: $notes, ')
          ..write('gbifKey: $gbifKey, ')
          ..write('location: $location, ')
          ..write('longitude: $longitude, ')
          ..write('latitude: $latitude, ')
          ..write('date: $date, ')
          ..write('isMine: $isMine')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, description, scientificName, notes,
      gbifKey, location, longitude, latitude, date, isMine);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Observation &&
          other.id == this.id &&
          other.description == this.description &&
          other.scientificName == this.scientificName &&
          other.notes == this.notes &&
          other.gbifKey == this.gbifKey &&
          other.location == this.location &&
          other.longitude == this.longitude &&
          other.latitude == this.latitude &&
          other.date == this.date &&
          other.isMine == this.isMine);
}

class ObservationsCompanion extends UpdateCompanion<Observation> {
  final Value<int> id;
  final Value<String> description;
  final Value<String?> scientificName;
  final Value<String?> notes;
  final Value<int?> gbifKey;
  final Value<String> location;
  final Value<double?> longitude;
  final Value<double?> latitude;
  final Value<DateTime> date;
  final Value<bool> isMine;

  const ObservationsCompanion({
    this.id = const Value.absent(),
    this.description = const Value.absent(),
    this.scientificName = const Value.absent(),
    this.notes = const Value.absent(),
    this.gbifKey = const Value.absent(),
    this.location = const Value.absent(),
    this.longitude = const Value.absent(),
    this.latitude = const Value.absent(),
    this.date = const Value.absent(),
    this.isMine = const Value.absent(),
  });
  ObservationsCompanion.insert({
    this.id = const Value.absent(),
    required String description,
    this.scientificName = const Value.absent(),
    this.notes = const Value.absent(),
    this.gbifKey = const Value.absent(),
    required String location,
    this.longitude = const Value.absent(),
    this.latitude = const Value.absent(),
    required DateTime date,
    required bool isMine,
  })  : description = Value(description),
        location = Value(location),
        date = Value(date),
        isMine = Value(isMine);
  static Insertable<Observation> custom({
    Expression<int>? id,
    Expression<String>? description,
    Expression<String>? scientificName,
    Expression<String>? notes,
    Expression<int>? gbifKey,
    Expression<String>? location,
    Expression<double>? longitude,
    Expression<double>? latitude,
    Expression<DateTime>? date,
    Expression<bool>? isMine,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (description != null) 'description': description,
      if (scientificName != null) 'scientific_name': scientificName,
      if (notes != null) 'notes': notes,
      if (gbifKey != null) 'gbif_key': gbifKey,
      if (location != null) 'location': location,
      if (longitude != null) 'longitude': longitude,
      if (latitude != null) 'latitude': latitude,
      if (date != null) 'date': date,
      if (isMine != null) 'is_mine': isMine,
    });
  }

  ObservationsCompanion copyWith(
      {Value<int>? id,
      Value<String>? description,
      Value<String?>? scientificName,
      Value<String?>? notes,
      Value<int?>? gbifKey,
      Value<String>? location,
      Value<double?>? longitude,
      Value<double?>? latitude,
      Value<DateTime>? date,
      Value<bool>? isMine}) {
    return ObservationsCompanion(
      id: id ?? this.id,
      description: description ?? this.description,
      scientificName: scientificName ?? this.scientificName,
      notes: notes ?? this.notes,
      gbifKey: gbifKey ?? this.gbifKey,
      location: location ?? this.location,
      longitude: longitude ?? this.longitude,
      latitude: latitude ?? this.latitude,
      date: date ?? this.date,
      isMine: isMine ?? this.isMine,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (scientificName.present) {
      map['scientific_name'] = Variable<String>(scientificName.value);
    }
    if (notes.present) {
      map['notes'] = Variable<String>(notes.value);
    }
    if (gbifKey.present) {
      map['gbif_key'] = Variable<int>(gbifKey.value);
    }
    if (location.present) {
      map['location'] = Variable<String>(location.value);
    }
    if (longitude.present) {
      map['longitude'] = Variable<double>(longitude.value);
    }
    if (latitude.present) {
      map['latitude'] = Variable<double>(latitude.value);
    }
    if (date.present) {
      map['date'] = Variable<DateTime>(date.value);
    }
    if (isMine.present) {
      map['is_mine'] = Variable<bool>(isMine.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ObservationsCompanion(')
          ..write('id: $id, ')
          ..write('description: $description, ')
          ..write('scientificName: $scientificName, ')
          ..write('notes: $notes, ')
          ..write('gbifKey: $gbifKey, ')
          ..write('location: $location, ')
          ..write('longitude: $longitude, ')
          ..write('latitude: $latitude, ')
          ..write('date: $date, ')
          ..write('isMine: $isMine')
          ..write(')'))
        .toString();
  }
}

class $TagsTable extends Tags with TableInfo<$TagsTable, Tag> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $TagsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _refIdMeta = const VerificationMeta('refId');
  @override
  late final GeneratedColumn<String> refId = GeneratedColumn<String>(
      'ref_id', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _typeMeta = const VerificationMeta('type');
  @override
  late final GeneratedColumn<String> type = GeneratedColumn<String>(
      'type', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _tagMeta = const VerificationMeta('tag');
  @override
  late final GeneratedColumn<String> tag = GeneratedColumn<String>(
      'tag', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _activeMeta = const VerificationMeta('active');
  @override
  late final GeneratedColumn<bool> active = GeneratedColumn<bool>(
      'active', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("active" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns => [id, refId, type, tag, active];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'tags';
  @override
  VerificationContext validateIntegrity(Insertable<Tag> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('ref_id')) {
      context.handle(
          _refIdMeta, refId.isAcceptableOrUnknown(data['ref_id']!, _refIdMeta));
    } else if (isInserting) {
      context.missing(_refIdMeta);
    }
    if (data.containsKey('type')) {
      context.handle(
          _typeMeta, type.isAcceptableOrUnknown(data['type']!, _typeMeta));
    } else if (isInserting) {
      context.missing(_typeMeta);
    }
    if (data.containsKey('tag')) {
      context.handle(
          _tagMeta, tag.isAcceptableOrUnknown(data['tag']!, _tagMeta));
    } else if (isInserting) {
      context.missing(_tagMeta);
    }
    if (data.containsKey('active')) {
      context.handle(_activeMeta,
          active.isAcceptableOrUnknown(data['active']!, _activeMeta));
    } else if (isInserting) {
      context.missing(_activeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Tag map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Tag(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      refId: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}ref_id'])!,
      type: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}type'])!,
      tag: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}tag'])!,
      active: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}active'])!,
    );
  }

  @override
  $TagsTable createAlias(String alias) {
    return $TagsTable(attachedDatabase, alias);
  }
}

class Tag extends DataClass implements Insertable<Tag> {
  final int id;
  final String refId;
  final String type;
  final String tag;
  final bool active;
  const Tag(
      {required this.id,
      required this.refId,
      required this.type,
      required this.tag,
      required this.active});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['ref_id'] = Variable<String>(refId);
    map['type'] = Variable<String>(type);
    map['tag'] = Variable<String>(tag);
    map['active'] = Variable<bool>(active);
    return map;
  }

  TagsCompanion toCompanion(bool nullToAbsent) {
    return TagsCompanion(
      id: Value(id),
      refId: Value(refId),
      type: Value(type),
      tag: Value(tag),
      active: Value(active),
    );
  }

  factory Tag.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Tag(
      id: serializer.fromJson<int>(json['id']),
      refId: serializer.fromJson<String>(json['refId']),
      type: serializer.fromJson<String>(json['type']),
      tag: serializer.fromJson<String>(json['tag']),
      active: serializer.fromJson<bool>(json['active']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'refId': serializer.toJson<String>(refId),
      'type': serializer.toJson<String>(type),
      'tag': serializer.toJson<String>(tag),
      'active': serializer.toJson<bool>(active),
    };
  }

  Tag copyWith(
          {int? id, String? refId, String? type, String? tag, bool? active}) =>
      Tag(
        id: id ?? this.id,
        refId: refId ?? this.refId,
        type: type ?? this.type,
        tag: tag ?? this.tag,
        active: active ?? this.active,
      );
  @override
  String toString() {
    return (StringBuffer('Tag(')
          ..write('id: $id, ')
          ..write('refId: $refId, ')
          ..write('type: $type, ')
          ..write('tag: $tag, ')
          ..write('active: $active')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, refId, type, tag, active);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Tag &&
          other.id == this.id &&
          other.refId == this.refId &&
          other.type == this.type &&
          other.tag == this.tag &&
          other.active == this.active);
}

class TagsCompanion extends UpdateCompanion<Tag> {
  final Value<int> id;
  final Value<String> refId;
  final Value<String> type;
  final Value<String> tag;
  final Value<bool> active;
  const TagsCompanion({
    this.id = const Value.absent(),
    this.refId = const Value.absent(),
    this.type = const Value.absent(),
    this.tag = const Value.absent(),
    this.active = const Value.absent(),
  });
  TagsCompanion.insert({
    this.id = const Value.absent(),
    required String refId,
    required String type,
    required String tag,
    required bool active,
  })  : refId = Value(refId),
        type = Value(type),
        tag = Value(tag),
        active = Value(active);
  static Insertable<Tag> custom({
    Expression<int>? id,
    Expression<String>? refId,
    Expression<String>? type,
    Expression<String>? tag,
    Expression<bool>? active,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (refId != null) 'ref_id': refId,
      if (type != null) 'type': type,
      if (tag != null) 'tag': tag,
      if (active != null) 'active': active,
    });
  }

  TagsCompanion copyWith(
      {Value<int>? id,
      Value<String>? refId,
      Value<String>? type,
      Value<String>? tag,
      Value<bool>? active}) {
    return TagsCompanion(
      id: id ?? this.id,
      refId: refId ?? this.refId,
      type: type ?? this.type,
      tag: tag ?? this.tag,
      active: active ?? this.active,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (refId.present) {
      map['ref_id'] = Variable<String>(refId.value);
    }
    if (type.present) {
      map['type'] = Variable<String>(type.value);
    }
    if (tag.present) {
      map['tag'] = Variable<String>(tag.value);
    }
    if (active.present) {
      map['active'] = Variable<bool>(active.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('TagsCompanion(')
          ..write('id: $id, ')
          ..write('refId: $refId, ')
          ..write('type: $type, ')
          ..write('tag: $tag, ')
          ..write('active: $active')
          ..write(')'))
        .toString();
  }
}

class $LocationsTable extends Locations
    with TableInfo<$LocationsTable, Location> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $LocationsTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _descriptionMeta =
      const VerificationMeta('description');
  @override
  late final GeneratedColumn<String> description = GeneratedColumn<String>(
      'description', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _geoLocationMeta =
      const VerificationMeta('geoLocation');
  @override
  late final GeneratedColumn<String> geoLocation = GeneratedColumn<String>(
      'geo_location', aliasedName, true,
      type: DriftSqlType.string, requiredDuringInsert: false);
  static const VerificationMeta _activeMeta = const VerificationMeta('active');
  @override
  late final GeneratedColumn<bool> active = GeneratedColumn<bool>(
      'active', aliasedName, false,
      type: DriftSqlType.bool,
      requiredDuringInsert: true,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('CHECK ("active" IN (0, 1))'));
  @override
  List<GeneratedColumn> get $columns =>
      [id, name, description, geoLocation, active];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'locations';
  @override
  VerificationContext validateIntegrity(Insertable<Location> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('description')) {
      context.handle(
          _descriptionMeta,
          description.isAcceptableOrUnknown(
              data['description']!, _descriptionMeta));
    }
    if (data.containsKey('geo_location')) {
      context.handle(
          _geoLocationMeta,
          geoLocation.isAcceptableOrUnknown(
              data['geo_location']!, _geoLocationMeta));
    }
    if (data.containsKey('active')) {
      context.handle(_activeMeta,
          active.isAcceptableOrUnknown(data['active']!, _activeMeta));
    } else if (isInserting) {
      context.missing(_activeMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Location map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return Location(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      description: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}description']),
      geoLocation: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}geo_location']),
      active: attachedDatabase.typeMapping
          .read(DriftSqlType.bool, data['${effectivePrefix}active'])!,
    );
  }

  @override
  $LocationsTable createAlias(String alias) {
    return $LocationsTable(attachedDatabase, alias);
  }
}

class Location extends DataClass implements Insertable<Location> {
  final int id;
  final String name;
  final String? description;
  final String? geoLocation;
  final bool active;
  const Location(
      {required this.id,
      required this.name,
      this.description,
      this.geoLocation,
      required this.active});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['name'] = Variable<String>(name);
    if (!nullToAbsent || description != null) {
      map['description'] = Variable<String>(description);
    }
    if (!nullToAbsent || geoLocation != null) {
      map['geo_location'] = Variable<String>(geoLocation);
    }
    map['active'] = Variable<bool>(active);
    return map;
  }

  LocationsCompanion toCompanion(bool nullToAbsent) {
    return LocationsCompanion(
      id: Value(id),
      name: Value(name),
      description: description == null && nullToAbsent
          ? const Value.absent()
          : Value(description),
      geoLocation: geoLocation == null && nullToAbsent
          ? const Value.absent()
          : Value(geoLocation),
      active: Value(active),
    );
  }

  factory Location.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return Location(
      id: serializer.fromJson<int>(json['id']),
      name: serializer.fromJson<String>(json['name']),
      description: serializer.fromJson<String?>(json['description']),
      geoLocation: serializer.fromJson<String?>(json['geoLocation']),
      active: serializer.fromJson<bool>(json['active']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'name': serializer.toJson<String>(name),
      'description': serializer.toJson<String?>(description),
      'geoLocation': serializer.toJson<String?>(geoLocation),
      'active': serializer.toJson<bool>(active),
    };
  }

  Location copyWith(
          {int? id,
          String? name,
          Value<String?> description = const Value.absent(),
          Value<String?> geoLocation = const Value.absent(),
          bool? active}) =>
      Location(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description.present ? description.value : this.description,
        geoLocation: geoLocation.present ? geoLocation.value : this.geoLocation,
        active: active ?? this.active,
      );
  @override
  String toString() {
    return (StringBuffer('Location(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('description: $description, ')
          ..write('geoLocation: $geoLocation, ')
          ..write('active: $active')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(id, name, description, geoLocation, active);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is Location &&
          other.id == this.id &&
          other.name == this.name &&
          other.description == this.description &&
          other.geoLocation == this.geoLocation &&
          other.active == this.active);
}

class LocationsCompanion extends UpdateCompanion<Location> {
  final Value<int> id;
  final Value<String> name;
  final Value<String?> description;
  final Value<String?> geoLocation;
  final Value<bool> active;
  const LocationsCompanion({
    this.id = const Value.absent(),
    this.name = const Value.absent(),
    this.description = const Value.absent(),
    this.geoLocation = const Value.absent(),
    this.active = const Value.absent(),
  });
  LocationsCompanion.insert({
    this.id = const Value.absent(),
    required String name,
    this.description = const Value.absent(),
    this.geoLocation = const Value.absent(),
    required bool active,
  })  : name = Value(name),
        active = Value(active);
  static Insertable<Location> custom({
    Expression<int>? id,
    Expression<String>? name,
    Expression<String>? description,
    Expression<String>? geoLocation,
    Expression<bool>? active,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (name != null) 'name': name,
      if (description != null) 'description': description,
      if (geoLocation != null) 'geo_location': geoLocation,
      if (active != null) 'active': active,
    });
  }

  LocationsCompanion copyWith(
      {Value<int>? id,
      Value<String>? name,
      Value<String?>? description,
      Value<String?>? geoLocation,
      Value<bool>? active}) {
    return LocationsCompanion(
      id: id ?? this.id,
      name: name ?? this.name,
      description: description ?? this.description,
      geoLocation: geoLocation ?? this.geoLocation,
      active: active ?? this.active,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (description.present) {
      map['description'] = Variable<String>(description.value);
    }
    if (geoLocation.present) {
      map['geo_location'] = Variable<String>(geoLocation.value);
    }
    if (active.present) {
      map['active'] = Variable<bool>(active.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LocationsCompanion(')
          ..write('id: $id, ')
          ..write('name: $name, ')
          ..write('description: $description, ')
          ..write('geoLocation: $geoLocation, ')
          ..write('active: $active')
          ..write(')'))
        .toString();
  }
}

abstract class _$MoorDb extends GeneratedDatabase {
  _$MoorDb(QueryExecutor e) : super(e);
  late final $BlogPostsTable blogPosts = $BlogPostsTable(this);
  late final $FavoriteSpeciesTable favoriteSpecies =
      $FavoriteSpeciesTable(this);
  late final $FavoriteTagsTable favoriteTags = $FavoriteTagsTable(this);
  late final $ObservationsTable observations = $ObservationsTable(this);
  late final $TagsTable tags = $TagsTable(this);
  late final $LocationsTable locations = $LocationsTable(this);
  late final BlogPostDao blogPostDao = BlogPostDao(this as MoorDb);
  late final FavoriteSpeciesDao favoriteSpeciesDao =
      FavoriteSpeciesDao(this as MoorDb);
  late final FavoriteTagsDao favoriteTagsDao = FavoriteTagsDao(this as MoorDb);
  late final ObservationsDao observationsDao = ObservationsDao(this as MoorDb);
  late final TagsDao tagsDao = TagsDao(this as MoorDb);
  late final LocationsDao locationsDao = LocationsDao(this as MoorDb);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [blogPosts, favoriteSpecies, favoriteTags, observations, tags, locations];
}
