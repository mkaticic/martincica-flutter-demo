import 'dart:io';

import 'package:martincica_flutter_demo/data/sp_helper.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

class FileHelper {
  SharedPreferencesHelper settings = SharedPreferencesHelper();

  Future<String> get _docsPath async {
    final Directory dir = await getApplicationDocumentsDirectory();
    String fullPath = join(dir.path, 'shared_files');
    Directory myDir = Directory(fullPath);
    if (!myDir.existsSync()) {
      await myDir.create();
    }
    return fullPath;
  }

  Future<List<File>> geFiles() async {
    final String dirPath = await _docsPath;
    List<File> files = [];
    List<FileSystemEntity> fse = Directory(dirPath).listSync();
    fse.forEach((element) {
      if (element is File) {
        files.add(element);
      }
    });
    return files;
  }

  Future<List<File>> getImages(String dirPath, bool sortOrderDesc) async {
    List<File> files = [];
    List<FileSystemEntity> fse = Directory(dirPath).listSync();
    fse.forEach((element) {
      if (element is File) {
        files.add(element);
      }
    });
    if (sortOrderDesc) {
      files.sort(
          (f1, f2) => f2.lastModifiedSync().compareTo(f1.lastModifiedSync()));
    } else {
      files.sort(
          (f1, f2) => f1.lastModifiedSync().compareTo(f2.lastModifiedSync()));
    }
    return files;
  }

  Future writeToFile(String fileName, String content) async {
    final String dirPath = await _docsPath;
    final String filePath = join(dirPath, fileName);
    File file = File(filePath);
    return file.writeAsString(content);
  }

  Future<String> readFromAFile(File file) async {
    String content = await file.readAsString();
    return content;
  }

  Future deleteFile(File file) async {
    return await file.delete();
  }

  Future<File> moveFile(File sourceFile, String newPath) async {
    try {
      // prefer using rename as it is probably faster
      File newFile = await sourceFile.rename(newPath);
      return newFile;
    } on FileSystemException catch (e) {
      // if rename fails, copy the source file and then delete it
      final newFile = await sourceFile.copy(newPath);
      await sourceFile.delete();
      return newFile;
    }
  }

  Future<File> moveFileToObservationFolder(File sourceFile) async {
    Directory? dir = await getExternalStorageDirectory();
    if (dir == null) {
      throw Exception("Can't get external storage");
    }
    String dirPath = join(dir.path, 'GEOTropa', 'observations');
    Directory(dirPath).createSync(recursive: true);
    String fullPath = join(dirPath, basename(sourceFile.path));
    return moveFile(sourceFile, fullPath);
  }
}
