// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'dart:async';
import 'dart:io';

import 'package:martincica_flutter_demo/data/model/db/sqlite/note.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class SqliteDb {
  final String colId = 'id';
  final String colName = 'name';
  final String colDate = 'date';
  final String colNotes = 'notes';
  final String colPosition = 'position';
  final String tableNotes = 'notes';

  static final SqliteDb _singleton = SqliteDb._internal();
  static Database? _db;

  SqliteDb._internal();

  factory SqliteDb() {
    return _singleton;
  }

  final int version = 1;

  Future<Database> init() async {
    Directory dir = await getApplicationDocumentsDirectory();
    String dbPath = join(dir.path, 'database', 'sqlite.db');
    Database db =
        await openDatabase(dbPath, version: version, onCreate: _createDb);
    return db;
  }

  Future _createDb(Database db, int version) async {
    String quey = 'CREATE TABLE $tableNotes ('
        '$colId INTEGER PRIMARY KEY, '
        '$colName TEXT, '
        '$colDate TEXT, '
        '$colNotes TEXT, '
        '$colPosition INTEGER '
        ')';
    await db.execute(quey);
  }

  Future<List<Note>> getNotes() async {
    _db ??= await init();
    List<Map<String, dynamic>> noteList =
        await _db!.query(tableNotes, orderBy: colPosition);
    List<Note> notes = [];
    noteList.forEach((element) {
      notes.add(Note.fromMap(element));
    });
    return notes;
  }

  Future<int> insertNote(Note note) async {
    note.position = await findPosition();
    _db ??= await init();
    int id = await _db!.insert(tableNotes, note.toMap());
    return id;
  }

  Future<int> updateNote(Note note) async {
    _db ??= await init();
    return await _db!.update(tableNotes, note.toMap(),
        where: '$colId = ?', whereArgs: [note.id]);
  }

  Future<int> deleteNote(Note note) async {
    _db ??= await init();
    return await _db!
        .delete(tableNotes, where: '$colId = ?', whereArgs: [note.id]);
  }

  Future<int> findPosition() async {
    _db ??= await init();
    String sql = 'select max($colPosition) from $tableNotes';
    List<Map> result = await _db!.rawQuery(sql);
    int? position = result.first.values.first;
    position = (position == null) ? 0 : ++position;
    return position;
  }

  Future reorder(Note note, int newIndex, int oldIndex) async {
    if (oldIndex > newIndex) {
      await _updatePositions(true, newIndex, oldIndex);
    } else {
      await _updatePositions(false, oldIndex, newIndex);
    }
    note.position = newIndex;
    await updateNote(note);
  }

  Future _updatePositions(bool increment, int start, int end) async {
    _db ??= await init();
    late String sql;
    if (increment) {
      sql = 'update $tableNotes '
          'set $colPosition = $colPosition + 1 '
          'where $colPosition >= $start and $colPosition <= $end';
    } else {
      sql = 'update $tableNotes '
          'set $colPosition = $colPosition - 1 '
          'where $colPosition >= $start and $colPosition <= $end';
    }
    await _db!.rawUpdate(sql);
  }
}
