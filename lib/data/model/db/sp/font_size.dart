class FontSize {
  String name;
  double size;

  FontSize(this.name, this.size);

  @override
  String toString() {
    return name + ': ' + size.toString();
  }
}
