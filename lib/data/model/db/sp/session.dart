import 'package:uuid_type/uuid_type.dart';

class Session {
  String id = RandomUuidGenerator().generate().toString();
  String date = '';
  String name = '';
  String description = '';
  int duration = 0;

  Session(this.id, this.date, this.name, this.description, this.duration);

  Session.fromJson(Map<String, dynamic> data) {
    id = data['id'] ?? 0;
    date = data['date'] ?? '';
    name = data['name'] ?? '';
    description = data['description'] ?? '';
    duration = data['duration'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'date': date,
      'name': name,
      'description': description,
      'duration': duration
    };
  }
}
