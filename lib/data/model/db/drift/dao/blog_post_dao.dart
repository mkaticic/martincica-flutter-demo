import 'package:drift/drift.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/blog_post.dart';

import '../../../../drift_db.dart';

part 'blog_post_dao.g.dart';

@DriftAccessor(tables: [BlogPosts])
class BlogPostDao extends DatabaseAccessor<MoorDb> with _$BlogPostDaoMixin {
  final MoorDb db;

  BlogPostDao(this.db) : super(db);

  Future<List<BlogPost>> getPosts() => (select(blogPosts)
    ..orderBy([
          (post) => OrderingTerm(expression: post.date, mode: OrderingMode.desc)
    ]))
      .get();

  Future<int> insertPost(BlogPostsCompanion post) =>
      into(blogPosts).insert(post);

  Future<bool> updatePost(BlogPost post) => update(blogPosts).replace(post);

  Future<int> deletePost(BlogPost post) => delete(blogPosts).delete(post);
}
