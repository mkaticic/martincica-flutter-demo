// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_species_dao.dart';

// ignore_for_file: type=lint
mixin _$FavoriteSpeciesDaoMixin on DatabaseAccessor<MoorDb> {
  $FavoriteSpeciesTable get favoriteSpecies => attachedDatabase.favoriteSpecies;
  $FavoriteTagsTable get favoriteTags => attachedDatabase.favoriteTags;
}
