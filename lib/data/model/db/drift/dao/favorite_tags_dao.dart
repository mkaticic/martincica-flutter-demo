import 'package:drift/drift.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/favorite_tags.dart';

import '../../../../drift_db.dart';

part 'favorite_tags_dao.g.dart';

@DriftAccessor(tables: [FavoriteTags])
class FavoriteTagsDao extends DatabaseAccessor<MoorDb>
    with _$FavoriteTagsDaoMixin {
  final MoorDb db;

  FavoriteTagsDao(this.db) : super(db);

  Future<List<FavoriteTag>> getTagsByFavoriteSpeciesId(int favoriteSpeciesId) =>
      (select(favoriteTags)
        ..where((row) => row.favoriteSpeciesId.equals(favoriteSpeciesId))
        ..orderBy([(row) => OrderingTerm(expression: row.tag)]))
          .get();

  Future<List<String>> getSuggestions() async {
    List<String> tags = await customSelect(
      'SELECT DISTINCT tag FROM favorite_tags',
      readsFrom: {favoriteTags},
    ).map((row) => row.readString('tag')).get();
    return tags;
  }

  Future<int> insert(FavoriteTagsCompanion tagsCompanion) async {
    return into(favoriteTags).insert(tagsCompanion);
  }

  Future deleteTag(int id, String tag) async {
    (delete(favoriteTags)
      ..where(
              (row) => row.favoriteSpeciesId.equals(id) & row.tag.equals(tag)))
        .go();
  }
}
