// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'observations_dao.dart';

// ignore_for_file: type=lint
mixin _$ObservationsDaoMixin on DatabaseAccessor<MoorDb> {
  $ObservationsTable get observations => attachedDatabase.observations;
  $TagsTable get tags => attachedDatabase.tags;
}
