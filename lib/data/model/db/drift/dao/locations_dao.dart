import 'package:drift/drift.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/locations.dart';

import '../../../../drift_db.dart';

part 'locations_dao.g.dart';

@DriftAccessor(tables: [Locations])
class LocationsDao extends DatabaseAccessor<MoorDb> with _$LocationsDaoMixin {
  final MoorDb db;

  LocationsDao(this.db) : super(db);

  Future<List<Location>> getAllLocations() =>
      (select(locations)
      ..orderBy([
            (row) => OrderingTerm(expression: row.name)
      ]))
      .get();

  Future getLocationsByText(String? text) async {
    if (text == null || text.isEmpty) return getAllLocations();
    List<Location> result = await customSelect(
      'SELECT locations.* '
      'FROM locations '
      'WHERE '
      'locations.description LIKE \'%\' || :text || \'%\''
      'OR locations.name LIKE \'%\' || :text || \'%\''
      'ORDER BY locations.name',
      variables: [Variable.withString(text)],
      readsFrom: {locations},
    ).map((row) => locations.map(row.data)).get();
    return result;
  }

  Future deleteAllLocation() async {
    (delete(locations)
        .go());
  }

  Future deleteLocation(int id) async {
    (delete(locations)
      ..where(
              (row) => row.id.equals(id)))
        .go();
  }

  Future<int> insert(LocationsCompanion locationCompanion) async {
    return into(locations).insert(locationCompanion);
  }

}
