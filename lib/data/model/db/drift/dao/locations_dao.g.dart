// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'locations_dao.dart';

// ignore_for_file: type=lint
mixin _$LocationsDaoMixin on DatabaseAccessor<MoorDb> {
  $LocationsTable get locations => attachedDatabase.locations;
}
