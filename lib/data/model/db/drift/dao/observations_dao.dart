import 'package:drift/drift.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/observations.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/tags.dart';

import '../../../../drift_db.dart';

part 'observations_dao.g.dart';

@DriftAccessor(tables: [Observations, Tags])
class ObservationsDao extends DatabaseAccessor<MoorDb>
    with _$ObservationsDaoMixin {
  final MoorDb db;

  ObservationsDao(this.db) : super(db);

  Future<List<Observation>> getAll() =>
      (select(observations)
      ..orderBy([
            (row) => OrderingTerm(expression: row.date, mode: OrderingMode.desc)
      ]))
      .get();

  Future<List<Observation>> getObservationsByDate(DateTime value) =>
     (select(observations)
          ..where(
              (row) => row.date.isBiggerOrEqualValue(value))
          ..orderBy([
                (row) => OrderingTerm(expression: row.date, mode: OrderingMode.asc)
          ]))
          .get();

  Future getObservationsByTag(String? tag, String type) async {
    if (tag == null) return getAll();
    List<Observation> result = await customSelect(
      'SELECT observations.* '
          'FROM observations '
          'JOIN tags on observations.id = tags.ref_id '
          'WHERE tags.tag = :tag AND '
          'tags.type = :type',
      variables: [Variable.withString(tag), Variable.withString(type)],
      readsFrom: {tags, observations},
    ).map((row) => observations.map(row.data)).get();
    return result;
  }

  Future getObservationsByText(String? text) async {
    if (text == null || text.isEmpty) return getAll();
    List<Observation> result = await customSelect(
      'SELECT observations.* '
      'FROM observations '
      'WHERE '
      'observations.description LIKE \'%\' || :text || \'%\''
      'OR observations.scientific_name LIKE \'%\' || :text || \'%\''
      'OR observations.location LIKE \'%\' || :text || \'%\''
      'OR exists ('
          'SELECT 1 '
          'FROM tags '
          'WHERE observations.id = tags.ref_id '
          'AND tags.tag LIKE \'%\' || :text || \'%\' '
          'AND tags.type NOT LIKE \'%observation_photo%\' '
      ') '
      'ORDER BY observations.date DESC',
      variables: [Variable.withString(text)],
      readsFrom: {tags, observations},
    ).map((row) => observations.map(row.data)).get();
    return result;
  }

  Future<Observation> getById(int id) =>
      (select(observations)..where((row) => row.id.equals(id))).getSingle();

  Future<int> insertRow(ObservationsCompanion row) {
    return into(observations).insert(row);
  }

  Future<bool> updateRow(ObservationsCompanion row) =>
      update(observations).replace(row);

  Future<int> deleteRow(Observation row) => delete(observations).delete(row);

  Future<List<String>> getLocationSuggestions() async {
    List<String> locationSuggestions = await customSelect(
        'SELECT DISTINCT location '
            'FROM observations ',
        readsFrom: {observations},
        variables: [])
        .map((row) => row.readString('location'))
        .get();
    return locationSuggestions;
  }

}
