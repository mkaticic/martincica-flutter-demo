// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_tags_dao.dart';

// ignore_for_file: type=lint
mixin _$FavoriteTagsDaoMixin on DatabaseAccessor<MoorDb> {
  $FavoriteSpeciesTable get favoriteSpecies => attachedDatabase.favoriteSpecies;
  $FavoriteTagsTable get favoriteTags => attachedDatabase.favoriteTags;
}
