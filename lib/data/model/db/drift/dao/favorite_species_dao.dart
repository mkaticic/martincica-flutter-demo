import 'package:drift/drift.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/favorite_species.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/favorite_tags.dart';

import '../../../../drift_db.dart';

part 'favorite_species_dao.g.dart';

@DriftAccessor(tables: [FavoriteSpecies, FavoriteTags])
class FavoriteSpeciesDao extends DatabaseAccessor<MoorDb>
    with _$FavoriteSpeciesDaoMixin {
  final MoorDb db;

  FavoriteSpeciesDao(this.db) : super(db);

  Future<List<FavoriteSpecy>> getFavorites() => (select(favoriteSpecies)
        ..orderBy([
          (row) =>
              OrderingTerm(expression: row.created, mode: OrderingMode.desc)
        ]))
      .get();

  Future getFavoritesByTag(String? tag) async {
    if (tag == null || tag.isEmpty) return getFavorites();
    List<FavoriteSpecy> result = await customSelect(
      'SELECT favorite_species.* '
      'FROM favorite_species '
      'LEFT JOIN favorite_tags on favorite_species.id = favorite_tags.favorite_species_id '
      'WHERE favorite_tags.tag LIKE \'%\' || :tag || \'%\' '
      'OR favorite_species.canonical_name LIKE \'%\' || :tag || \'%\' '
      'COLLATE NOCASE',
      variables: [Variable.withString(tag ?? '')],
      readsFrom: {favoriteTags, favoriteSpecies},
    ).map((row) => favoriteSpecies.map(row.data)).get();
    return result;
  }

  Future<FavoriteSpecy> getByGbifKey(int gbifKey) =>
      (select(favoriteSpecies)..where((row) => row.gbifKey.equals(gbifKey)))
          .getSingle();

  Future<int> insert(FavoriteSpeciesCompanion post) {
    return into(favoriteSpecies).insert(post);
  }

  Future<bool> updatePost(FavoriteSpecy row) =>
      update(favoriteSpecies).replace(row);

  Future<int> deleteFavorite(FavoriteSpecy row) =>
      delete(favoriteSpecies).delete(row);

  Future insertIfNew(FavoriteSpeciesCompanion favoriteSpeciesCompanion) async {
    int? gbifKey = favoriteSpeciesCompanion.gbifKey.value;
    if (gbifKey == null) {
      return;
    }
    // if species does not exist there will be an error
    getByGbifKey(gbifKey).catchError((_) => insert(favoriteSpeciesCompanion));
  }
}
