import 'package:drift/drift.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/tags.dart';

import '../../../../drift_db.dart';

part 'tags_dao.g.dart';

@DriftAccessor(tables: [Tags])
class TagsDao extends DatabaseAccessor<MoorDb> with _$TagsDaoMixin {
  final MoorDb db;

  TagsDao(this.db) : super(db);

  Future<List<Tag>> getTags() => select(tags).get();

  Future<List<Tag>> getTagsByRefId(String type, String refId) => (select(tags)
    ..where((row) => row.type.equals(type) & row.refId.equals(refId)))
      .get();

  Future<List<Tag>> getTagsByTag(String tag) => (select(tags)
    ..where((row) => row.tag.equals(tag)))
      .get();

  Future<List<String>> getTagsByType(String type) async {
    List<String> tagSuggestions = await customSelect(
        'SELECT DISTINCT tag '
            'FROM tags '
            'WHERE type = :type',
        readsFrom: {tags},
        variables: [Variable.withString(type)])
        .map((row) => row.readString('tag'))
        .get();
    return tagSuggestions;
  }

  Future<List<Tag>> getTagTypes(String tag) =>
      (select(tags)..where((row) => row.tag.equals(tag))).get();

  Future<int> insert(TagsCompanion tagsCompanion) async {
    return into(tags).insert(tagsCompanion);
  }

  Future deleteTagById(int id) async {
    (delete(tags)..where((row) => row.id.equals(id))).go();
  }

  Future deleteTag(String refId, String type, String tag) async {
    (delete(tags)
      ..where((row) =>
      row.refId.equals(refId) &
      row.type.equals(type) &
      row.tag.equals(tag)))
        .go();
  }
}
