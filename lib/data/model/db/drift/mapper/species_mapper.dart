import 'package:drift/drift.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif/gbif_species.dart';
import 'package:martincica_flutter_demo/data/drift_db.dart';

class SpeciesMapper {
  static FavoriteSpeciesCompanion toFavoriteSpeciesCompanion(GbifSpecies gbifSpecies) {
    return FavoriteSpeciesCompanion(
        gbifKey: Value(gbifSpecies.key),
        canonicalName: Value(gbifSpecies.canonicalName),
        created: Value(DateTime.now()));
  }
}
