import 'package:drift/drift.dart';

class Tags extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get refId => text()();

  TextColumn get type => text()();

  TextColumn get tag => text()();

  BoolColumn get active => boolean()();
}
