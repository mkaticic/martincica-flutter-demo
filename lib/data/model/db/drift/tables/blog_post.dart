import 'package:drift/drift.dart';

class BlogPosts extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get name => text().withLength(min: 1, max: 80)();

  TextColumn get content => text().nullable()();

  DateTimeColumn get date => dateTime().nullable()();
}
