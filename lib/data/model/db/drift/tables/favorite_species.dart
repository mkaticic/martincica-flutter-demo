import 'package:drift/drift.dart';

class FavoriteSpecies extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get canonicalName => text().withLength(min: 1, max: 300)();
  DateTimeColumn get created => dateTime().nullable()();
  IntColumn get gbifKey => integer().nullable()();
}
