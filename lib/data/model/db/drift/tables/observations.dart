import 'package:drift/drift.dart';

class Observations extends Table {
  IntColumn get id => integer().autoIncrement()();

  TextColumn get description => text()();

  TextColumn get scientificName => text().nullable()();

  TextColumn get notes => text().nullable()();

  IntColumn get gbifKey => integer().nullable()();

  TextColumn get location => text()();

  RealColumn get longitude => real().nullable()();

  RealColumn get latitude => real().nullable()();

  DateTimeColumn get date => dateTime()();

  BoolColumn get isMine => boolean()();
}
