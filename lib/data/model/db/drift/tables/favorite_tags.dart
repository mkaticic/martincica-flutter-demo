import 'package:drift/drift.dart';
import 'package:martincica_flutter_demo/data/model/db/drift/tables/favorite_species.dart';

class FavoriteTags extends Table {
  IntColumn get id => integer().autoIncrement()();

  IntColumn get favoriteSpeciesId =>
      integer().nullable().references(FavoriteSpecies, #id)();

  TextColumn get tag => text()();

  BoolColumn get active => boolean()();
}
