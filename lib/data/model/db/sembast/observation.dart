// ignore_for_file: unnecessary_new

class Observation {
  String? id;
  late String description;
  late String location;
  late String date;
  bool isMine = true;

  Observation(this.description, this.location, this.date);

  Observation.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    description = map['description'];
    location = map['location'];
    date = map['date'];
    isMine = map['isMine'] ?? true;
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'description': description,
      'location': location,
      'date': date,
      'isMine': isMine
    };
  }

  @override
  String toString() {
    return date + ' - ' + description + (isMine ? '' : ' [not mine]');
  }
}
