import 'dart:convert';

import 'package:drift/drift.dart';
import 'package:http/http.dart' as http;
import 'package:martincica_flutter_demo/data/model/api/gbif/gbif_related.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif/gbif_species.dart';

enum GbifTaxon {
  fungi('5','Fungi'),
  planteae('6','Planteae'),
  animalia('1','Animalia');

  final String name;
  final String key;

  const GbifTaxon(this.key, this.name);

  String getKey() {
    return key;
  }

}

class GbifApi {
  // https://api.gbif.org/v1/species/search?q=Geotropa
  // https://api.gbif.org/v1/species/177389964
  // https://api.gbif.org/v1/species/9186073/media
  // https://api.gbif.org/v1/occurrence/search?limit=20&media_type=stillImage&taxon_key=5240269
  // https://api.gbif.org/v1/species/search?rank=KINGDOM
  // https://api.gbif.org/v1/species/2532103/related

  final String authority = 'api.gbif.org';
  final String occurrencePath = '/v1/occurrence';
  final String occurrenceSearchPath = '/v1/occurrence/search';
  final String speciesPath = '/v1/species';
  final String searchPath = '/v1/species/search';

  String? query;

  Future<List<String>> searchSpeciesMediaPng(int id) async {
    if (id == -1) return [];
    Uri uri = Uri.https(authority, speciesPath + '/$id/media');
    http.Response response = await http.get(uri);
    Map<String, dynamic> data = json.decode(response.body);
    List<String> list = (data['results'] as List)
        .where((element) => (element['format'] == 'image/png' ||
        element['format'] == 'image/jpeg'))
        .map((element) => element['identifier'] as String)
        .toList();
    return list;
  }

  // /v1/occurrence/search?limit=20&media_type=stillImage&taxon_key=$id
  Future<List<String>> searchOccuranceMediaPng(int id) async {
    if (id == -1) return [];
    Map<String, dynamic> parameters = {
      'limit': '20',
      'media_type': 'stillImage',
      'taxon_key': id.toString()
    };
    Uri uri = Uri.https(authority, occurrencePath + '/search', parameters);
    http.Response response = await http.get(uri);
    Map<String, dynamic> data = json.decode(response.body);
    List<String> list = [];
    for (Map<String, dynamic> result in data['results']) {
      list.addAll((result['media'] as List)
          .where((element) => (element['identifier'] != null &&
          (element['format'] == 'image/png' ||
              element['format'] == 'image/jpeg')))
          .map((element) => element['identifier'] as String)
          .toList());
    }
    return list;
  }

  // https://api.gbif.org/v1/species/2532103
  Future<GbifSpecies> getSpeciesByKey(int key) async {
    Uri uri = Uri.https(authority, speciesPath + '/$key');
    http.Response response = await http.get(uri);
    Map<String, dynamic> data = json.decode(utf8.decode(response.bodyBytes));
    return GbifSpecies.fromJson(data);
  }

  Future<GbifSpecies> searchSpeciesAndReturnFirst(String query,
      {GbifTaxon taxonKey = GbifTaxon.fungi}) async {
    Map<String, dynamic> parameters = {
      'q': query,
      'highertaxonKey': taxonKey.key
    };
    Uri uri = Uri.https(authority, searchPath, parameters);
    http.Response response = await http.get(uri);
    Map<String, dynamic> data = json.decode(utf8.decode(response.bodyBytes));
    if (data['results'].length == 0) {
      return GbifSpecies();
    }
    return GbifSpecies.fromJson(data['results'][0]);
  }

  Future<List<GbifSpecies>> searchSpecies(String query,
      {GbifTaxon taxonKey = GbifTaxon.fungi}) async {
    Map<String, dynamic> parameters = {
      'q': query,
      'highertaxonKey': taxonKey.key,
      'limit': '100'
    };
    Uri uri = Uri.https(authority, searchPath, parameters);
    http.Response response = await http.get(uri);
    return toGbifSpeciesList(response);
  }

  Future<List<GbifRelated>> getRelated(int? id) async {
    if (id == null) {
      return [];
    }
    Map<String, dynamic> parameters = {'limit': '50'};
    Uri uri = Uri.https(authority, '$speciesPath/$id/related', parameters);
    http.Response response = await http.get(uri);
    return toGbifRelatedList(response);
  }

  List<GbifSpecies> toGbifSpeciesList(http.Response response) {
    Map<String, dynamic> data = json.decode(utf8.decode(response.bodyBytes));
    if (data['results'].length == 0) {
      return [];
    }
    List<GbifSpecies> list =
        (data['results'] as List).map((e) => GbifSpecies.fromJson(e)).toList();
    return list;
  }

  List<GbifRelated> toGbifRelatedList(http.Response response) {
    Map<String, dynamic> data = json.decode(response.body);
    if (data['results'].length == 0) {
      return [];
    }
    List<GbifRelated> list =
        (data['results'] as List).map((e) => GbifRelated.fromJson(e)).toList();
    list.sort(
        (a, b) => b.lastCrawled!.compareTo(a.lastCrawled ?? DateTime.now()));
    return list;
  }

  GbifSpecies findAcceptedOrFirst(List<GbifSpecies> list) {
    if (list.isEmpty) {
      return GbifSpecies();
    }
    GbifSpecies? species;
    try {
      species =
          list.where((element) => element.taxonomicStatus == 'ACCEPTED').first;
    } catch (err) {
      species ??= list[0];
    }
    return species;
  }

  Future<Set<String>> getMycorrhizalPartners(int? speciesKey) async {
    final partners = <String>{};
    // GBIF API endpoint for occurrences
    final occurrencesUrl = 'https://api.gbif.org/v1/occurrence/search';

    // Search for occurrences of the species
    Map<String, dynamic> parameters = {
      'taxonKey': speciesKey.toString(),
      'hasCoordinate': 'true',
      'limit': '300' // Adjust the limit as needed
    };

    Uri uri = Uri.https(authority, occurrenceSearchPath, parameters);
    http.Response response = await http.get(uri);

    if (response.statusCode == 200) {
      final occurrencesResults = json.decode(utf8.decode(response.bodyBytes));
      if (occurrencesResults['results'].isNotEmpty) {
        for (var record in occurrencesResults['results']) {
          if (record.containsKey('associatedTaxa')) {
            partners.addAll(record['associatedTaxa'].split('|'));
          }
        }
        //print('Mycorrhizal partners for $speciesKey: $partners');
      } else {
        //print('No occurrences found for $speciesKey.');
      }
    } else {
      //print('Error fetching occurrences for species key $speciesKey: ${response.statusCode}');
    }
    return partners;
  }
}
