enum MycoMorphBoxProperties {
  sporeprintcolor,
  stipecharacter,
  whichgills,
  capshapem,
  ecologicaltype,
  howedible,
  hymeniumtype
}

class MycoMorphBox {
  final String prop = 'http://dbpedia.org/property/';
  final String ontology = 'http://dbpedia.org/ontology/';

  List<String> sporeprintcolor = [];
  List<String> stipecharacter = [];
  List<String> whichgills = [];
  List<String> capshapem = [];
  List<String> ecologicaltype = [];
  List<String> howedible = [];
  List<String> hymeniumtype = [];
  List<String> description = [];

  MycoMorphBox();

  MycoMorphBox.fromJson(Map<String, dynamic> json) {
    sporeprintcolor = getValue(json, prop + 'sporeprintcolor');
    whichgills = getValue(json, prop + 'whichgills');
    stipecharacter = getValue(json, prop + 'stipecharacter');
    capshapem = getValue(json, prop + 'capshapem');
    ecologicaltype = getValue(json, prop + 'ecologicaltype');
    howedible = getValue(json, prop + 'howedible');
    hymeniumtype = getValue(json, prop + 'hymeniumtype');
    description = getValue(json, ontology + 'abstract');
  }

  List<String> getValue(Map<String, dynamic> json, String propKey) {
    if (!json.containsKey(propKey)) {
      return [];
    }
    List<dynamic> prop = json[propKey];
    return prop
        .where((element) => element['lang'] == 'en')
        .map((element) => element['value'] ?? '')
        .map((e) => e.toString())
        .toList();
  }
}
