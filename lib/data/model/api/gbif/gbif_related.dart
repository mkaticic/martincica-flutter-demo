import 'package:googleapis/cloudsearch/v1.dart';

class GbifRelated {
  DateTime? lastCrawled = null;
  String? references = '';

  GbifRelated.fromJson(Map<String, dynamic> json) {
    lastCrawled = DateTime.parse(json['lastCrawled']);
    references = json['references'];
  }
}
