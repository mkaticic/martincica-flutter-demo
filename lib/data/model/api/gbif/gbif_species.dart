import 'package:martincica_flutter_demo/utils/json_utils.dart';

class GbifSpecies {
  int? key;
  int? nubKey;
  int? nameKey;
  String? taxonID;
  String kingdom = '';
  String phylum = ''; // odjeljak, stablo (ugl. bot.) koljeno (ugl. zool.)
  String clazz = ''; // razred
  String order = ''; // red
  String family = ''; // porodica
  String genus = ''; // rod
  String species = ''; // vrsta
  String authorship = '';
  int? kingdomKey;
  int? phylumKey;
  int? classKey;
  int? orderKey;
  int? familyKey;
  int? genusKey;
  int? speciesKey;
  String? datasetKey;
  int? parentKey;
  String? parent;
  int? acceptedKey;
  String? accepted;
  String scientificName = '';
  String canonicalName = '';
  String? nameType;
  String? origin;
  String taxonomicStatus = '';

  //List<Null> nomenclaturalStatus;
  //int numDescendants;
  // String lastCrawled;
  // String lastInterpreted;
  //List<Null> issues;
  bool? synonym;

  //String class;

  GbifSpecies.empty();

  GbifSpecies({
    this.key,
    this.nubKey,
    this.nameKey,
    this.taxonID,
    this.kingdom = '',
    this.phylum = '',
    this.clazz = '',
    this.order = '',
    this.family = '',
    this.genus = '',
    this.species = '',
    this.authorship = '',
    this.kingdomKey,
    this.phylumKey,
    this.classKey,
    this.orderKey,
    this.familyKey,
    this.genusKey,
    this.speciesKey,
    this.datasetKey,
    this.parentKey,
    this.parent,
    this.acceptedKey,
    this.accepted,
    this.scientificName = '',
    this.canonicalName = '',
    this.nameType,
    this.origin,
    this.taxonomicStatus = '',
    // this.nomenclaturalStatus,
    // this.numDescendants,
    // this.lastCrawled,
    // this.lastInterpreted,
    // this.issues,
    this.synonym,
    // this.class
  });

  GbifSpecies.fromJson(Map<String, dynamic> json) {
    key = json['key'];
    nubKey = json['nubKey'];
    nameKey = json['nameKey'];
    taxonID = json['taxonID'];
    kingdom = JsonUtils.getOrDashStr(json, 'kingdom');
    phylum = JsonUtils.getOrDashStr(json, 'phylum');
    clazz = JsonUtils.getOrDashStr(json, 'class');
    order = JsonUtils.getOrDashStr(json, 'order');
    family = JsonUtils.getOrDashStr(json, 'family');
    genus = JsonUtils.getOrDashStr(json, 'genus');
    species = JsonUtils.getOrDashStr(json, 'species');
    authorship = JsonUtils.getOrDashStr(json, 'authorship');
    kingdomKey = json['kingdomKey'];
    phylumKey = json['phylumKey'];
    classKey = json['classKey'];
    orderKey = json['orderKey'];
    familyKey = json['familyKey'];
    genusKey = json['genusKey'];
    speciesKey = json['speciesKey'];
    datasetKey = json['datasetKey'];
    parentKey = json['parentKey'];
    parent = JsonUtils.getOrDashStr(json, 'parent');
    acceptedKey = json['acceptedKey'];
    accepted = JsonUtils.getOrDashStr(json, 'accepted');
    scientificName = JsonUtils.getOrDashStr(json, 'scientificName');
    canonicalName = JsonUtils.getOrDashStr(json, 'canonicalName');
    authorship = JsonUtils.getOrDashStr(json, 'authorship');
    nameType = JsonUtils.getOrDashStr(json, 'nameType');
    origin = JsonUtils.getOrDashStr(json, 'origin');
    taxonomicStatus = JsonUtils.getOrDashStr(json, 'taxonomicStatus');
    synonym = JsonUtils.getOrDashBoolean(json, 'synonym');
  }

}
