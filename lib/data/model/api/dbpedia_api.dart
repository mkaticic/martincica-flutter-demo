import 'dart:convert';
import 'package:http/http.dart' as http;

import 'dbpedia/myco_morph_box.dart';

class DbpediaApi {
  // http://dbpedia.org/resource/Amanita_ovoidea
  final String resourceKeyPrefix = 'http://dbpedia.org/resource/';

  final String authority = 'dbpedia.org';

  // https://dbpedia.org/data/Amanita_caesarea.json

  Future<MycoMorphBox?> getMycoMorphBox(String speciesName) async {
    if (speciesName.isEmpty) {
      return null;
    }
    //String dbpediaSpeciesKey = 'Amanita_caesarea';
    String dbpediaSpeciesKey =
        speciesName.toLowerCase().replaceAll(' ', '_').capitalize();
    String path = '/data/' + dbpediaSpeciesKey + '.json';
    Uri uri = Uri.https(authority, path);
    http.Response response = await http.get(uri);
    Map<String, dynamic> data = json.decode(utf8.decode(response.bodyBytes));
    String dbpediaFullKey = resourceKeyPrefix + dbpediaSpeciesKey;
    if (!data.containsKey(dbpediaFullKey) || data[dbpediaFullKey].length == 0) {
      return null;
    }
    Map<String, dynamic> speciesDesc = data[dbpediaFullKey];

    // remove unuseful data
    speciesDesc.remove('http://www.w3.org/2002/07/owl#sameAs');
    if (speciesDesc.isEmpty) {
      return null;
    }
    return MycoMorphBox.fromJson(speciesDesc);
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
