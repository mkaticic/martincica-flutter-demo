import 'dart:convert';

import 'package:http/http.dart' as http;

class LatinWordNetApi {
  // https://latinwordnet.exeter.ac.uk/lemmatize/hirsutum/

  final String authority = 'latinwordnet.exeter.ac.uk';

  Future<String> lemmatize(String word) async {
    String cleanedWord = clean(word);
    Uri uri = Uri.https(authority, '/lemmatize/$cleanedWord');
    http.Response response = await http.get(uri);
    List<dynamic> data = json.decode(utf8.decode(response.bodyBytes));
    if (data.isEmpty) {
      return "";
    }
    Map<String, dynamic> lemma = data[0]['lemma'] as Map<String, dynamic>;
    return lemma['lemma'];
  }

  Future<GetSynsetsResult> getSynsets(String lemma) async {
    // part of speech: ('n', 'v', 'a', 'r')
    String partOfSpeech = '*';
    Uri uri = Uri.https(authority, '/api/lemmas/$lemma/$partOfSpeech/synsets/');
    http.Response response = await http.get(uri);
    Map<String, dynamic> data = json.decode(utf8.decode(response.bodyBytes));
    return GetSynsetsResult.fromJson(data);
  }

  Future<LemaElem?> translate(String word) async {
    String lemma = await lemmatize(word);
    if (lemma.isEmpty) {
      return LemaElem();
    }
    GetSynsetsResult result = await getSynsets(lemma);
    if (result.results!.isEmpty) {
      return null;
    }
    return result.results![0];
  }

  String clean(String word) {
    return word.trim().toLowerCase();
  }
}

class GetSynsetsResult {
  int? count;
  Null? next;
  Null? previous;
  List<LemaElem>? results;

  GetSynsetsResult({this.count, this.next, this.previous, this.results});

  GetSynsetsResult.fromJson(Map<String, dynamic> json) {
    count = json['count'];
    next = json['next'];
    previous = json['previous'];
    if (json['results'] != null) {
      results = <LemaElem>[];
      json['results'].forEach((v) {
        results!.add(LemaElem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['count'] = this.count;
    data['next'] = this.next;
    data['previous'] = this.previous;
    if (this.results != null) {
      data['results'] = this.results!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LemaElem {
  String? lemma;
  String? pos;
  String? morpho;
  String? uri;
  String? prosody;
  Synsets? synsets;

  LemaElem(
      {this.lemma,
      this.pos,
      this.morpho,
      this.uri,
      this.prosody,
      this.synsets});

  LemaElem.fromJson(Map<String, dynamic> json) {
    lemma = json['lemma'];
    pos = json['pos'];
    morpho = json['morpho'];
    uri = json['uri'];
    prosody = json['prosody'];
    synsets =
        json['synsets'] != null ? Synsets.fromJson(json['synsets']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['lemma'] = this.lemma;
    data['pos'] = this.pos;
    data['morpho'] = this.morpho;
    data['uri'] = this.uri;
    data['prosody'] = this.prosody;
    if (this.synsets != null) {
      data['synsets'] = this.synsets!.toJson();
    }
    return data;
  }
}

class Synsets {
  List<SynsetElement>? literal;
  List<SynsetElement>? metonymic;
  List<SynsetElement>? metaphoric;

  Synsets({this.literal, this.metonymic, this.metaphoric});

  Synsets.fromJson(Map<String, dynamic> json) {
    if (json['literal'] != null) {
      literal = <SynsetElement>[];
      json['literal'].forEach((v) {
        literal!.add(SynsetElement.fromJson(v));
      });
    }
    if (json['metonymic'] != null) {
      metonymic = <SynsetElement>[];
      json['metonymic'].forEach((v) {
        metonymic!.add(SynsetElement.fromJson(v));
      });
    }
    if (json['metaphoric'] != null) {
      metaphoric = <SynsetElement>[];
      json['metaphoric'].forEach((v) {
        metaphoric!.add(SynsetElement.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.literal != null) {
      data['literal'] = this.literal!.map((v) => v.toJson()).toList();
    }
    if (this.metonymic != null) {
      data['metonymic'] = this.metonymic!.map((v) => v.toJson()).toList();
    }
    if (this.metaphoric != null) {
      data['metaphoric'] = this.metaphoric!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SynsetElement {
  Language? language;
  String? pos;
  String? offset;
  String? gloss;
  List<Semfield>? semfield;

  SynsetElement(
      {this.language, this.pos, this.offset, this.gloss, this.semfield});

  SynsetElement.fromJson(Map<String, dynamic> json) {
    language =
        json['language'] != null ? Language.fromJson(json['language']) : null;
    pos = json['pos'];
    offset = json['offset'];
    gloss = json['gloss'];
    if (json['semfield'] != null) {
      semfield = <Semfield>[];
      json['semfield'].forEach((v) {
        semfield!.add(Semfield.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (this.language != null) {
      data['language'] = this.language!.toJson();
    }
    data['pos'] = this.pos;
    data['offset'] = this.offset;
    data['gloss'] = this.gloss;
    if (this.semfield != null) {
      data['semfield'] = this.semfield!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Language {
  String? abbrev;

  Language({this.abbrev});

  Language.fromJson(Map<String, dynamic> json) {
    abbrev = json['abbrev'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['abbrev'] = this.abbrev;
    return data;
  }
}

class Semfield {
  String? code;
  String? english;

  Semfield({this.code, this.english});

  Semfield.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    english = json['english'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['code'] = this.code;
    data['english'] = this.english;
    return data;
  }
}