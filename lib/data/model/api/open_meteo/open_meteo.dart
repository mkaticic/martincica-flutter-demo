class OpenMeteoData {
  double latitude = 0;
  double longitude = 0;
  List<dynamic> temperatureHourhly = [];
  List<dynamic> days = [];
  List<dynamic> temperatureMaxDaily = [];
  List<dynamic> temperatureMinDaily = [];
  List<dynamic> weatherCode = [];

  List<List<dynamic>> data = [];

  OpenMeteoData.fromJson(Map<String, dynamic> json) {
    latitude = json["latitude"];
    longitude = json["longitude"];
    Map<String, dynamic> t1 = json["hourly"];
    List<dynamic> t2 = t1["temperature_2m"];
    temperatureHourhly = json["hourly"]["temperature_2m"];
    days = json["daily"]["time"];
    temperatureMaxDaily = json["daily"]["temperature_2m_max"];
    temperatureMinDaily = json["daily"]["temperature_2m_min"];
    weatherCode = json["daily"]["weather_code"];

    int i = 0;
    for (String day in days) {
      data.add([
        days[i],
        temperatureMinDaily[i].toString() +
            " / " +
            temperatureMaxDaily[i].toString() +
            "°C",
        codeDescription(weatherCode[i]),
      ]);
      i = i + 1;
    }
  }

  codeDescription(weatherCode) {
    switch (weatherCode) {
      case 0:
        return "Clear sky";
      case 1:
        return "Mainly clear";
      case 2:
        return "Partly cloudy,";
      case 3:
        return "Overcast";
      case 45:
      case 48:
        return "Fog and depositing rime fog";
      case 51:
        return "Drizzle: Light";
      case 53:
        return "Drizzle: moderate";
      case 55:
        return "Drizzle: dense intensity";
      case 56:
        return "Freezing drizzle: Light";
      case 57:
        return "Freezing drizzle: dense intensity";
      case 61:
        return "Rain: Light";
      case 63:
        return "Rain: moderate";
      case 65:
        return "Rain: dense intensity";
      case 66:
        return "Freezing rain: Light";
      case 67:
        return "Freezing rain: dense intensity";
      case 71:
        return "Snow fall: Light";
      case 73:
        return "Snow fall: moderate";
      case 75:
        return "Snow fall: dense intensity";
      case 77:
        return "Snow grains";
      case 80:
        return "Rain showers: Light";
      case 81:
        return "Rain showers: moderate";
      case 82:
        return "Rain showers: dense intensity";
      case 85:
        return "Snow showers: Light";
      case 86:
        return "Snow showers: dense intensity";
      case 95:
        return "Thunderstorm: Slight or moderate";
      case 96:
        return "Thunderstorm with slight hail";
      case 99:
        return "Thunderstorm with heavy hail";
      default:
        return "unknown";
    }
  }

//   Code 	Description
// 0 	Clear sky
// 1, 2, 3 	Mainly clear, partly cloudy, and overcast
// 45, 48 	Fog and depositing rime fog
// 51, 53, 55 	Drizzle: Light, moderate, and dense intensity
// 56, 57 	Freezing Drizzle: Light and dense intensity
// 61, 63, 65 	Rain: Slight, moderate and heavy intensity
// 66, 67 	Freezing Rain: Light and heavy intensity
// 71, 73, 75 	Snow fall: Slight, moderate, and heavy intensity
// 77 	Snow grains
// 80, 81, 82 	Rain showers: Slight, moderate, and violent
// 85, 86 	Snow showers slight and heavy
// 95 * 	Thunderstorm: Slight or moderate
// 96, 99 * 	Thunderstorm with slight and heavy hail
}

// {
//   "latitude": 45.1,
//   "longitude": 14.119999,
//   "generationtime_ms": 0.054955482482910156,
//   "utc_offset_seconds": 0,
//   "timezone": "GMT",
//   "timezone_abbreviation": "GMT",
//   "elevation": 216,
