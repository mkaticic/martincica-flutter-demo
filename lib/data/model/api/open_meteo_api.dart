import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:martincica_flutter_demo/data/model/api/gbif/gbif_species.dart';

import 'open_meteo/open_meteo.dart';
import 'package:latlong2/latlong.dart';

class OpenMeteoApi {
  // https://api.open-meteo.com

  final String authority = 'api.open-meteo.com';

  Future<OpenMeteoData?> getOpenMeteoData(
      LatLng? latLong, DateTime date) async {
    if (latLong == null) {
      return null;
    }
    String path = '/v1/forecast';
    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    Map<String, dynamic> queryParams = {
      "latitude": latLong.latitude.toString(),
      "longitude": latLong.longitude.toString(),
      "hourly": "temperature_2m",
      "daily":
          "weather_code,temperature_2m_max,temperature_2m_min,sunrise,sunset",
      "start_date": formatter.format(date.subtract(Duration(days: 14))),
      "end_date": formatter.format(date)
    };

    Uri uri = Uri.https(authority, path, queryParams);
    http.Response response = await http.get(uri);
    Map<String, dynamic> data = json.decode(response.body);
    return OpenMeteoData.fromJson(data);
  }
}
