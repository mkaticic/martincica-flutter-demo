// ignore_for_file: avoid_function_literals_in_foreach_calls

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'model/db/sp/session.dart';

class SharedPreferencesHelper {
  bool isInitialized = false;
  static late SharedPreferences _prefs;
  static SharedPreferencesHelper? _instance;

  final sessionPrefix = 'session_';
  final favoriteFungiPrefix = 'favoriteFungiPrefix';

  final fontSizeKey = 'settings_font_size';
  final themeColorKey = 'settings_theme_color';

  // bool
  final encryptDbKey = 'settings_encrypt_db';
  final debugModeOnKey = 'settings_debug_mode_on';


  final photoGalleryDir = 'setting_photo_gallery_directory_path';
  final observationsDir = 'setting_observations_directory_path';
  final whatsupPhoneNumber = 'setting_whatsup_phone_number';

  // dropdown history
  final ddHistorySearchKey = 'ddHistorySearchKey';
  final ddHistoryLocationsKey = 'ddHistoryLocationsKey';

  SharedPreferencesHelper._internal();

  factory SharedPreferencesHelper() {
    _instance ??= SharedPreferencesHelper._internal();
    return _instance as SharedPreferencesHelper;
  }

  Future init() async {
    if (!isInitialized) {
      _prefs = await SharedPreferences.getInstance();
      isInitialized = true;
    }
  }

  Future setColor(int color) async {
    return _prefs.setInt(themeColorKey, color);
  }

  Future setFontSize(double fontSize) async {
    return _prefs.setDouble(fontSizeKey, fontSize);
  }

  int getColor() {
    int color = _prefs.getInt(themeColorKey) ?? 0xFF4CAF50; // default green
    return color;
  }

  double getFontSize() {
    double fontSize = _prefs.getDouble(fontSizeKey) ?? 16; // default 16 DPI
    return fontSize;
  }

  bool getEncryptDatabase() {
    bool encryptDb = _prefs.getBool(encryptDbKey) ?? false; // default 16 DPI
    return encryptDb;
  }

  Future setEncryptDatabase(bool encryptDb) async {
    return _prefs.setBool(encryptDbKey, encryptDb);
  }
  
  bool getDebugModeOn() {
    bool encryptDb = _prefs.getBool(debugModeOnKey) ?? false; // default 16 DPI
    return encryptDb;
  }
  
  Future setDebugModeOn(bool debugModeOn) async {
    return _prefs.setBool(debugModeOnKey, debugModeOn);
  }

  Future setStr(String key, String value) async {
    return _prefs.setString(key, value);
  }

  String? getStr(String key) {
    return _prefs.getString(key);
  }

  bool? getBool(String key) {
    return _prefs.getBool(key);
  }

  Future writeSession(Session session) async {
    _prefs.setString(session.id.toString(), json.encode(session.toJson()));
  }

  Future deleteSession(String id) async {
    _prefs.remove(id);
  }

  List<Session> getSessions() {
    List<Session> sessions = [];
    Set<String> keys = _prefs.getKeys();
    keys.forEach((String key) {
      if (key.startsWith(sessionPrefix)) {
        Session session =
            Session.fromJson(json.decode(_prefs.getString(key) ?? ''));
        sessions.add(session);
      }
    });
    return sessions;
  }

  Future<List<String>> getSuggestions(String key, String pattern) async {
    if (pattern == '') return [];
    List<String> suggestions = _prefs.getStringList(key) ?? [];
    return suggestions
        .where((element) => element.toLowerCase().contains(pattern.toLowerCase()))
        .toList();
  }

  Future addSuggestion(String key, String value) async {
    if(value.isEmpty){
      return;
    }
    Set<String> suggestion = (_prefs.getStringList(key) ?? []).toSet();
    suggestion.add(value);
    _prefs.setStringList(key, suggestion.toList());
  }

  Future deleteSuggestion(String key, String value) async {
    Set<String> suggestion = (_prefs.getStringList(key) ?? []).toSet();
    suggestion.remove(value);
    _prefs.setStringList(key, suggestion.toList());
  }
}